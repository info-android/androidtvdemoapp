package app.convergent.androidtvdemoapp.Users.Model.UrlListModel;

import com.google.gson.annotations.SerializedName;

import app.convergent.androidtvdemoapp.Helper.MethodClass;

public class Result{

	@SerializedName("see_set_with_urls")
	private SeeSetWithUrls seeSetWithUrls;

	public void setSeeSetWithUrls(SeeSetWithUrls seeSetWithUrls){
		this.seeSetWithUrls = seeSetWithUrls;
	}

	public SeeSetWithUrls getSeeSetWithUrls(){
		return MethodClass.defaultWhenNull(seeSetWithUrls,new SeeSetWithUrls());

	}

	@Override
 	public String toString(){
		return 
			"Result{" + 
			"see_set_with_urls = '" + seeSetWithUrls + '\'' + 
			"}";
		}
}