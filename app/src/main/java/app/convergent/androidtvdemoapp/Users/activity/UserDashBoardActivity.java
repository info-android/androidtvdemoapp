package app.convergent.androidtvdemoapp.Users.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;

/*import app.convergent.androidtvapp.Helper.MethodClass;
import app.convergent.androidtvapp.R;
import app.convergent.androidtvapp.Users.Set_Management.Add_Edit_Set_Mgmt;
import app.convergent.androidtvapp.Users.Set_Management.SetManagement;
import app.convergent.androidtvapp.Users.URL_Management.Add_Edit_URL;
import app.convergent.androidtvapp.Users.URL_Management.URLManagement*/;import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import app.convergent.androidtvdemoapp.Helper.Constants;
import app.convergent.androidtvdemoapp.Helper.MethodClass;
import app.convergent.androidtvdemoapp.Helper.SharedPrefManager;
import app.convergent.androidtvdemoapp.Helper.VolleySingleton;
import app.convergent.androidtvdemoapp.Model_Class.AdminDashboard.MyProfile;
import app.convergent.androidtvdemoapp.R;
import app.convergent.androidtvdemoapp.Users.activity.DeviceManagement.DeviceMgmtActivity;
import app.convergent.androidtvdemoapp.Users.activity.Set_Management.AddSetActivity;
import app.convergent.androidtvdemoapp.Users.activity.Set_Management.SetManagementActivity;

import static app.convergent.androidtvdemoapp.Helper.Constants.UNAME;
import static app.convergent.androidtvdemoapp.Helper.SharedPrefManager.KEY_TOKEN;

public class UserDashBoardActivity extends AppCompatActivity {
    private ImageView add_devices, view_devices, add_sets, view_sets, add_url, view_url;
    private TextView user_name_tv, user_mail_tv, user_phone_tv, total_dev_tv, total_set_tv, total_url_tv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_dash_board);

        TextView title = findViewById(R.id.user_dash_title);
        title.setText(getString(R.string.dashboard));

        add_devices = findViewById(R.id.addDevice);
        view_devices = findViewById(R.id.viewDevice);
        add_sets = findViewById(R.id.addSet);
        view_sets = findViewById(R.id.viewSet);
        add_url = findViewById(R.id.addUrl);
        view_url = findViewById(R.id.viewUrl);

        user_name_tv = findViewById(R.id.userId);
        user_mail_tv = findViewById(R.id.userMail);
        user_phone_tv = findViewById(R.id.userPhone);
        total_dev_tv = findViewById(R.id.totalDevice);
        total_set_tv = findViewById(R.id.totalSet);
        total_url_tv = findViewById(R.id.totalUrl);

        add_devices.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(UserDashBoardActivity.this, AddDeviceActivity.class);
                //intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);

            }
        });

        view_devices.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(UserDashBoardActivity.this, DeviceMgmtActivity.class);
                //intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);

            }
        });

        /*add_url.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(UserDashBoardActivity.this, Add_Edit_URL.class);
                //intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);

            }
        });

        view_url.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(UserDashBoardActivity.this, URLManagement.class);
                //intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);

            }
        });*/

        add_sets.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(UserDashBoardActivity.this, AddSetActivity.class);
                //intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);

            }
        });

        view_sets.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(UserDashBoardActivity.this, SetManagementActivity.class);
                //intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);

            }
        });



    }

    @Override
    protected void onResume() {
        super.onResume();
        MethodClass.setUserMenu(this);

        user_dashboard();
    }

    @SuppressLint("WrongConstant")
    public void user_home_menu(View view) {
        DrawerLayout drawer_layout = findViewById(R.id.user_drawer_layout);
        drawer_layout.openDrawer(Gravity.START);
    }

    public void user_dashboard() {

        String utoken = String.valueOf(SharedPrefManager.getInstance(UserDashBoardActivity.this).getUserToken(UserDashBoardActivity.this,KEY_TOKEN));

        if (!MethodClass.isNetworkConnected(this)) {
            Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), getResources().getString(R.string.please_check_your_internet_connection), Snackbar.LENGTH_SHORT);
            snackbar.show();
            return;
        }

        MethodClass.showProgressDialog(UserDashBoardActivity.this);
        String server_url = Constants.BASE_URL + "my-profile";

        HashMap<String, String> params = new HashMap<String, String>();

        JSONObject jsonObject = MethodClass.Json_rpc_format(params);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                MethodClass.hideProgressDialog(UserDashBoardActivity.this);
                Log.e("resp", response.toString());
                try {

                    JSONObject jsonObject = MethodClass.get_result_from_webservice(UserDashBoardActivity.this, response);

                    //if no error in response
                    if (jsonObject!= null) {
                        //Toast.makeText(getApplicationContext(), jsonObject.getString("message"), Toast.LENGTH_SHORT).show();

                        /*GsonBuilder gsonBuilder = new GsonBuilder();
                        Gson gson = gsonBuilder.create();
                        LoginMain loginMain = gson.fromJson(response.toString(), LoginMain.class);*/

                        //getting user-profile from the response
                        JSONObject profileJson = jsonObject.getJSONObject("myprofile");

                        //creating a new user object
                        MyProfile myProfile = new MyProfile(
                                profileJson.getInt("id"),
                                profileJson.getString("name"),
                                profileJson.getString("email"),
                                profileJson.getString("user_type"),
                                profileJson.getString("ph"),
                                profileJson.getString("status")
                        );

                        UNAME = myProfile.getName();
                        user_name_tv.setText(UNAME);

                        //storing the user name
                        //SharedPrefManager.getInstance(getApplicationContext()).userName(UNAME);

                        String user_mail = myProfile.getEmail();
                        user_mail_tv.setText(user_mail);
                        String user_ph = myProfile.getPh();
                        user_phone_tv.setText(user_ph);

                        String total_device = jsonObject.getString("total_device");
                        total_dev_tv.setText(total_device);
                        String total_set = jsonObject.getString("totalSet");
                        total_set_tv.setText(total_set);
                        String total_url = jsonObject.getString("totalUrl");
                        total_url_tv.setText(total_url);

                    }

                } catch (JSONException e) {
                    MethodClass.error_alert(UserDashBoardActivity.this);
                    e.printStackTrace();
                    Log.e("myprofile_user_parce", e.toString());
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                MethodClass.hideProgressDialog(UserDashBoardActivity.this);
                if (error.toString().contains("ConnectException")) {
                    MethodClass.network_error_alert(UserDashBoardActivity.this);
                } else {
                    MethodClass.error_alert(UserDashBoardActivity.this);
                }
            }

        }){
            //* Passing some request headers*
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", "Bearer " + utoken);

                Log.e("getHeaders: ", headers.toString());

                return headers;
            }
        };

        VolleySingleton.getInstance(UserDashBoardActivity.this).addToRequestQueue(jsonObjectRequest);
    }

}
