package app.convergent.androidtvdemoapp.Users.adapter;

import static app.convergent.androidtvdemoapp.Helper.SharedPrefManager.KEY_TOKEN;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.material.snackbar.Snackbar;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/*import app.convergent.androidtvapp.R;
import app.convergent.androidtvapp.Users.Set_Management.Add_Edit_Set_Mgmt;
import app.convergent.androidtvapp.Users.URL_Management.URLManagement;*/
import app.convergent.androidtvdemoapp.Helper.Constants;
import app.convergent.androidtvdemoapp.Helper.MethodClass;
import app.convergent.androidtvdemoapp.Helper.SharedPrefManager;
import app.convergent.androidtvdemoapp.Helper.VolleySingleton;
import app.convergent.androidtvdemoapp.R;
import app.convergent.androidtvdemoapp.Users.Model.SetListModel.SeeSetWithUrlsItem;
import app.convergent.androidtvdemoapp.Users.activity.Set_Management.AddSetActivity;
import app.convergent.androidtvdemoapp.Users.activity.Set_Management.SetManagementActivity;
import app.convergent.androidtvdemoapp.Users.activity.URL_Management.URLManagement;
import de.hdodenhof.circleimageview.CircleImageView;

public class SetAdapter extends RecyclerView.Adapter<SetAdapter.ViewHolder> {
    Activity activity;
    public List<SeeSetWithUrlsItem> map_list;

    public SetAdapter(Activity activity, List<SeeSetWithUrlsItem> map_list) {
        this.activity = activity;
        this.map_list = map_list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.user_set_list, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        /*final HashMap<String, String> map = map_list.get(position);*/

        // int pos = holder.getAdapterPosition();

        SeeSetWithUrlsItem model = map_list.get(position);

        holder.tv_setname.setText(model.getSetName());

        holder.edtDev.setOnClickListener(v -> {

            Intent intent = new Intent(activity, AddSetActivity.class);
            intent.putExtra("set_header", "EDIT SET");
            intent.putExtra("set_save", "SAVE SET");
            intent.putExtra("set_id", model.getSetId());
            intent.putExtra("set_name", model.getSetName());
            activity.startActivity(intent);

        });

        holder.img_view_url.setOnClickListener(v -> {

            Intent intent = new Intent(activity, URLManagement.class);
            intent.putExtra("fetch_all_url", false);
            intent.putExtra("set_name", holder.tv_setname.getText());
            intent.putExtra("set_id", model.getSetId());
            activity.startActivity(intent);

        });

        holder.img_delete.setOnClickListener(v -> {

            SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(activity, SweetAlertDialog.WARNING_TYPE);
            if (sweetAlertDialog.isShowing()) {
                sweetAlertDialog.dismissWithAnimation();
            }

            sweetAlertDialog.setTitleText("Delete Set");
            sweetAlertDialog.setContentText("Are you sure you want to delete this set?");
            sweetAlertDialog.setConfirmText("Ok");
            sweetAlertDialog.setCancelText("No");
            sweetAlertDialog.setCancelClickListener(null);

            sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sDialog) {
                    sDialog.dismissWithAnimation();
                    deleteSet(model.getSetId());
                }
            });
            sweetAlertDialog.setCancelable(true);
            sweetAlertDialog.setCanceledOnTouchOutside(true);
            sweetAlertDialog.show();

        });
    }

    @Override
    public int getItemCount() {
        return map_list == null ? 0 : map_list.size();  /*map_list.size()*/
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private CircleImageView edtDev, img_delete, img_view_url;
        private CardView cardView;
        TextView tv_setname;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            img_view_url = itemView.findViewById(R.id.img_view_url);
            tv_setname = itemView.findViewById(R.id.tv_setname);
            edtDev = itemView.findViewById(R.id.edtDev);
            cardView = itemView.findViewById(R.id.cardView);
            img_delete = itemView.findViewById(R.id.img_delete);
        }
    }

    public void deleteSet(String setId) {


        if (!MethodClass.isNetworkConnected(activity)) {
            Snackbar snackbar = Snackbar.make(activity.findViewById(android.R.id.content), activity.getResources().getString(R.string.please_check_your_internet_connection), Snackbar.LENGTH_SHORT);
            snackbar.show();
            return;
        }

        MethodClass.showProgressDialog(activity);

        String server_url = Constants.BASE_URL + "set-delete";

        HashMap<String, String> params = new HashMap<String, String>();
        params.put("set_id", setId);
        JSONObject jsonObject = MethodClass.Json_rpc_format(params);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                MethodClass.hideProgressDialog(activity);
                Log.e("resp", response.toString());
                try {

                    JSONObject jsonObject = MethodClass.get_result_from_webservice(activity, response);


                    if (jsonObject != null) {

                        ((SetManagementActivity) activity).getSetList();

                        SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(activity, SweetAlertDialog.SUCCESS_TYPE);
                        if (sweetAlertDialog.isShowing()) {
                            sweetAlertDialog.dismissWithAnimation();
                        }

                        sweetAlertDialog.setTitleText(jsonObject.getString("message"));
                        sweetAlertDialog.setContentText(jsonObject.getString("message"));
                        sweetAlertDialog.setConfirmText("Ok");
                        sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();

                            }
                        });
                        sweetAlertDialog.setCancelable(false);
                        sweetAlertDialog.setCanceledOnTouchOutside(false);
                        sweetAlertDialog.show();


                    }

                } catch (JSONException e) {
                    MethodClass.error_alert(activity);
                    e.printStackTrace();
                    Log.e("login_parce", e.toString());
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                MethodClass.hideProgressDialog(activity);
                if (error.toString().contains("ConnectException")) {
                    MethodClass.network_error_alert(activity);
                } else {
                    MethodClass.error_alert(activity);
                }
            }
        }) {
            // Passing some request headers
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", "Bearer " + String.valueOf(SharedPrefManager.getInstance(activity).getUserToken(activity, KEY_TOKEN)));

                Log.e("getHeaders: ", headers.toString());

                return headers;
            }
        };

        VolleySingleton.getInstance(activity).addToRequestQueue(jsonObjectRequest);

    }
}

