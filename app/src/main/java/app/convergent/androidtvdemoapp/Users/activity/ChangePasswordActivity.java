package app.convergent.androidtvdemoapp.Users.activity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.text.InputType;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;

import app.convergent.androidtvdemoapp.Helper.MethodClass;
import app.convergent.androidtvdemoapp.R;

/*import app.convergent.androidtvapp.Helper.MethodClass;
import app.convergent.androidtvapp.R;*/

public class ChangePasswordActivity extends AppCompatActivity {
    private Button user_button1, user_button2;
    private EditText confm_pass, pass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_change_password);

        TextView title = findViewById(R.id.user_dash_title);
        title.setText(getString(R.string.change_pwd));

        confm_pass = findViewById(R.id.uconfpass);
        pass = findViewById(R.id.upass);
        user_button1 = findViewById(R.id.user_button1);
        user_button2 = findViewById(R.id.user_button2);

        user_button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pass.getInputType() == InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD) {
                    pass.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    pass.setTransformationMethod(new PasswordTransformationMethod());
                } else {
                    pass.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                    pass.setTransformationMethod(new HideReturnsTransformationMethod());
                }
            }
        });

        user_button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (confm_pass.getInputType() == InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD) {
                    confm_pass.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    confm_pass.setTransformationMethod(new PasswordTransformationMethod());
                } else {
                    confm_pass.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                    confm_pass.setTransformationMethod(new HideReturnsTransformationMethod());
                }
            }
        });

    }

    public void uchangePwd(View view) {

    }
    @Override
    protected void onResume() {
        super.onResume();
        MethodClass.setUserMenu(this);
    }

    @SuppressLint("WrongConstant")
    public void user_home_menu(View view) {
        DrawerLayout drawer_layout = findViewById(R.id.user_drawer_layout);
        drawer_layout.openDrawer(Gravity.START);
    }

//    public void uback(View view) {
//        onBackPressed();
//    }
}

