package app.convergent.androidtvdemoapp.Users.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import app.convergent.androidtvdemoapp.Activity.SplashActivity;
import app.convergent.androidtvdemoapp.Admin.AdminDashboardActivity;
import app.convergent.androidtvdemoapp.Admin.AdminEditProfileActivity;
import app.convergent.androidtvdemoapp.Helper.Constants;
import app.convergent.androidtvdemoapp.Helper.MethodClass;
import app.convergent.androidtvdemoapp.Helper.SharedPrefManager;
import app.convergent.androidtvdemoapp.Helper.VolleySingleton;
import app.convergent.androidtvdemoapp.Model_Class.AdminDashboard.MyProfile;
import app.convergent.androidtvdemoapp.Model_Class.LoginModel.Userdata;
import app.convergent.androidtvdemoapp.R;

import static app.convergent.androidtvdemoapp.Helper.Constants.UNAME;
import static app.convergent.androidtvdemoapp.Helper.SharedPrefManager.KEY_TOKEN;

/*import app.convergent.androidtvapp.Helper.MethodClass;
import app.convergent.androidtvapp.R;*/

public class UserEditProfileActivity extends AppCompatActivity {

    public static Activity activity;
    private EditText uname_et, uemail_et, uphone_et;
    public String username;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_edit_profile);

        uname_et = findViewById(R.id.uname);
        uemail_et = findViewById(R.id.uemail);
        uphone_et = findViewById(R.id.uphone);

        TextView title = findViewById(R.id.user_dash_title);
        title.setText(getString(R.string.edit_user));

    }

    public void userEditProfile(View view) {

        String get_user_name = uname_et.getText().toString().trim();
        String get_user_ph = uphone_et.getText().toString().trim();

        if (get_user_name.length() == 0) {
            uname_et.setError(getString(R.string.entername));
            uname_et.requestFocus();
            return;
        }

        if (get_user_ph.length() == 0) {
            uphone_et.setError(getString(R.string.enterph));
            uphone_et.requestFocus();
            return;
        }

        String utoken = String.valueOf(SharedPrefManager.getInstance(UserEditProfileActivity.this).getUserToken(UserEditProfileActivity.this,KEY_TOKEN));

        if(MethodClass.isNetworkConnected(this)) {
            MethodClass.showProgressDialog(UserEditProfileActivity.this);

            String server_url = Constants.BASE_URL + "edit-profile";

            HashMap<String, String> params = new HashMap<String, String>();
            params.put("name", get_user_name);
            params.put("ph", get_user_ph);

            JSONObject jsonObject = MethodClass.Json_rpc_format(params);

            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    MethodClass.hideProgressDialog(UserEditProfileActivity.this);
                    Log.e("resp", response.toString());
                    try {

                        JSONObject jsonObject = MethodClass.get_result_from_webservice(UserEditProfileActivity.this, response);

                        //if no error in response
                        if (jsonObject!= null) {

                        /*GsonBuilder gsonBuilder = new GsonBuilder();
                        Gson gson = gsonBuilder.create();
                        LoginMain loginMain = gson.fromJson(response.toString(), LoginMain.class);*/

                            //getting user-profile from the response
                            JSONObject profileJson = jsonObject.getJSONObject("myprofile");

                            //creating a new user object
                            MyProfile myProfile = new MyProfile(
                                    profileJson.getInt("id"),
                                    profileJson.getString("name"),
                                    profileJson.getString("email"),
                                    profileJson.getString("user_type"),
                                    profileJson.getString("ph"),
                                    profileJson.getString("status")
                            );


                            //creating a new user object
                            Userdata user = new Userdata(
                                    profileJson.getInt("id"),
                                    profileJson.getString("name"),
                                    profileJson.getString("email"),
                                    profileJson.getString("user_type")
                            );


                            //storing the user in shared preferences
                            SharedPrefManager.getInstance(getApplicationContext()).userLogin(user);
                            String username = myProfile.getName();
                            uname_et.setText(username);

                            //updating the user in shared preferences
                            SharedPrefManager.getInstance(getApplicationContext()).userLogin(user);

                            //storing the user name
                            //SharedPrefManager.getInstance(getApplicationContext()).userName(UNAME);

                            String user_mail = myProfile.getEmail();
                            uemail_et.setText(user_mail);
                            String user_ph = myProfile.getPh();
                            uphone_et.setText(user_ph);

                            Intent I = new Intent(UserEditProfileActivity.this, UserDashBoardActivity.class);
                            startActivity(I);


                        }

                    } catch (JSONException e) {
                        MethodClass.error_alert(UserEditProfileActivity.this);
                        e.printStackTrace();
                        Log.e("change_parce", e.toString());
                    }


                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    MethodClass.hideProgressDialog(UserEditProfileActivity.this);
                    if (error.toString().contains("ConnectException")) {
                        MethodClass.network_error_alert(UserEditProfileActivity.this);
                    } else {
                        MethodClass.error_alert(UserEditProfileActivity.this);
                    }
                }

            }){
                //* Passing some request headers*
                @Override
                public Map getHeaders() throws AuthFailureError {
                    HashMap headers = new HashMap();
                    headers.put("Content-Type", "application/json");
                    headers.put("Authorization", "Bearer " + utoken);

                    Log.e("getHeaders: ", headers.toString());

                    return headers;
                }
            };

            VolleySingleton.getInstance(UserEditProfileActivity.this).addToRequestQueue(jsonObjectRequest);

        } else {

            Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), getResources().getString(R.string.no_internet_connection), Snackbar.LENGTH_SHORT);
            snackbar.show();
            return;

        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        MethodClass.setUserMenu(this);

        showUserProfile();
    }

    @SuppressLint("WrongConstant")
    public void user_home_menu(View view) {
        DrawerLayout drawer_layout = findViewById(R.id.user_drawer_layout);
        drawer_layout.openDrawer(Gravity.START);
    }

    public void showUserProfile() {

        String utoken = String.valueOf(SharedPrefManager.getInstance(UserEditProfileActivity.this).getUserToken(UserEditProfileActivity.this,KEY_TOKEN));

        if (!MethodClass.isNetworkConnected(this)) {
            Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), getResources().getString(R.string.please_check_your_internet_connection), Snackbar.LENGTH_SHORT);
            snackbar.show();
            return;
        }

        MethodClass.showProgressDialog(UserEditProfileActivity.this);

        String server_url = Constants.BASE_URL + "my-profile";

        HashMap<String, String> params = new HashMap<String, String>();

        JSONObject jsonObject = MethodClass.Json_rpc_format(params);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                MethodClass.hideProgressDialog(UserEditProfileActivity.this);
                Log.e("resp", response.toString());
                try {

                    JSONObject jsonObject = MethodClass.get_result_from_webservice(UserEditProfileActivity.this, response);

                    //if no error in response
                    if (jsonObject!= null) {
                        //Toast.makeText(getApplicationContext(), jsonObject.getString("message"), Toast.LENGTH_SHORT).show();

                        /*GsonBuilder gsonBuilder = new GsonBuilder();
                        Gson gson = gsonBuilder.create();
                        LoginMain loginMain = gson.fromJson(response.toString(), LoginMain.class);*/

                        //getting user-profile from the response
                        JSONObject profileJson = jsonObject.getJSONObject("myprofile");

                        //creating a new user object
                        MyProfile myProfile = new MyProfile(
                                profileJson.getInt("id"),
                                profileJson.getString("name"),
                                profileJson.getString("email"),
                                profileJson.getString("user_type"),
                                profileJson.getString("ph"),
                                profileJson.getString("status")
                        );

                        String user_name = myProfile.getName();
                        uname_et.setText(user_name);
                        String user_mail = myProfile.getEmail();
                        uemail_et.setText(user_mail);
                        String user_ph = myProfile.getPh();
                        uphone_et.setText(user_ph);


                    }

                } catch (JSONException e) {
                    MethodClass.error_alert(UserEditProfileActivity.this);
                    e.printStackTrace();
                    Log.e("user_edt_prof_parce", e.toString());
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                MethodClass.hideProgressDialog(UserEditProfileActivity.this);
                if (error.toString().contains("ConnectException")) {
                    MethodClass.network_error_alert(UserEditProfileActivity.this);
                } else {
                    MethodClass.error_alert(UserEditProfileActivity.this);
                }
            }

        }){
            //* Passing some request headers*
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", "Bearer " + utoken);

                Log.e("getHeaders: ", headers.toString());

                return headers;
            }
        };

        VolleySingleton.getInstance(UserEditProfileActivity.this).addToRequestQueue(jsonObjectRequest);

    }


}

