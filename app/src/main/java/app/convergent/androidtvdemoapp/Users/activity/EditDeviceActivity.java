package app.convergent.androidtvdemoapp.Users.activity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import app.convergent.androidtvdemoapp.Admin.UserEditActivity;
import app.convergent.androidtvdemoapp.Helper.Constants;
import app.convergent.androidtvdemoapp.Helper.MethodClass;
import app.convergent.androidtvdemoapp.Helper.SharedPrefManager;
import app.convergent.androidtvdemoapp.Helper.VolleySingleton;
import app.convergent.androidtvdemoapp.R;

import static app.convergent.androidtvdemoapp.Helper.SharedPrefManager.KEY_TOKEN;

/*import app.convergent.androidtvapp.Helper.MethodClass;
import app.convergent.androidtvapp.R;*/

public class EditDeviceActivity extends AppCompatActivity {

    int dv_id;
    public String dev_primary_id, device_id, device_name;
    private EditText dev_id, dev_name;
    private Button save_btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_device);

        TextView title = findViewById(R.id.user_dash_title);
        title.setText(getString(R.string.dev_edit));

        dv_id = getIntent().getIntExtra("device_primary_id",0);
        dev_primary_id = String.valueOf(dv_id);
        device_id = getIntent().getStringExtra("dev_id");
        device_name = getIntent().getStringExtra("dev_name");

        dev_id = findViewById(R.id.dev_id);
        dev_name = findViewById(R.id.dev_name);

        dev_id.setText(device_id);
        dev_name.setText(device_name);

        save_btn = findViewById(R.id.save_btn);
        save_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String get_dev_name = dev_name.getText().toString().trim();
                String get_dev_id = dev_id.getText().toString().trim();

                String utoken = String.valueOf(SharedPrefManager.getInstance(EditDeviceActivity.this).getUserToken(EditDeviceActivity.this,KEY_TOKEN));

                if (get_dev_id.length() == 0) {
                    dev_id.setError(getString(R.string.entr_dev_id));
                    dev_id.requestFocus();
                    return;
                }
                if (get_dev_name.length() == 0) {
                    dev_name.setError(getString(R.string.entr_dev_name));
                    dev_name.requestFocus();
                    return;
                }

                if(MethodClass.isNetworkConnected(EditDeviceActivity.this)) {
                    MethodClass.showProgressDialog(EditDeviceActivity.this);

                    String server_url = Constants.BASE_URL + "update-device";

                    HashMap<String, String> params = new HashMap<String, String>();
                    params.put("device_primary_id", dev_primary_id);
                    params.put("device_id", get_dev_id);
                    params.put("device_name", get_dev_name);

                    JSONObject jsonObject = MethodClass.Json_rpc_format(params);

                    JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            MethodClass.hideProgressDialog(EditDeviceActivity.this);
                            Log.e("resp", response.toString());
                            try {

                                JSONObject jsonObject = MethodClass.get_result_from_webservice(EditDeviceActivity.this, response);

                                //if no error in response
                                if (jsonObject!= null) {

                                    MethodClass.hideProgressDialog(EditDeviceActivity.this);
                                    String msg = jsonObject.getString("message");

                                    Toast.makeText(EditDeviceActivity.this, msg, Toast.LENGTH_SHORT).show();

                                }

                            } catch (JSONException e) {
                                MethodClass.error_alert(EditDeviceActivity.this);
                                e.printStackTrace();
                                Log.e("edt_dev_parce", e.toString());
                            }


                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                            MethodClass.hideProgressDialog(EditDeviceActivity.this);
                            if (error.toString().contains("ConnectException")) {
                                MethodClass.network_error_alert(EditDeviceActivity.this);
                            } else {
                                MethodClass.error_alert(EditDeviceActivity.this);
                            }
                        }

                    }){
                        //* Passing some request headers*
                        @Override
                        public Map getHeaders() throws AuthFailureError {
                            HashMap headers = new HashMap();
                            headers.put("Content-Type", "application/json");
                            headers.put("Authorization", "Bearer " + utoken);

                            Log.e("getHeaders: ", headers.toString());

                            return headers;
                        }
                    };

                    VolleySingleton.getInstance(EditDeviceActivity.this).addToRequestQueue(jsonObjectRequest);
                }
                else {

                    Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), getResources().getString(R.string.no_internet_connection), Snackbar.LENGTH_SHORT);
                    snackbar.show();
                    return;
                }

            }
        });
    }


    @Override
    protected void onResume() {
        super.onResume();
        MethodClass.setUserMenu(this);
    }

    @SuppressLint("WrongConstant")
    public void user_home_menu(View view) {
        DrawerLayout drawer_layout = findViewById(R.id.user_drawer_layout);
        drawer_layout.openDrawer(Gravity.START);
    }

}

