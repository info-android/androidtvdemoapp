package app.convergent.androidtvdemoapp.Users.activity.AssignDevices;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/*
<<<<<<< HEAD:app/src/main/java/app/convergent/androidtvdemoapp/Users/activity/DeviceListActivity.java
//import app.convergent.androidtvdemoapp.Adapter.UserItemAdapter;
=======
>>>>>>> 14edb8a3f68483498ecaa39846703ddc30871ab7:app/src/main/java/app/convergent/androidtvdemoapp/Users/activity/AssignDeviceList/AssignDeviceListActivity.java
*/

import app.convergent.androidtvdemoapp.Helper.Constants;
import app.convergent.androidtvdemoapp.Helper.MethodClass;
import app.convergent.androidtvdemoapp.Helper.SharedPrefManager;
import app.convergent.androidtvdemoapp.Helper.VolleySingleton;
import app.convergent.androidtvdemoapp.R;
import app.convergent.androidtvdemoapp.Users.Model.AssignDeviceList.AssignDevicesList;
import app.convergent.androidtvdemoapp.Users.Model.DeviceList.DeviceDetailsList;
import app.convergent.androidtvdemoapp.Users.activity.DeviceManagement.DeviceMgmtActivity;
import app.convergent.androidtvdemoapp.Users.adapter.AssignDeviceListAdapter;
import app.convergent.androidtvdemoapp.Users.adapter.DeviceManageMentAdapter;

import static app.convergent.androidtvdemoapp.Helper.SharedPrefManager.KEY_TOKEN;

public class AssignDeviceListActivity extends AppCompatActivity {

    public RecyclerView recy_view;
    private ArrayList<AssignDevicesList> assign_dev_list;
    AssignDeviceListAdapter adapter;
    CardView cardView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_assign_device_list);

        recy_view = findViewById(R.id.user_recy_view);
        cardView = findViewById(R.id.nodata_cardView);

    }

    @Override
    protected void onResume() {
        super.onResume();
        MethodClass.setUserMenu(this);

        assigned_device_list();

    }

    public void assigned_device_list() {

        String utoken = String.valueOf(SharedPrefManager.getInstance(AssignDeviceListActivity.this).getUserToken(AssignDeviceListActivity.this, KEY_TOKEN));

        if (MethodClass.isNetworkConnected(this)) {
            MethodClass.showProgressDialog(AssignDeviceListActivity.this);

            String server_url = Constants.BASE_URL + "already-assign";

            HashMap<String, String> params = new HashMap<String, String>();

            JSONObject jsonObject = MethodClass.Json_rpc_format(params);

            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, server_url, jsonObject, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    MethodClass.hideProgressDialog(AssignDeviceListActivity.this);
                    Log.e("resp", response.toString());
                    try {

                        JSONObject jsonObj = MethodClass.get_result_from_webservice(AssignDeviceListActivity.this, response);

                        //if no error in response
                        if (jsonObj != null) {
                            JSONArray deviceList_jsonArray = jsonObj.getJSONArray("message");
                            Log.e("assigned_device_array:", String.valueOf(deviceList_jsonArray));

                            assign_dev_list = new ArrayList<>();
                            assign_dev_list.clear();
                            cardView.setVisibility(View.GONE);


                            if (deviceList_jsonArray.length() != 0) {

                                for (int i = 0; i < deviceList_jsonArray.length(); i++) {

                                    AssignDevicesList assignDevicesList = new AssignDevicesList();

                                    assignDevicesList.setId(Integer.parseInt(deviceList_jsonArray.getJSONObject(i).getString("id")));
                                    assignDevicesList.setDevice_id((deviceList_jsonArray.getJSONObject(i).getString("device_id")));
                                    assignDevicesList.setDevice_name(deviceList_jsonArray.getJSONObject(i).getString("device_name"));
                                    assignDevicesList.setSet_id(deviceList_jsonArray.getJSONObject(i).getString("set_id"));
                                    assignDevicesList.setSet_name(deviceList_jsonArray.getJSONObject(i).getString("set_name"));
                                    assignDevicesList.setInterval(deviceList_jsonArray.getJSONObject(i).getString("delay_time"));

                                    assign_dev_list.add(assignDevicesList);
                                    Log.e("all_device:", String.valueOf(assign_dev_list));

                                }
                                adapter = new AssignDeviceListAdapter(AssignDeviceListActivity.this, assign_dev_list);
                                recy_view.setAdapter(adapter);
                                recy_view.setFocusable(false);
                                adapter.notifyDataSetChanged();


                            } else if(deviceList_jsonArray.length() == 0) {

                                cardView.setVisibility(View.VISIBLE);

                            }
                            else {

                                if (recy_view.getAdapter()!=null){
                                    adapter.update(assign_dev_list);
                                    recy_view.getAdapter().notifyDataSetChanged();

                                    recy_view.setVisibility(View.GONE);
                                    cardView.setVisibility(View.VISIBLE);
                                }
                            }

                        }

                    } catch (JSONException e) {
                        MethodClass.error_alert(AssignDeviceListActivity.this);
                        e.printStackTrace();
                        Log.e("devicelist_parce", e.toString());
                    }


                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    MethodClass.hideProgressDialog(AssignDeviceListActivity.this);
                    if (error.toString().contains("ConnectException")) {
                        MethodClass.network_error_alert(AssignDeviceListActivity.this);
                    } else {
                        MethodClass.error_alert(AssignDeviceListActivity.this);
                        Log.e("devicelist_parce2", error.toString());
                    }
                }

            }) {
                //* Passing some request headers*
                @Override
                public Map getHeaders() throws AuthFailureError {
                    HashMap headers = new HashMap();
                    headers.put("Content-Type", "application/json");
                    headers.put("Authorization", "Bearer " + utoken);

                    Log.e("getHeaders: ", headers.toString());

                    return headers;
                }
            };

            VolleySingleton.getInstance(AssignDeviceListActivity.this).addToRequestQueue(jsonObjectRequest);


        } else {

            Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), getResources().getString(R.string.no_internet_connection), Snackbar.LENGTH_SHORT);
            snackbar.show();
            return;

        }

    }

    public void uadd_dev(View view) {

    }

    @SuppressLint("WrongConstant")
    public void user_home_menu(View view) {
        DrawerLayout drawer_layout = findViewById(R.id.user_drawer_layout);
        drawer_layout.openDrawer(Gravity.START);
    }

}