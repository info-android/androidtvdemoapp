package app.convergent.androidtvdemoapp.Users.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import app.convergent.androidtvdemoapp.Users.adapter.DeviceManageMentAdapter;
import app.convergent.androidtvdemoapp.Helper.Constants;
import app.convergent.androidtvdemoapp.Helper.MethodClass;
import app.convergent.androidtvdemoapp.Helper.SharedPrefManager;
import app.convergent.androidtvdemoapp.Helper.VolleySingleton;
import app.convergent.androidtvdemoapp.R;
import app.convergent.androidtvdemoapp.Users.Model.DeviceList.DeviceDetailsList;

import static app.convergent.androidtvdemoapp.Helper.SharedPrefManager.KEY_TOKEN;

/*import app.convergent.androidtvapp.Adapter.UserItemAdapter;
import app.convergent.androidtvapp.Helper.MethodClass;
import app.convergent.androidtvapp.R;*/

public class UserHomeActivity extends AppCompatActivity {

    public RecyclerView user_recy_view;
    private ArrayList<DeviceDetailsList> device_list = new ArrayList<>();
    DeviceManageMentAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_home);

        user_recy_view=findViewById(R.id.user_recy_view);

        //user_list();
    }

    public void user_list() {

        String utoken = String.valueOf(SharedPrefManager.getInstance(UserHomeActivity.this).getUserToken(UserHomeActivity.this,KEY_TOKEN));

        if(MethodClass.isNetworkConnected(this)) {
            MethodClass.showProgressDialog(UserHomeActivity.this);

            String server_url = Constants.BASE_URL + "all-device";

            HashMap<String, String> params = new HashMap<String, String>();

            JSONObject jsonObject = MethodClass.Json_rpc_format(params);

            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, server_url, jsonObject, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    MethodClass.hideProgressDialog(UserHomeActivity.this);
                    Log.e("resp", response.toString());
                    try {

                        JSONObject jsonObj = MethodClass.get_result_from_webservice(UserHomeActivity.this, response);

                        //if no error in response
                        if (jsonObj!= null) {
                            JSONArray device_jsonArray = jsonObj.getJSONArray("see_device");
                            Log.e("device_array:", String.valueOf(device_jsonArray));

                            if (device_jsonArray.length() != 0) {
                                Log.e("device_jsonArray", String.valueOf(device_jsonArray.length()));

                                device_list.clear();

                                for (int i = 0; i < device_jsonArray.length(); i++) {

                                    DeviceDetailsList deviceDetailsList = new DeviceDetailsList();

                                    deviceDetailsList.setId(Integer.parseInt(device_jsonArray.getJSONObject(i).getString("id")));
                                    deviceDetailsList.setDevice_id((device_jsonArray.getJSONObject(i).getString("device_id")));
                                    deviceDetailsList.setUser_id(Integer.parseInt(device_jsonArray.getJSONObject(i).getString("user_id")));
                                    deviceDetailsList.setDevice_name(device_jsonArray.getJSONObject(i).getString("device_name"));
                                    deviceDetailsList.setDevice_status(device_jsonArray.getJSONObject(i).getString("status"));

                                    device_list.add(deviceDetailsList);
                                    Log.e("all_user:", String.valueOf(device_list));

                                }


                                adapter = new DeviceManageMentAdapter(UserHomeActivity.this,device_list);
                                user_recy_view.setAdapter(adapter);
                                user_recy_view.setFocusable(false);
                                adapter.notifyDataSetChanged();
                            }

                        }

                    } catch (JSONException e) {
                        MethodClass.error_alert(UserHomeActivity.this);
                        e.printStackTrace();
                        Log.e("user_parce", e.toString());
                    }


                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    MethodClass.hideProgressDialog(UserHomeActivity.this);
                    if (error.toString().contains("ConnectException")) {
                        MethodClass.network_error_alert(UserHomeActivity.this);
                    } else {
                        MethodClass.error_alert(UserHomeActivity.this);
                        Log.e("user_parce2", error.toString());
                    }
                }

            }){
                //* Passing some request headers*
                @Override
                public Map getHeaders() throws AuthFailureError {
                    HashMap headers = new HashMap();
                    headers.put("Content-Type", "application/json");
                    headers.put("Authorization", "Bearer " + utoken);

                    Log.e("getHeaders: ", headers.toString());

                    return headers;
                }
            };

            VolleySingleton.getInstance(UserHomeActivity.this).addToRequestQueue(jsonObjectRequest);

        }
        else {

            Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), getResources().getString(R.string.no_internet_connection), Snackbar.LENGTH_SHORT);
            snackbar.show();
            return;

        }

        /*ArrayList<HashMap<String,String>> arrayList=new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            HashMap<String,String> hashMap=new HashMap<>();
            hashMap.put("key","value");
            arrayList.add(hashMap);
        }

        UserItemAdapter userItemAdapter = new UserItemAdapter(this,arrayList);
        user_recy_view.setAdapter(userItemAdapter);
        user_recy_view.setFocusable(false);*/

    }

    public void uadd_member(View view) {

        Intent intent = new Intent(UserHomeActivity.this, AddDeviceActivity.class);
        startActivity(intent);

    }

    @Override
    protected void onResume() {
        super.onResume();
        MethodClass.setUserMenu(this);

        user_list();

    }

    @SuppressLint("WrongConstant")
    public void user_home_menu(View view) {
        DrawerLayout drawer_layout = findViewById(R.id.user_drawer_layout);
        drawer_layout.openDrawer(Gravity.START);
    }

}

