package app.convergent.androidtvdemoapp.Users.activity.URL_Management;

import static app.convergent.androidtvdemoapp.Helper.SharedPrefManager.KEY_TOKEN;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/*import app.convergent.androidtvapp.Adapter.URLAdapter;
import app.convergent.androidtvapp.Helper.MethodClass;
import app.convergent.androidtvapp.R;*/
import app.convergent.androidtvdemoapp.Helper.Constants;
import app.convergent.androidtvdemoapp.Helper.MethodClass;
import app.convergent.androidtvdemoapp.Helper.SharedPrefManager;
import app.convergent.androidtvdemoapp.Helper.VolleySingleton;
import app.convergent.androidtvdemoapp.R;
import app.convergent.androidtvdemoapp.Users.Model.SetListModel.SeeSetWithUrlsItem;
import app.convergent.androidtvdemoapp.Users.Model.SetListModel.SetListResponse;
import app.convergent.androidtvdemoapp.Users.Model.UrlListModel.SetToUrlItem;
import app.convergent.androidtvdemoapp.Users.Model.UrlListModel.UrlListResponse;
import app.convergent.androidtvdemoapp.Users.adapter.SetAdapter;
import app.convergent.androidtvdemoapp.Users.adapter.URLAdapter;
import app.convergent.androidtvdemoapp.Users.helper.DragItemTouchHelper;
import de.hdodenhof.circleimageview.CircleImageView;

public class URLManagement extends AppCompatActivity {

    public RecyclerView rv_url;
    CircleImageView addURL_fav;
    TextView setName_tv;
    Activity activity;

    boolean fetchAll = false;
    private ItemTouchHelper mItemTouchHelper;

    public ArrayList<SetToUrlItem> list;
    URLAdapter urlAdapter;
    CardView cardView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_set);
        activity = this;

        rv_url = findViewById(R.id.rv_url);
        rv_url.setHasFixedSize(true);

        setName_tv = findViewById(R.id.setName_tv);
        setName_tv.setText(getIntent().getStringExtra("set_name"));
        addURL_fav = findViewById(R.id.addURL_fav);
        cardView = findViewById(R.id.nodata_cardView);

        // fetchAll = getIntent().getBooleanExtra("fetch_all_url", false);


        addURL_fav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(activity, Add_Edit_URL.class);
                intent.putExtra("type", "new");
                intent.putExtra("url_header", "Add URL");
                intent.putExtra("set_id", getIntent().getStringExtra("set_id"));
                intent.putExtra("set_name", getIntent().getStringExtra("set_name"));
                activity.startActivity(intent);


            }
        });


    }


    @Override
    protected void onResume() {
        super.onResume();
        MethodClass.setUserMenu(this);
        getUrlList();


    }

    @SuppressLint("WrongConstant")
    public void user_home_menu(View view) {
        DrawerLayout drawer_layout = findViewById(R.id.user_drawer_layout);
        drawer_layout.openDrawer(Gravity.START);
    }

    public void getUrlList() {


        if (!MethodClass.isNetworkConnected(this)) {
            Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content),
                    getResources().getString(R.string.please_check_your_internet_connection), Snackbar.LENGTH_SHORT);
            snackbar.show();
            return;
        }

        MethodClass.showProgressDialog(activity);

        String server_url = Constants.BASE_URL + "get-single-set-url";

        HashMap<String, String> params = new HashMap<String, String>();
        params.put("parent_set_id", getIntent().getStringExtra("set_id"));
        JSONObject jsonObject = MethodClass.Json_rpc_format(params);


        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(fetchAll ? Request.Method.GET : Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                MethodClass.hideProgressDialog(activity);

                try {

                    JSONObject jsonObject = MethodClass.get_result_from_webservice(activity, response);


                    if (jsonObject != null) {


                        UrlListResponse model
                                = new Gson().fromJson(response.toString(), UrlListResponse.class);

                        list = new ArrayList<>();
                        list.clear();
                        list.addAll(model.getResult().getSeeSetWithUrls().getSetToUrl());
                        cardView.setVisibility(View.GONE);
                        rv_url.setVisibility(View.VISIBLE);

                        if(model.getResult().getSeeSetWithUrls().getSetToUrl().isEmpty()) {

                            rv_url.setVisibility(View.GONE);
                            cardView.setVisibility(View.VISIBLE);
                        }

                        if (urlAdapter == null) {
                            urlAdapter = new URLAdapter(activity, list);
                            rv_url.setAdapter(urlAdapter);


                            urlAdapter.setOnItemClickListener(new URLAdapter.OnItemClickListener() {
                                @Override
                                public void onItemClick(View view, SeeSetWithUrlsItem obj, int position) {
                                    Snackbar.make(view, "Item " + obj.getSetName() + " clicked", Snackbar.LENGTH_SHORT).show();

                                }
                            });

                            urlAdapter.setDragListener(new URLAdapter.OnStartDragListener() {
                                @Override
                                public void onStartDrag(RecyclerView.ViewHolder viewHolder) {
                                    mItemTouchHelper.startDrag(viewHolder);
                                }
                            });

                            urlAdapter.dragComplete(new URLAdapter.OnDragComplete() {
                                                        @Override
                                                        public void onCompleteDrag(String id, String to_pos) {
                                                            Log.d("onCompleteDrag", "from_order_id " + id + " to position " + to_pos);
                                                            swipe_Url(id, to_pos);
                                                        }
                                                    }
                            );

                            ItemTouchHelper.Callback callback = new DragItemTouchHelper(urlAdapter);
                            mItemTouchHelper = new ItemTouchHelper(callback);
                            mItemTouchHelper.attachToRecyclerView(rv_url);

                        } else {
                            urlAdapter.add_item(list);
                            urlAdapter.notifyDataSetChanged();

                        }


                    }

                } catch (Exception e) {
                    MethodClass.error_alert(activity);
                    e.printStackTrace();

                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {


                MethodClass.hideProgressDialog(activity);
                if (error.toString().contains("ConnectException")) {
                    MethodClass.network_error_alert(activity);
                } else {
                    MethodClass.error_alert(activity);
                }
            }
        }) {
            // Passing some request headers
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", "Bearer " + String.valueOf(SharedPrefManager.getInstance(activity).getUserToken(activity, KEY_TOKEN)));

                Log.e("getHeaders: ", headers.toString());

                return headers;
            }
        };

        VolleySingleton.getInstance(activity).addToRequestQueue(jsonObjectRequest);

    }

    public void swipe_Url(String order_id, String swipe_to) {


        if (!MethodClass.isNetworkConnected(activity)) {
            Snackbar snackbar = Snackbar.make(activity.findViewById(android.R.id.content), activity.getResources().getString(R.string.please_check_your_internet_connection), Snackbar.LENGTH_SHORT);
            snackbar.show();
            return;
        }

        MethodClass.showProgressDialog(activity);

        String server_url = Constants.BASE_URL + "url-swap";

        HashMap<String, String> params = new HashMap<String, String>();
        params.put("parent_set_id", getIntent().getStringExtra("set_id"));
        params.put("from", order_id);
        params.put("to", swipe_to);
        JSONObject jsonObject = MethodClass.Json_rpc_format(params);

        Log.e("swiping-hashmap", jsonObject.toString());

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                MethodClass.hideProgressDialog(activity);
                Log.e("resp", response.toString());
                Log.e("swiping-response", response.toString());
                try {

                    JSONObject jsonObject = MethodClass.get_result_from_webservice(activity, response);


                    if (jsonObject != null) {

                        getUrlList();

                        SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(activity, SweetAlertDialog.SUCCESS_TYPE);
                        if (sweetAlertDialog.isShowing()) {
                            sweetAlertDialog.dismissWithAnimation();
                        }

                        sweetAlertDialog.setTitleText(jsonObject.getString("message"));
                        sweetAlertDialog.setContentText(jsonObject.getString("message"));
                        sweetAlertDialog.setConfirmText("Ok");
                        sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();

                            }
                        });
                        sweetAlertDialog.setCancelable(false);
                        sweetAlertDialog.setCanceledOnTouchOutside(false);
                        sweetAlertDialog.show();


                    } else getUrlList();

                } catch (JSONException e) {
                    MethodClass.error_alert(activity);
                    e.printStackTrace();
                    Log.e("login_parce", e.toString());
                    Log.e("swiping-exception", e.toString());

                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("swiping-error", error.toString());
                getUrlList();
                MethodClass.hideProgressDialog(activity);
                if (error.toString().contains("ConnectException")) {
                    MethodClass.network_error_alert(activity);
                } else {
                    MethodClass.error_alert(activity);
                }
            }
        }) {
            // Passing some request headers
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", "Bearer " + String.valueOf(SharedPrefManager.getInstance(activity).getUserToken(activity, KEY_TOKEN)));

                Log.e("getHeaders: ", headers.toString());

                return headers;
            }
        };

        VolleySingleton.getInstance(activity).addToRequestQueue(jsonObjectRequest);

    }

}