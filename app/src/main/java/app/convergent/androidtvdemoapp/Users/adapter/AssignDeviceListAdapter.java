package app.convergent.androidtvdemoapp.Users.adapter;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.material.snackbar.Snackbar;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;

import org.json.JSONException;
import org.json.JSONObject;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import app.convergent.androidtvdemoapp.Helper.Constants;
import app.convergent.androidtvdemoapp.Helper.MethodClass;
import app.convergent.androidtvdemoapp.Helper.SharedPrefManager;
import app.convergent.androidtvdemoapp.Helper.VolleySingleton;
import app.convergent.androidtvdemoapp.R;
import app.convergent.androidtvdemoapp.Users.Model.AssignDeviceList.AssignDevicesList;
import app.convergent.androidtvdemoapp.Users.Model.DeviceList.DeviceDetailsList;
import app.convergent.androidtvdemoapp.Users.activity.AssignDevices.AssignDeviceListActivity;
import app.convergent.androidtvdemoapp.Users.activity.AssignDevices.AssignDevicesActivity;
import app.convergent.androidtvdemoapp.Users.activity.DeviceManagement.DeviceMgmtActivity;
import app.convergent.androidtvdemoapp.Users.activity.EditDeviceActivity;
import de.hdodenhof.circleimageview.CircleImageView;

import static app.convergent.androidtvdemoapp.Helper.SharedPrefManager.KEY_TOKEN;

public class AssignDeviceListAdapter extends RecyclerView.Adapter<AssignDeviceListAdapter.ViewHolder> {

    Activity activity;
    public ArrayList<AssignDevicesList> map_list;
    public String utoken;

    public AssignDeviceListAdapter(Activity activity, ArrayList<AssignDevicesList> map_list) {
        this.activity = activity;
        this.map_list = map_list;

    }

    public void update(ArrayList<AssignDevicesList> deviceDetailsLists) {
        this.map_list = deviceDetailsLists;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public AssignDeviceListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.assigned_device, parent, false);
        return new AssignDeviceListAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AssignDeviceListAdapter.ViewHolder holder, int position) {

        final AssignDevicesList assignDevice_list_item = map_list.get(position);

        if (assignDevice_list_item.getSet_name() == null || assignDevice_list_item.getSet_name().equals("null")) {
            holder.set_name_tv.setText("");
            holder.dev_name_tv.setText(assignDevice_list_item.getDevice_name());
        } else if (assignDevice_list_item.getDevice_name() == null || assignDevice_list_item.getDevice_name().equals("null")) {
            holder.set_name_tv.setText(assignDevice_list_item.getSet_name());
            holder.dev_name_tv.setText("");
        } else {
            holder.set_name_tv.setText(assignDevice_list_item.getSet_name());
            holder.dev_name_tv.setText(assignDevice_list_item.getDevice_name());

        }

        int id = assignDevice_list_item.getId();
        String set_id = assignDevice_list_item.getSet_id();
        String set_name = assignDevice_list_item.getSet_name();
        String dev_id = assignDevice_list_item.getDevice_id();
        String dev_name = assignDevice_list_item.getDevice_name();
        String delay_time = assignDevice_list_item.getInterval();
        String from_acty = "dev_list";
        utoken = String.valueOf(SharedPrefManager.getInstance(activity).getUserToken(activity, KEY_TOKEN));

        holder.edtAssignDev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(activity, AssignDevicesActivity.class);
                intent.putExtra("type", "edit");
                intent.putExtra("from", from_acty);
                intent.putExtra("assign_id", "" + id);
                intent.putExtra("set_id", set_id);
                intent.putExtra("set_name", set_name);
                intent.putExtra("dev_id", dev_id);
                intent.putExtra("dev_name", dev_name);
                intent.putExtra("interval", delay_time);
                activity.startActivity(intent);

            }
        });


        holder.delAssignDev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(activity, SweetAlertDialog.WARNING_TYPE);
                if (sweetAlertDialog.isShowing()) {
                    sweetAlertDialog.dismissWithAnimation();
                }

                sweetAlertDialog.setTitleText("Delete Device");
                sweetAlertDialog.setContentText("Are you sure you want to delete this assign device?");
                sweetAlertDialog.setConfirmText("Ok");
                sweetAlertDialog.setCancelText("No");
                sweetAlertDialog.setCancelClickListener(null);

                sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();
                        deleteAssignDevice(String.valueOf(id));
                    }
                });
                sweetAlertDialog.setCancelable(true);
                sweetAlertDialog.setCanceledOnTouchOutside(true);
                sweetAlertDialog.show();

            }
        });

    }

    @Override
    public int getItemCount() {
        return map_list == null ? 0 : map_list.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private CircleImageView edtAssignDev, delAssignDev;
        private TextView set_name_tv, dev_name_tv;

        public ViewHolder(@NonNull View itemView) {

            super(itemView);

            edtAssignDev = itemView.findViewById(R.id.img_edit_dev);
            delAssignDev = itemView.findViewById(R.id.img_del_dev);
            set_name_tv = itemView.findViewById(R.id.tv_set_name);
            dev_name_tv = itemView.findViewById(R.id.tv_dev_name);

        }
    }

    public void deleteAssignDevice(String assignId) {

        if (!MethodClass.isNetworkConnected(activity)) {
            Snackbar snackbar = Snackbar.make(activity.findViewById(android.R.id.content), activity.getResources().getString(R.string.please_check_your_internet_connection), Snackbar.LENGTH_SHORT);
            snackbar.show();
            return;
        }
        MethodClass.showProgressDialog(activity);
        String server_del_url = Constants.BASE_URL + "delete-assign-device";
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("assign_id", String.valueOf(assignId));

        JSONObject jsonObject = MethodClass.Json_rpc_format(params);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_del_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                MethodClass.hideProgressDialog(activity);
                Log.e("resp", response.toString());
                try {

                    JSONObject jsonObject = MethodClass.get_result_from_webservice(activity, response);

                    //if no error in response
                    if (jsonObject != null) {

                        MethodClass.hideProgressDialog(activity);
                        String msg = jsonObject.getString("message");
                        ((AssignDeviceListActivity) activity).assigned_device_list();

                        Toast.makeText(activity, msg, Toast.LENGTH_SHORT).show();

                    }

                } catch (JSONException e) {
                    MethodClass.error_alert(activity);
                    e.printStackTrace();
                    Log.e("del_assign_dev_parce", e.toString());
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                MethodClass.hideProgressDialog(activity);
                if (error.toString().contains("ConnectException")) {
                    MethodClass.network_error_alert(activity);
                } else {
                    MethodClass.error_alert(activity);
                }
            }

        }) {
            //* Passing some request headers*
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", "Bearer " + utoken);

                Log.e("getHeaders: ", headers.toString());

                return headers;
            }
        };

        VolleySingleton.getInstance(activity).addToRequestQueue(jsonObjectRequest);

    }
}
