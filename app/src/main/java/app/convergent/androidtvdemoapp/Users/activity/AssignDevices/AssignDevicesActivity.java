package app.convergent.androidtvdemoapp.Users.activity.AssignDevices;

import static app.convergent.androidtvdemoapp.Helper.SharedPrefManager.KEY_TOKEN;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import app.convergent.androidtvdemoapp.Adapter.DevicesAdapter;
import app.convergent.androidtvdemoapp.Helper.Constants;
import app.convergent.androidtvdemoapp.Helper.MethodClass;
import app.convergent.androidtvdemoapp.Helper.SharedPrefManager;
import app.convergent.androidtvdemoapp.Helper.VolleySingleton;
import app.convergent.androidtvdemoapp.R;
import app.convergent.androidtvdemoapp.Users.Model.DeviceList.DeviceDetailsList;
import app.convergent.androidtvdemoapp.Users.Model.SetListModel.SeeSetWithUrlsItem;
import app.convergent.androidtvdemoapp.Users.Model.SetListModel.SetListResponse;
import app.convergent.androidtvdemoapp.Users.Model.UrlListModel.UrlListResponse;
import app.convergent.androidtvdemoapp.Users.adapter.AssignDeviceURLAdapter;

/*import app.convergent.androidtvapp.Adapter.AssignAdapter;
import app.convergent.androidtvapp.Adapter.DevicesAdapter;
import app.convergent.androidtvapp.Helper.MethodClass;
import app.convergent.androidtvapp.R;*/

public class AssignDevicesActivity extends AppCompatActivity {

    Spinner set_spin;
    public RecyclerView rv_url;
    TextView tvDeviceSelect, delay;
    private ImageView down_arw;
    LinearLayout set_spin_for;
    private SeekBar delay_interval;
    Activity activity;
    ArrayList<String> set_list;
    Button submit_btn;

    private ArrayList<DeviceDetailsList> device_list = new ArrayList<>();
    public static ArrayList<DeviceDetailsList> selectedDevicesArr = new ArrayList<>();
    AlertDialog alertDialog;
    String selected_set_id = "";
    String time_interval = "0";
    String from = "", assign_id = "", set_name = "", set_id = "", dev_id = "", dev_name = "";

    String type = "add";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_assign_devices);

        activity = this;

        TextView title = findViewById(R.id.user_dash_title);
        title.setText(getString(R.string.assign_device));

        set_spin = findViewById(R.id.set_spin);
        submit_btn = findViewById(R.id.submit_btn);
        set_spin_for = findViewById(R.id.spin_set_for);

        delay = findViewById(R.id.delay);
        delay_interval = findViewById(R.id.delay_time);

        rv_url = findViewById(R.id.rv_url);
        tvDeviceSelect = findViewById(R.id.tvDeviceSelect);

        findViewById(R.id.img_all).setOnClickListener(v -> MethodClass.go_to_next_activity(activity, AssignDeviceListActivity.class));


        if (getIntent().hasExtra("type")) {
            if (getIntent().getStringExtra("type").equalsIgnoreCase("edit")) {
                type = getIntent().getStringExtra("type");
                from = getIntent().getStringExtra("from");
                assign_id = getIntent().getStringExtra("assign_id");
                set_id = getIntent().getStringExtra("set_id");
                selected_set_id = getIntent().getStringExtra("set_id");
                set_name = getIntent().getStringExtra("set_name");
                dev_id = getIntent().getStringExtra("dev_id");
                dev_name = getIntent().getStringExtra("dev_name");
                time_interval = getIntent().getStringExtra("interval");
                delay_interval.setProgress(Integer.parseInt(time_interval));
                delay.setText(time_interval + "Sec");

                if(dev_name.trim().equalsIgnoreCase("null")) {
                    tvDeviceSelect.setText("Device not found!");
                }
                else {
                    tvDeviceSelect.setText(dev_name);
                }
            }
        } else {
            xmlClick();
            device_list();
        }


        submit_btn.setOnClickListener(v ->
                {
                    if (type.equalsIgnoreCase("edit"))
                        editDevice();
                    else
                        assignDevice();

                }
        );


        delay_interval.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                int val = (progress * (seekBar.getWidth() - 6 * seekBar.getThumbOffset())) / seekBar.getMax();
                time_interval = "" + progress;
                delay.setText(progress + "sec");
                delay.setX(seekBar.getX() + val + seekBar.getThumbOffset() / 2);
                delay.setPaddingRelative(0, 10, 10, 0);

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });


        set_spin_for.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //if(set_spin.getSelectedItem() == null) {
                set_spin.performClick();
                //}
            }
        });


        rv_url.setFocusable(false);

        getSetList();


    }

    private void xmlClick() {


        tvDeviceSelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(AssignDevicesActivity.this);
                LayoutInflater inflater = getLayoutInflater();
                View dialogView = inflater.inflate(R.layout.devices_list_dialog, null);

                RecyclerView menuList = dialogView.findViewById(R.id.lvTableList);
                Button btnAddDevices = dialogView.findViewById(R.id.btnAddDevices);
                ImageView closeDialog_iv = dialogView.findViewById(R.id.closeDialog_iv);


                DevicesAdapter devicesAdapter = new DevicesAdapter(AssignDevicesActivity.this, device_list);
                menuList.setAdapter(devicesAdapter);
                menuList.setFocusable(false);

                tvDeviceSelect.setText("Select Devices");

                closeDialog_iv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        alertDialog.dismiss();
                    }
                });

                btnAddDevices.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        String selectedTable = "";
                        String delimeter = " , ";

                        for (int i = 0; i < selectedDevicesArr.size(); i++) {

                            /*selectedTable = selectedTable + selectedDevicesArr.get(i);*/

                            if (i == selectedDevicesArr.size() - 1) {
                                selectedTable = selectedTable + selectedDevicesArr.get(i).getDevice_name();
                            } else
                                selectedTable = selectedTable + selectedDevicesArr.get(i).getDevice_name() + delimeter;


                        }
                        if (selectedDevicesArr.size() > 0)
                            tvDeviceSelect.setText(selectedTable);

                        alertDialog.hide();

                    }
                });

                dialogBuilder.setView(dialogView);
                alertDialog = dialogBuilder.create();
                alertDialog.show();
            }
        });

    }


    public void device_list() {

        String utoken = String.valueOf(SharedPrefManager.getInstance(activity).getUserToken(activity, KEY_TOKEN));

        if (MethodClass.isNetworkConnected(this)) {
            MethodClass.showProgressDialog(activity);

            String server_url = Constants.BASE_URL + "all-device";

            HashMap<String, String> params = new HashMap<String, String>();

            JSONObject jsonObject = MethodClass.Json_rpc_format(params);

            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, server_url, jsonObject, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    MethodClass.hideProgressDialog(activity);
                    Log.e("resp", response.toString());
                    try {

                        JSONObject jsonObj = MethodClass.get_result_from_webservice(activity, response);

                        //if no error in response
                        if (jsonObj != null) {
                            JSONArray device_jsonArray = jsonObj.getJSONArray("see_device");
                            Log.e("user_array:", String.valueOf(device_jsonArray));

                            if (device_jsonArray.length() != 0) {
                                Log.e("user_jsonArray", String.valueOf(device_jsonArray.length()));

                                device_list.clear();

                                for (int i = 0; i < device_jsonArray.length(); i++) {

                                    DeviceDetailsList deviceDetailsList = new DeviceDetailsList();

                                    deviceDetailsList.setId(Integer.parseInt(device_jsonArray.getJSONObject(i).getString("id")));
                                    deviceDetailsList.setDevice_id((device_jsonArray.getJSONObject(i).getString("device_id")));
                                    deviceDetailsList.setUser_id(Integer.parseInt(device_jsonArray.getJSONObject(i).getString("user_id")));
                                    deviceDetailsList.setDevice_name(device_jsonArray.getJSONObject(i).getString("device_name"));
                                    deviceDetailsList.setDevice_status(device_jsonArray.getJSONObject(i).getString("status"));

                                    device_list.add(deviceDetailsList);
                                    Log.e("all_user:", String.valueOf(device_list));

                                }


                            }

                        }

                    } catch (JSONException e) {
                        MethodClass.error_alert(activity);
                        e.printStackTrace();
                        Log.e("user_parce", e.toString());
                    }


                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    MethodClass.hideProgressDialog(activity);
                    if (error.toString().contains("ConnectException")) {
                        MethodClass.network_error_alert(activity);
                    } else {
                        MethodClass.error_alert(activity);
                        Log.e("user_parce2", error.toString());
                    }
                }

            }) {
                //* Passing some request headers*
                @Override
                public Map getHeaders() throws AuthFailureError {
                    HashMap headers = new HashMap();
                    headers.put("Content-Type", "application/json");
                    headers.put("Authorization", "Bearer " + utoken);

                    Log.e("getHeaders: ", headers.toString());

                    return headers;
                }
            };

            VolleySingleton.getInstance(activity).addToRequestQueue(jsonObjectRequest);

        } else {

            Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), getResources().getString(R.string.no_internet_connection), Snackbar.LENGTH_SHORT);
            snackbar.show();
            return;

        }

        /*ArrayList<HashMap<String,String>> arrayList=new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            HashMap<String,String> hashMap=new HashMap<>();
            hashMap.put("key","value");
            arrayList.add(hashMap);
        }

        adapter = new UserItemAdapter(this,arrayList);
        recy_view.setAdapter(adapter);
        recy_view.setFocusable(false);*/

    }


    public void getSetList() {


        if (!MethodClass.isNetworkConnected(this)) {
            Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content),
                    getResources().getString(R.string.please_check_your_internet_connection), Snackbar.LENGTH_SHORT);
            snackbar.show();
            return;
        }

        MethodClass.showProgressDialog(activity);

        String server_url = Constants.BASE_URL + "set-list";

        HashMap<String, String> params = new HashMap<String, String>();
        //  params.put("set_name", tv_setname.getText().toString());


        JSONObject jsonObject = MethodClass.Json_rpc_format(params);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                MethodClass.hideProgressDialog(activity);
                Log.e("resp", response.toString());
                try {

                    JSONObject jsonObject = MethodClass.get_result_from_webservice(activity, response);


                    if (jsonObject != null) {


                        SetListResponse model
                                = new Gson().fromJson(response.toString(), SetListResponse.class);


                        set_list = new ArrayList<>();
                        set_list.add("Select set");
                        for (SeeSetWithUrlsItem set : model.getResult().getSeeSetWithUrls()) {
                            set_list.add(set.getSetName());
                        }


                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(activity,
                                android.R.layout.simple_spinner_item, set_list);//setting the country_array to spinner
                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        set_spin.setAdapter(adapter);


                        //set value of assign set
                        if (!set_name.isEmpty()) {
                            int spinnerPosition = adapter.getPosition(set_name);
                            set_spin.setSelection(spinnerPosition);
                        }

                        //if you want to set any action you can do in this listener
                        set_spin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                                       int position, long id) {
                                if (position != 0) {
                                    //  Toast.makeText(activity, "" + model.getResult().getSeeSetWithUrls().get(position - 1).getSetName(), Toast.LENGTH_SHORT).show();
                                    getUrlList(model.getResult().getSeeSetWithUrls().get(position - 1).getSetId());
                                    selected_set_id = model.getResult().getSeeSetWithUrls().get(position - 1).getSetId();
                                }
                                /*else {
                                    Toast.makeText(AssignDevicesActivity.this,"Select set!",Toast.LENGTH_SHORT).show();
                                }*/


                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> arg0) {
                            }
                        });


                    }

                } catch (Exception e) {
                    MethodClass.error_alert(activity);
                    e.printStackTrace();
                    Log.e("login_parce", e.toString());
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                MethodClass.hideProgressDialog(activity);
                if (error.toString().contains("ConnectException")) {
                    MethodClass.network_error_alert(activity);
                } else {
                    MethodClass.error_alert(activity);
                }
            }
        }) {
            // Passing some request headers
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", "Bearer " + String.valueOf(SharedPrefManager.getInstance(activity).getUserToken(activity, KEY_TOKEN)));

                Log.e("getHeaders: ", headers.toString());

                return headers;
            }
        };

        VolleySingleton.getInstance(activity).addToRequestQueue(jsonObjectRequest);

    }

    public void getUrlList(String setid) {


        if (!MethodClass.isNetworkConnected(this)) {
            Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content),
                    getResources().getString(R.string.please_check_your_internet_connection), Snackbar.LENGTH_SHORT);
            snackbar.show();
            return;
        }

        MethodClass.showProgressDialog(activity);

        String server_url = Constants.BASE_URL + "get-single-set-url";

        HashMap<String, String> params = new HashMap<String, String>();
        params.put("parent_set_id", setid);


        JSONObject jsonObject = MethodClass.Json_rpc_format(params);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                MethodClass.hideProgressDialog(activity);
                Log.e("resp", response.toString());
                try {

                    JSONObject jsonObject = MethodClass.get_result_from_webservice(activity, response);


                    if (jsonObject != null) {


                        UrlListResponse model
                                = new Gson().fromJson(response.toString(), UrlListResponse.class);

                        AssignDeviceURLAdapter adapter = new AssignDeviceURLAdapter(activity, model.getResult().getSeeSetWithUrls().getSetToUrl());
                        rv_url.setAdapter(adapter);

                    }

                } catch (Exception e) {
                    MethodClass.error_alert(activity);
                    e.printStackTrace();
                    Log.e("login_parce", e.toString());
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                MethodClass.hideProgressDialog(activity);
                if (error.toString().contains("ConnectException")) {
                    MethodClass.network_error_alert(activity);
                } else {
                    MethodClass.error_alert(activity);
                }
            }
        }) {
            // Passing some request headers
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", "Bearer " + String.valueOf(SharedPrefManager.getInstance(activity).getUserToken(activity, KEY_TOKEN)));

                Log.e("getHeaders: ", headers.toString());

                return headers;
            }
        };

        VolleySingleton.getInstance(activity).addToRequestQueue(jsonObjectRequest);

    }

    public void assignDevice() {


        if (selected_set_id.isEmpty()) {
            Toast.makeText(activity, "Please select set", Toast.LENGTH_SHORT).show();
            return;
        }

        if (time_interval.isEmpty() || time_interval.equalsIgnoreCase("0")) {
            Toast.makeText(activity, "Please select interval", Toast.LENGTH_SHORT).show();
            return;
        }

        if (rv_url.getAdapter() == null || rv_url.getAdapter().getItemCount() == 0) {
            Toast.makeText(activity, "No url available for selected set", Toast.LENGTH_SHORT).show();
            return;
        }


        if (!MethodClass.isNetworkConnected(activity)) {
            Snackbar snackbar = Snackbar.make(activity.findViewById(android.R.id.content), activity.getResources().getString(R.string.please_check_your_internet_connection), Snackbar.LENGTH_SHORT);
            snackbar.show();
            return;
        }

        MethodClass.showProgressDialog(activity);

        String server_url = Constants.BASE_URL + "device-assing";

        /*HashMap<String, Object> main_param = new HashMap<String, Object>();
        main_param.put("params", new JSONObject(params));
        main_param.put("jsonrpc", "2.0");*/

        JSONObject params = new JSONObject();
        JSONObject main = new JSONObject();
        try {
            params.put("set_id", selected_set_id);
            params.put("delay_time", time_interval);

            JSONArray jsonArray = new JSONArray();
            for (DeviceDetailsList list : selectedDevicesArr) {
                JSONObject object = new JSONObject();
                object.put("device_id", list.getDevice_id());
                jsonArray.put(object);
            }
            params.put("device", jsonArray);
            main.put("jsonrpc", "2.0");
            main.put("params", params);
        } catch (JSONException e) {
            e.printStackTrace();
        }


        Log.d("hashmap", main.toString());

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, main, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                MethodClass.hideProgressDialog(activity);
                Log.e("resp", response.toString());
                try {

                    JSONObject jsonObject = MethodClass.get_result_from_webservice(activity, response);


                    if (jsonObject != null) {


                        SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(activity, SweetAlertDialog.SUCCESS_TYPE);
                        if (sweetAlertDialog.isShowing()) {
                            sweetAlertDialog.dismissWithAnimation();
                        }

                        sweetAlertDialog.setTitleText(jsonObject.getString("message"));
                        sweetAlertDialog.setContentText(jsonObject.getString("message"));
                        sweetAlertDialog.setConfirmText("Ok");
                        sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();
                                finish();
                            }
                        });
                        sweetAlertDialog.setCancelable(false);
                        sweetAlertDialog.setCanceledOnTouchOutside(false);
                        sweetAlertDialog.show();


                    }

                } catch (JSONException e) {
                    MethodClass.error_alert(activity);
                    e.printStackTrace();
                    Log.e("login_parce", e.toString());
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                MethodClass.hideProgressDialog(activity);
                if (error.toString().contains("ConnectException")) {
                    MethodClass.network_error_alert(activity);
                } else {
                    MethodClass.error_alert(activity);
                }
            }
        }) {
            // Passing some request headers
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", "Bearer " + String.valueOf(SharedPrefManager.getInstance(activity).getUserToken(activity, KEY_TOKEN)));

                Log.e("getHeaders: ", headers.toString());

                return headers;
            }
        };

        VolleySingleton.getInstance(activity).addToRequestQueue(jsonObjectRequest);

    }

    public void editDevice() {

        if (selected_set_id.isEmpty()) {
            Toast.makeText(activity, "Please select set", Toast.LENGTH_SHORT).show();
            return;
        }

        if (time_interval.isEmpty() || time_interval.equalsIgnoreCase("0")) {
            Toast.makeText(activity, "Please select interval", Toast.LENGTH_SHORT).show();
            return;
        }

        if (!MethodClass.isNetworkConnected(activity)) {
            Snackbar snackbar = Snackbar.make(activity.findViewById(android.R.id.content), activity.getResources().getString(R.string.please_check_your_internet_connection), Snackbar.LENGTH_SHORT);
            snackbar.show();
            return;
        }

        MethodClass.showProgressDialog(activity);

        String server_url = Constants.BASE_URL + "edit-assign-device";

        String utoken = String.valueOf(SharedPrefManager.getInstance(AssignDevicesActivity.this).getUserToken(AssignDevicesActivity.this, KEY_TOKEN));

        HashMap<String, String> params = new HashMap<String, String>();
        params.put("assign_id", assign_id);
        params.put("set_id", selected_set_id);
        params.put("delay_time", time_interval);

        JSONObject jsonObject = MethodClass.Json_rpc_format(params);


        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                MethodClass.hideProgressDialog(AssignDevicesActivity.this);
                Log.e("resp", response.toString());
                try {

                    JSONObject jsonObject = MethodClass.get_result_from_webservice(AssignDevicesActivity.this, response);

                    //if no error in response
                    if (jsonObject != null) {

                        MethodClass.hideProgressDialog(AssignDevicesActivity.this);
                        String msg = jsonObject.getString("message");

                        Toast.makeText(AssignDevicesActivity.this, msg, Toast.LENGTH_SHORT).show();

                    }

                } catch (JSONException e) {
                    MethodClass.error_alert(AssignDevicesActivity.this);
                    e.printStackTrace();
                    Log.e("edt_dev_parce", e.toString());
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                MethodClass.hideProgressDialog(AssignDevicesActivity.this);
                if (error.toString().contains("ConnectException")) {
                    MethodClass.network_error_alert(AssignDevicesActivity.this);
                } else {
                    MethodClass.error_alert(AssignDevicesActivity.this);
                }
            }

        }) {
            //* Passing some request headers*
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", "Bearer " + utoken);

                Log.e("getHeaders: ", headers.toString());

                return headers;
            }
        };

        VolleySingleton.getInstance(AssignDevicesActivity.this).addToRequestQueue(jsonObjectRequest);

    }


    @Override
    protected void onResume() {
        super.onResume();
        MethodClass.setUserMenu(this);

    }

    @SuppressLint("WrongConstant")
    public void user_home_menu(View view) {
        DrawerLayout drawer_layout = findViewById(R.id.user_drawer_layout);
        drawer_layout.openDrawer(Gravity.START);
    }
}