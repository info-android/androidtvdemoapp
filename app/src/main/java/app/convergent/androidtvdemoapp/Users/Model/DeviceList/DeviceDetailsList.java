package app.convergent.androidtvdemoapp.Users.Model.DeviceList;

import java.util.ArrayList;

import app.convergent.androidtvdemoapp.Model_Class.UserList.UserDetailsList;

public class DeviceDetailsList {

    private int id, user_id;
    private String  device_id,device_name, device_status;
    private boolean isChecked=false;
    ArrayList<DeviceDetailsList> deviceDetailsList = new ArrayList<>();

    public DeviceDetailsList() {

    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    public int getId() {
        return id;
    }

    public String getDevice_id() {
        return device_id;
    }

    public int getUser_id() {
        return user_id;
    }

    public String getDevice_name() {
        return device_name;
    }

    public String getDevice_status() {
        return device_status;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setDevice_id(String device_id) {
        this.device_id = device_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public void setDevice_name(String device_name) {
        this.device_name = device_name;
    }

    public void setDevice_status(String device_status) {
        this.device_status = device_status;
    }

}
