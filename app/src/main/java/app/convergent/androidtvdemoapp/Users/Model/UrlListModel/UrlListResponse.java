package app.convergent.androidtvdemoapp.Users.Model.UrlListModel;

import com.google.gson.annotations.SerializedName;

import app.convergent.androidtvdemoapp.Helper.MethodClass;

public class UrlListResponse {

	@SerializedName("result")
	private Result result;

	@SerializedName("jsonrpc")
	private String jsonrpc;

	public void setResult(Result result){
		this.result = result;
	}

	public Result getResult(){
		return MethodClass.defaultWhenNull(result,new Result());
	}

	public void setJsonrpc(String jsonrpc){
		this.jsonrpc = jsonrpc;
	}

	public String getJsonrpc(){
		return jsonrpc;
	}

	@Override
 	public String toString(){
		return 
			"UserListResponse{" + 
			"result = '" + result + '\'' + 
			",jsonrpc = '" + jsonrpc + '\'' + 
			"}";
		}
}