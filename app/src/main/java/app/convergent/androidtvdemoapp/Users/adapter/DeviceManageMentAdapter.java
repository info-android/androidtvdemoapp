package app.convergent.androidtvdemoapp.Users.adapter;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.material.snackbar.Snackbar;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/*import app.convergent.androidtvapp.R;
import app.convergent.androidtvapp.Users.EditDeviceActivity;*/
import app.convergent.androidtvdemoapp.Helper.Constants;
import app.convergent.androidtvdemoapp.Helper.MethodClass;
import app.convergent.androidtvdemoapp.Helper.SharedPrefManager;
import app.convergent.androidtvdemoapp.Helper.VolleySingleton;
import app.convergent.androidtvdemoapp.R;
import app.convergent.androidtvdemoapp.Users.Model.DeviceList.DeviceDetailsList;
import app.convergent.androidtvdemoapp.Users.activity.DeviceManagement.DeviceMgmtActivity;
import app.convergent.androidtvdemoapp.Users.activity.EditDeviceActivity;
import de.hdodenhof.circleimageview.CircleImageView;

import static app.convergent.androidtvdemoapp.Helper.SharedPrefManager.KEY_TOKEN;

public class DeviceManageMentAdapter extends RecyclerView.Adapter<DeviceManageMentAdapter.ViewHolder> {
    Activity activity;
    public ArrayList<HashMap<String, String>> map_list;
    public ArrayList<DeviceDetailsList> deviceDetailsLists;
    public String utoken, device_status = "", server_blck_dev_url, server_actv_dev_url;

    public DeviceManageMentAdapter(Activity activity, ArrayList<DeviceDetailsList> deviceDetailsLists) {
        this.activity = activity;
        this.deviceDetailsLists = deviceDetailsLists;
    }

    public void update(ArrayList<DeviceDetailsList> deviceDetailsLists) {
        this.deviceDetailsLists = deviceDetailsLists;
        notifyDataSetChanged();
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.home_list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {

        final DeviceDetailsList device_list_items = deviceDetailsLists.get(position);

        holder.dev_id_tv.setText(device_list_items.getDevice_id());
        holder.dev_name_tv.setText(device_list_items.getDevice_name());

        int id = device_list_items.getId();
        String device_id = device_list_items.getDevice_id();
        String device_name = device_list_items.getDevice_name();
        device_status = device_list_items.getDevice_status();

        utoken = String.valueOf(SharedPrefManager.getInstance(activity).getUserToken(activity, KEY_TOKEN));

        server_blck_dev_url = Constants.BASE_URL + "device-deactive";
        server_actv_dev_url = Constants.BASE_URL + "device-active";

        holder.edtDev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(activity, EditDeviceActivity.class);
                intent.putExtra("from", "dev_mngnt");
                intent.putExtra("device_primary_id", id);
                intent.putExtra("dev_id", device_id);
                intent.putExtra("dev_name", device_name);
                activity.startActivity(intent);

            }
        });

        if (device_status.equals("I")) {
            holder.blckDev.setColorFilter(ContextCompat.getColor(activity, R.color.red_clr));

        }

        holder.blckDev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String status_device = device_list_items.getDevice_status();

                SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(activity, SweetAlertDialog.WARNING_TYPE);
                if (sweetAlertDialog.isShowing()) {
                    sweetAlertDialog.dismissWithAnimation();
                }

                sweetAlertDialog.setTitleText(status_device.equals("A") ? "Block Device" : "Unblock Device");
                sweetAlertDialog.setContentText("Are you sure you want to " + (status_device.equals("A") ? "Block" : "Unblock") + " this device?");
                sweetAlertDialog.setConfirmText("Ok");
                sweetAlertDialog.setCancelText("No");
                sweetAlertDialog.setCancelClickListener(null);

                sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();
                        if (MethodClass.isNetworkConnected(activity)) {
                            MethodClass.showProgressDialog(activity);


                            if (status_device.equals("A")) {

                                HashMap<String, String> params = new HashMap<String, String>();
                                params.put("device_primary_id", String.valueOf(id));

                                JSONObject jsonObject = MethodClass.Json_rpc_format(params);

                                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_blck_dev_url, jsonObject, new Response.Listener<JSONObject>() {
                                    @Override
                                    public void onResponse(JSONObject response) {
                                        MethodClass.hideProgressDialog(activity);
                                        Log.e("resp", response.toString());
                                        try {

                                            JSONObject jsonObject = MethodClass.get_result_from_webservice(activity, response);

                                            //if no error in response
                                            if (jsonObject != null) {

                                                MethodClass.hideProgressDialog(activity);
                                                String msg = jsonObject.getString("message");
                                                ((DeviceMgmtActivity) activity).device_list();

                                                Toast.makeText(activity, msg, Toast.LENGTH_SHORT).show();

                                            }

                                        } catch (JSONException e) {
                                            MethodClass.error_alert(activity);
                                            e.printStackTrace();
                                            Log.e("blck_dev_parce", e.toString());
                                        }


                                    }
                                }, new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {

                                        MethodClass.hideProgressDialog(activity);
                                        if (error.toString().contains("ConnectException")) {
                                            MethodClass.network_error_alert(activity);
                                        } else {
                                            MethodClass.error_alert(activity);
                                        }
                                    }

                                }) {
                                    //* Passing some request headers*
                                    @Override
                                    public Map getHeaders() throws AuthFailureError {
                                        HashMap headers = new HashMap();
                                        headers.put("Content-Type", "application/json");
                                        headers.put("Authorization", "Bearer " + utoken);

                                        Log.e("getHeaders: ", headers.toString());

                                        return headers;
                                    }
                                };

                                VolleySingleton.getInstance(activity).addToRequestQueue(jsonObjectRequest);

                            } else if (status_device.equals("I")) {

                                HashMap<String, String> params = new HashMap<String, String>();
                                params.put("device_primary_id", String.valueOf(id));

                                JSONObject jsonObject = MethodClass.Json_rpc_format(params);

                                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_actv_dev_url, jsonObject, new Response.Listener<JSONObject>() {
                                    @Override
                                    public void onResponse(JSONObject response) {
                                        MethodClass.hideProgressDialog(activity);
                                        Log.e("resp", response.toString());
                                        try {

                                            JSONObject jsonObject = MethodClass.get_result_from_webservice(activity, response);

                                            //if no error in response
                                            if (jsonObject != null) {

                                                MethodClass.hideProgressDialog(activity);
                                                String msg = jsonObject.getString("message");
                                                ((DeviceMgmtActivity) activity).device_list();

                                                Toast.makeText(activity, msg, Toast.LENGTH_SHORT).show();

                                            }

                                        } catch (JSONException e) {
                                            MethodClass.error_alert(activity);
                                            e.printStackTrace();
                                            Log.e("actv_dev_parce", e.toString());
                                        }


                                    }
                                }, new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {

                                        MethodClass.hideProgressDialog(activity);
                                        if (error.toString().contains("ConnectException")) {
                                            MethodClass.network_error_alert(activity);
                                        } else {
                                            MethodClass.error_alert(activity);
                                        }
                                    }

                                }) {
                                    //* Passing some request headers*
                                    @Override
                                    public Map getHeaders() throws AuthFailureError {
                                        HashMap headers = new HashMap();
                                        headers.put("Content-Type", "application/json");
                                        headers.put("Authorization", "Bearer " + utoken);

                                        Log.e("getHeaders: ", headers.toString());

                                        return headers;
                                    }
                                };

                                VolleySingleton.getInstance(activity).addToRequestQueue(jsonObjectRequest);

                            }

                        } else {
                            Snackbar snackbar = Snackbar.make(v.findViewById(android.R.id.content), v.getResources().getString(R.string.no_internet_connection), Snackbar.LENGTH_SHORT);
                            snackbar.show();
                            return;

                        }
                    }
                });
                sweetAlertDialog.setCancelable(true);
                sweetAlertDialog.setCanceledOnTouchOutside(true);
                sweetAlertDialog.show();


            }
        });

        holder.delDev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(activity, SweetAlertDialog.WARNING_TYPE);
                if (sweetAlertDialog.isShowing()) {
                    sweetAlertDialog.dismissWithAnimation();
                }

                sweetAlertDialog.setTitleText("Delete Device");
                sweetAlertDialog.setContentText("Are you sure you want to delete this device?");
                sweetAlertDialog.setConfirmText("Ok");
                sweetAlertDialog.setCancelText("No");
                sweetAlertDialog.setCancelClickListener(null);

                sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();
                        deleteDevice(String.valueOf(id));
                    }
                });
                sweetAlertDialog.setCancelable(true);
                sweetAlertDialog.setCanceledOnTouchOutside(true);
                sweetAlertDialog.show();


            }
        });


    }

    @Override
    public int getItemCount() {

        return deviceDetailsLists == null ? 0 : deviceDetailsLists.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private CircleImageView edtDev, delDev, blckDev;
        private TextView dev_id_tv, dev_name_tv;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            edtDev = itemView.findViewById(R.id.edtDev);
            delDev = itemView.findViewById(R.id.delDev);
            blckDev = itemView.findViewById(R.id.blckDev);
            dev_id_tv = itemView.findViewById(R.id.deviceName_tv);
            dev_name_tv = itemView.findViewById(R.id.device_id_tv);
        }
    }

    public void deleteDevice(String dev_id) {

        if (!MethodClass.isNetworkConnected(activity)) {
            Snackbar snackbar = Snackbar.make(activity.findViewById(android.R.id.content), activity.getResources().getString(R.string.please_check_your_internet_connection), Snackbar.LENGTH_SHORT);
            snackbar.show();
            return;
        }
        MethodClass.showProgressDialog(activity);
        String server_del_url = Constants.BASE_URL + "device-delete";
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("device_primary_id", String.valueOf(dev_id));

        JSONObject jsonObject = MethodClass.Json_rpc_format(params);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_del_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                MethodClass.hideProgressDialog(activity);
                Log.e("resp", response.toString());
                try {

                    JSONObject jsonObject = MethodClass.get_result_from_webservice(activity, response);

                    //if no error in response
                    if (jsonObject != null) {

                        MethodClass.hideProgressDialog(activity);
                        String msg = jsonObject.getString("message");
                        ((DeviceMgmtActivity) activity).device_list();


                        Toast.makeText(activity, msg, Toast.LENGTH_SHORT).show();

                    }

                } catch (JSONException e) {
                    MethodClass.hideProgressDialog(activity);
                    MethodClass.error_alert(activity);
                    e.printStackTrace();
                    Log.e("del_dev_parce", e.toString());
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                MethodClass.hideProgressDialog(activity);
                if (error.toString().contains("ConnectException")) {
                    MethodClass.network_error_alert(activity);
                } else {
                    MethodClass.error_alert(activity);
                }
            }

        }) {
            //* Passing some request headers*
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", "Bearer " + utoken);

                Log.e("getHeaders: ", headers.toString());

                return headers;
            }
        };

        VolleySingleton.getInstance(activity).addToRequestQueue(jsonObjectRequest);


    }

}

