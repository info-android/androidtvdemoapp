package app.convergent.androidtvdemoapp.Users.activity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import app.convergent.androidtvdemoapp.Admin.AdminEditProfileActivity;
import app.convergent.androidtvdemoapp.Admin.UserAddActivity;
import app.convergent.androidtvdemoapp.Helper.Constants;
import app.convergent.androidtvdemoapp.Helper.MethodClass;
import app.convergent.androidtvdemoapp.Helper.SharedPrefManager;
import app.convergent.androidtvdemoapp.Helper.VolleySingleton;
import app.convergent.androidtvdemoapp.Model_Class.AdminDashboard.MyProfile;
import app.convergent.androidtvdemoapp.R;

import static app.convergent.androidtvdemoapp.Helper.SharedPrefManager.KEY_TOKEN;

/*import app.convergent.androidtvapp.Helper.MethodClass;
import app.convergent.androidtvapp.R;*/

public class AddDeviceActivity extends AppCompatActivity {

    private EditText dev_name_et, dev_id_et;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_device);

        TextView title = findViewById(R.id.user_dash_title);
        title.setText(getString(R.string.dev_add));

        dev_id_et = findViewById(R.id.dev_id);
        dev_name_et = findViewById(R.id.dev_name);
    }

    public void addDevice(View view) {

        String get_device_name = dev_name_et.getText().toString().trim();
        String get_device_id = dev_id_et.getText().toString().trim();

        String utoken = String.valueOf(SharedPrefManager.getInstance(AddDeviceActivity.this).getUserToken(AddDeviceActivity.this,KEY_TOKEN));

        if (get_device_id.length() == 0) {
            dev_id_et.setError(getString(R.string.entr_dev_id));
            dev_id_et.requestFocus();
            return;
        }

        if (get_device_name.length() == 0) {
            dev_name_et.setError(getString(R.string.entr_dev_name));
            dev_name_et.requestFocus();
            return;
        }

        if(MethodClass.isNetworkConnected(this)) {
            MethodClass.showProgressDialog(AddDeviceActivity.this);

            String server_url = Constants.BASE_URL + "create-device";

            HashMap<String, String> params = new HashMap<String, String>();
            params.put("device_id", get_device_id);
            params.put("device_name", get_device_name);

            JSONObject jsonObject = MethodClass.Json_rpc_format(params);

            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    MethodClass.hideProgressDialog(AddDeviceActivity.this);
                    Log.e("resp", response.toString());
                    try {

                        JSONObject jsonObject = MethodClass.get_result_from_webservice(AddDeviceActivity.this, response);

                        //if no error in response
                        if (jsonObject!= null) {

                            MethodClass.hideProgressDialog(AddDeviceActivity.this);
                            String msg = jsonObject.getString("message");

                            dev_id_et.setText("");
                            dev_name_et.setText("");

                            Toast.makeText(AddDeviceActivity.this, msg, Toast.LENGTH_SHORT).show();

                        }

                    } catch (JSONException e) {
                        MethodClass.error_alert(AddDeviceActivity.this);
                        e.printStackTrace();
                        Log.e("add_dev_parce", e.toString());
                    }


                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    MethodClass.hideProgressDialog(AddDeviceActivity.this);
                    if (error.toString().contains("ConnectException")) {
                        MethodClass.network_error_alert(AddDeviceActivity.this);
                    } else {
                        MethodClass.error_alert(AddDeviceActivity.this);
                    }
                }

            }){
                //* Passing some request headers*
                @Override
                public Map getHeaders() throws AuthFailureError {
                    HashMap headers = new HashMap();
                    headers.put("Content-Type", "application/json");
                    headers.put("Authorization", "Bearer " + utoken);

                    Log.e("getHeaders: ", headers.toString());

                    return headers;
                }
            };

            VolleySingleton.getInstance(AddDeviceActivity.this).addToRequestQueue(jsonObjectRequest);

        }
        else {

            Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), getResources().getString(R.string.no_internet_connection), Snackbar.LENGTH_SHORT);
            snackbar.show();
            return;

        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        MethodClass.setUserMenu(this);
    }

    @SuppressLint("WrongConstant")
    public void user_home_menu(View view) {
        DrawerLayout drawer_layout = findViewById(R.id.user_drawer_layout);
        drawer_layout.openDrawer(Gravity.START);
    }
}

