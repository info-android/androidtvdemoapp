package app.convergent.androidtvdemoapp.Users.adapter;

import static app.convergent.androidtvdemoapp.Helper.SharedPrefManager.KEY_TOKEN;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.material.card.MaterialCardView;
import com.google.android.material.snackbar.Snackbar;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/*import app.convergent.androidtvapp.R;
import app.convergent.androidtvapp.Users.URL_Management.Add_Edit_URL;*/
import app.convergent.androidtvdemoapp.Helper.Constants;
import app.convergent.androidtvdemoapp.Helper.MethodClass;
import app.convergent.androidtvdemoapp.Helper.SharedPrefManager;
import app.convergent.androidtvdemoapp.Helper.VolleySingleton;
import app.convergent.androidtvdemoapp.R;
import app.convergent.androidtvdemoapp.Users.Model.SetListModel.SeeSetWithUrlsItem;
import app.convergent.androidtvdemoapp.Users.Model.UrlListModel.SetToUrlItem;
import app.convergent.androidtvdemoapp.Users.activity.URL_Management.Add_Edit_URL;
import app.convergent.androidtvdemoapp.Users.activity.URL_Management.URLManagement;
import app.convergent.androidtvdemoapp.Users.helper.DragItemTouchHelper;
import de.hdodenhof.circleimageview.CircleImageView;

public class URLAdapter extends RecyclerView.Adapter<URLAdapter.ViewHolder> implements DragItemTouchHelper.MoveHelperAdapter {
    Activity activity;
    List<SetToUrlItem> map_list;

    private OnItemClickListener mOnItemClickListener;
    private OnStartDragListener mDragStartListener = null;
    OnDragComplete onDragComplete;


    int to_pos_order = -1;
    int from_pos_order = -1;


    public interface OnItemClickListener {
        void onItemClick(View view, SeeSetWithUrlsItem obj, int position);
    }

    public interface OnStartDragListener {
        void onStartDrag(RecyclerView.ViewHolder viewHolder);
    }

    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mOnItemClickListener = mItemClickListener;
    }


    public void setDragListener(OnStartDragListener dragStartListener) {
        this.mDragStartListener = dragStartListener;
    }

    public URLAdapter(Activity activity, List<SetToUrlItem> map_list) {
        this.activity = activity;
        this.map_list = map_list;
    }

    public void add_item(List<SetToUrlItem> map_list) {
        this.map_list = map_list;
        to_pos_order = -1;
        from_pos_order = -1;
        notifyDataSetChanged();
    }


    public interface OnDragComplete {
        void onCompleteDrag(String id, String to_pos);


    }


    public void dragComplete(OnDragComplete onDragComplete) {
        this.onDragComplete = onDragComplete;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.url_list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {

        SetToUrlItem model = map_list.get(position);


        // holder.setOrder_tv.setText(model.getDisplayOrder());
        holder.tv_url_name.setText(model.getUrlName());
        holder.tv_url.setText(model.getUrl());


        holder.img_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(activity, Add_Edit_URL.class);
                intent.putExtra("url_header", "EDIT URL");
                intent.putExtra("url_save", "SAVE URL");
                intent.putExtra("type", "edit");
                intent.putExtra("set_id", model.getParentSetId());
                intent.putExtra("url_id", "" + model.getId());

                intent.putExtra("url", model.getUrl());
                intent.putExtra("url_name", model.getUrlName());
                intent.putExtra("url_order", model.getDisplayOrder());


                activity.startActivity(intent);

            }
        });

        holder.img_delete.setOnClickListener(v -> {

            SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(activity, SweetAlertDialog.WARNING_TYPE);
            if (sweetAlertDialog.isShowing()) {
                sweetAlertDialog.dismissWithAnimation();
            }

            sweetAlertDialog.setTitleText("Delete Url");
            sweetAlertDialog.setContentText("Are you sure you want to delete this url?");
            sweetAlertDialog.setConfirmText("Ok");
            sweetAlertDialog.setCancelText("No");
            sweetAlertDialog.setCancelClickListener(null);

            sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sDialog) {
                    sDialog.dismissWithAnimation();
                    deleteUrl("" + model.getId());
                }
            });
            sweetAlertDialog.setCancelable(true);
            sweetAlertDialog.setCanceledOnTouchOutside(true);
            sweetAlertDialog.show();

        });
    }

    @Override
    public int getItemCount() {
        return map_list == null ? 0 : map_list.size();  /*map_list.size()*/
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements DragItemTouchHelper.TouchViewHolder {

        private CircleImageView img_edit, img_delete;
        TextView tv_url_name, tv_url;
        MaterialCardView cardView;


        public ViewHolder(@NonNull View itemView) {

            super(itemView);

            cardView = itemView.findViewById(R.id.cardView);
            tv_url = itemView.findViewById(R.id.tv_url);
            tv_url_name = itemView.findViewById(R.id.tv_url_name);
            img_edit = itemView.findViewById(R.id.img_edit);
            img_delete = itemView.findViewById(R.id.img_delete);

        }


        @Override
        public void onItemSelected() {


            from_pos_order = -1;
            from_pos_order = Integer.parseInt(map_list.get(getAdapterPosition()).getDisplayOrder());
            cardView.setCardBackgroundColor(activity.getResources().getColor(R.color.gray_btn_bg_color));
        }

        @Override
        public void onItemClear() {
            // Log.d("onItemMove", "from " + fromPosition1 + " to " + toPosition1);
            cardView.setCardBackgroundColor(activity.getResources().getColor(R.color.white));

            if (from_pos_order != -1 && to_pos_order != -1) {
                if (from_pos_order != to_pos_order) {

                    onDragComplete.onCompleteDrag("" + from_pos_order, "" + to_pos_order);


                }
            }
        }
    }


    @Override
    public boolean onItemMove(int fromPosition, int toPosition) {

        to_pos_order = -1;
        to_pos_order = Integer.parseInt(map_list.get(toPosition).getDisplayOrder());
        Collections.swap(map_list, fromPosition, toPosition);
        notifyItemMoved(fromPosition, toPosition);
        return true;

    }


    public void deleteUrl(String urlId) {


        if (!MethodClass.isNetworkConnected(activity)) {
            Snackbar snackbar = Snackbar.make(activity.findViewById(android.R.id.content), activity.getResources().getString(R.string.please_check_your_internet_connection), Snackbar.LENGTH_SHORT);
            snackbar.show();
            return;
        }

        MethodClass.showProgressDialog(activity);

        String server_url = Constants.BASE_URL + "url-delete";

        HashMap<String, String> params = new HashMap<String, String>();
        params.put("url_id", urlId);
        JSONObject jsonObject = MethodClass.Json_rpc_format(params);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                MethodClass.hideProgressDialog(activity);
                Log.e("resp", response.toString());
                try {

                    JSONObject jsonObject = MethodClass.get_result_from_webservice(activity, response);


                    if (jsonObject != null) {

                        ((URLManagement) activity).getUrlList();

                        SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(activity, SweetAlertDialog.SUCCESS_TYPE);
                        if (sweetAlertDialog.isShowing()) {
                            sweetAlertDialog.dismissWithAnimation();
                        }

                        sweetAlertDialog.setTitleText(jsonObject.getString("message"));
                        sweetAlertDialog.setContentText(jsonObject.getString("message"));
                        sweetAlertDialog.setConfirmText("Ok");
                        sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();

                            }
                        });
                        sweetAlertDialog.setCancelable(false);
                        sweetAlertDialog.setCanceledOnTouchOutside(false);
                        sweetAlertDialog.show();


                    }

                } catch (JSONException e) {
                    MethodClass.error_alert(activity);
                    e.printStackTrace();
                    Log.e("login_parce", e.toString());
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                MethodClass.hideProgressDialog(activity);
                if (error.toString().contains("ConnectException")) {
                    MethodClass.network_error_alert(activity);
                } else {
                    MethodClass.error_alert(activity);
                }
            }
        }) {
            // Passing some request headers
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", "Bearer " + String.valueOf(SharedPrefManager.getInstance(activity).getUserToken(activity, KEY_TOKEN)));

                Log.e("getHeaders: ", headers.toString());

                return headers;
            }
        };

        VolleySingleton.getInstance(activity).addToRequestQueue(jsonObjectRequest);

    }


}
