package app.convergent.androidtvdemoapp.Users.Model.SetListModel;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class SeeSetWithUrlsItem{

	@SerializedName("set_name")
	private String setName;

	@SerializedName("updated_at")
	private String updatedAt;

	@SerializedName("user_id")
	private String userId;

	@SerializedName("created_at")
	private String createdAt;

	@SerializedName("set_to_url")
	private List<Object> setToUrl;

	@SerializedName("set_id")
	private String setId;

	@SerializedName("id")
	private int id;

	@SerializedName("status")
	private String status;

	public void setSetName(String setName){
		this.setName = setName;
	}

	public String getSetName(){
		return setName;
	}

	public void setUpdatedAt(String updatedAt){
		this.updatedAt = updatedAt;
	}

	public String getUpdatedAt(){
		return updatedAt;
	}

	public void setUserId(String userId){
		this.userId = userId;
	}

	public String getUserId(){
		return userId;
	}

	public void setCreatedAt(String createdAt){
		this.createdAt = createdAt;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public void setSetToUrl(List<Object> setToUrl){
		this.setToUrl = setToUrl;
	}

	public List<Object> getSetToUrl(){
		return setToUrl;
	}

	public void setSetId(String setId){
		this.setId = setId;
	}

	public String getSetId(){
		return setId;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"SeeSetWithUrlsItem{" + 
			"set_name = '" + setName + '\'' + 
			",updated_at = '" + updatedAt + '\'' + 
			",user_id = '" + userId + '\'' + 
			",created_at = '" + createdAt + '\'' + 
			",set_to_url = '" + setToUrl + '\'' + 
			",set_id = '" + setId + '\'' + 
			",id = '" + id + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}