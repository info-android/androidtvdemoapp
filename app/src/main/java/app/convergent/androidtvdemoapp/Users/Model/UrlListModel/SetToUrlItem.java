package app.convergent.androidtvdemoapp.Users.Model.UrlListModel;

import com.google.gson.annotations.SerializedName;

public class SetToUrlItem{

	@SerializedName("url_name")
	private String urlName;

	@SerializedName("updated_at")
	private String updatedAt;

	@SerializedName("user_id")
	private String userId;

	@SerializedName("display_order")
	private String displayOrder;

	@SerializedName("created_at")
	private String createdAt;

	@SerializedName("id")
	private int id;

	@SerializedName("parent_set_id")
	private String parentSetId;

	@SerializedName("url")
	private String url;

	public void setUrlName(String urlName){
		this.urlName = urlName;
	}

	public String getUrlName(){
		return urlName;
	}

	public void setUpdatedAt(String updatedAt){
		this.updatedAt = updatedAt;
	}

	public String getUpdatedAt(){
		return updatedAt;
	}

	public void setUserId(String userId){
		this.userId = userId;
	}

	public String getUserId(){
		return userId;
	}

	public void setDisplayOrder(String displayOrder){
		this.displayOrder = displayOrder;
	}

	public String getDisplayOrder(){
		return displayOrder;
	}

	public void setCreatedAt(String createdAt){
		this.createdAt = createdAt;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setParentSetId(String parentSetId){
		this.parentSetId = parentSetId;
	}

	public String getParentSetId(){
		return parentSetId;
	}

	public void setUrl(String url){
		this.url = url;
	}

	public String getUrl(){
		return url;
	}

	@Override
 	public String toString(){
		return 
			"SetToUrlItem{" + 
			"url_name = '" + urlName + '\'' + 
			",updated_at = '" + updatedAt + '\'' + 
			",user_id = '" + userId + '\'' + 
			",display_order = '" + displayOrder + '\'' + 
			",created_at = '" + createdAt + '\'' + 
			",id = '" + id + '\'' + 
			",parent_set_id = '" + parentSetId + '\'' + 
			",url = '" + url + '\'' + 
			"}";
		}
}