package app.convergent.androidtvdemoapp.Users.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import app.convergent.androidtvdemoapp.Admin.AdminDashboardActivity;
import app.convergent.androidtvdemoapp.Admin.ChangePasswordActivity;
import app.convergent.androidtvdemoapp.Helper.Constants;
import app.convergent.androidtvdemoapp.Helper.MethodClass;
import app.convergent.androidtvdemoapp.Helper.SharedPrefManager;
import app.convergent.androidtvdemoapp.Helper.VolleySingleton;
import app.convergent.androidtvdemoapp.R;

import static app.convergent.androidtvdemoapp.Helper.SharedPrefManager.KEY_TOKEN;

public class UserChangePasswordActivity extends AppCompatActivity {

    private Button user_button1, user_button2, user_button3;
    private EditText oldPass, confm_pass, pass;
    public String get_user_confm_pswrd = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_change_password);

        TextView title = findViewById(R.id.user_dash_title);
        title.setText(getString(R.string.change_pwd));

        oldPass = findViewById(R.id.user_opass1);
        confm_pass = findViewById(R.id.uconfpass);
        pass = findViewById(R.id.upass);
        user_button1 = findViewById(R.id.user_button1);
        user_button2 = findViewById(R.id.user_button2);
        user_button3 = findViewById(R.id.user_old_button1);

        user_button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (oldPass.getInputType() == InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD) {
                    oldPass.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    oldPass.setTransformationMethod(new PasswordTransformationMethod());
                } else {
                    oldPass.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                    oldPass.setTransformationMethod(new HideReturnsTransformationMethod());
                }
            }
        });

        user_button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pass.getInputType() == InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD) {
                    pass.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    pass.setTransformationMethod(new PasswordTransformationMethod());
                } else {
                    pass.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                    pass.setTransformationMethod(new HideReturnsTransformationMethod());
                }
            }
        });

        user_button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (confm_pass.getInputType() == InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD) {
                    confm_pass.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    confm_pass.setTransformationMethod(new PasswordTransformationMethod());
                } else {
                    confm_pass.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                    confm_pass.setTransformationMethod(new HideReturnsTransformationMethod());
                }
            }
        });

    }

    public void uChangePwd(View view) {

        String get_old_pswrd = "", get_new_pswrd = "";
        get_old_pswrd = oldPass.getText().toString().trim();
        get_new_pswrd = pass.getText().toString().trim();
        get_user_confm_pswrd = confm_pass.getText().toString().trim();

        if (get_old_pswrd.length() == 0) {
            oldPass.setError(getString(R.string.enterpas));
            oldPass.requestFocus();
            return;
        }

        if (get_new_pswrd.length() == 0) {
            pass.setError(getString(R.string.enterpas));
            pass.requestFocus();
            return;
        }

        if (get_user_confm_pswrd.length() == 0) {
            confm_pass.setError(getString(R.string.enterpas));
            confm_pass.requestFocus();
            return;
        }

        if(!get_new_pswrd.equals(get_user_confm_pswrd)) {
            Toast.makeText(UserChangePasswordActivity.this,"Password doesn't match !",Toast.LENGTH_LONG).show();
            return;
        }

        if (!MethodClass.isNetworkConnected(this)) {
            Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), getResources().getString(R.string.please_check_your_internet_connection), Snackbar.LENGTH_SHORT);
            snackbar.show();
            return;
        }

        String utoken = String.valueOf(SharedPrefManager.getInstance(UserChangePasswordActivity.this).getUserToken(UserChangePasswordActivity.this,KEY_TOKEN));
        MethodClass.showProgressDialog(UserChangePasswordActivity.this);

        String server_url = Constants.BASE_URL + "change-password";

        HashMap<String, String> params = new HashMap<String, String>();
        params.put("old_password", get_old_pswrd);
        params.put("new_password", get_new_pswrd);
        params.put("password_confirmation", get_user_confm_pswrd);

        JSONObject jsonObject = MethodClass.Json_rpc_format(params);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                MethodClass.hideProgressDialog(UserChangePasswordActivity.this);
                Log.e("resp", response.toString());
                try {

                    JSONObject jsonObject = MethodClass.get_result_from_webservice(UserChangePasswordActivity.this, response);

                    //if no error in response
                    if (jsonObject!= null) {
                        //Toast.makeText(getApplicationContext(), jsonObject.getString("message"), Toast.LENGTH_SHORT).show();

                        /*GsonBuilder gsonBuilder = new GsonBuilder();
                        Gson gson = gsonBuilder.create();
                        LoginMain loginMain = gson.fromJson(response.toString(), LoginMain.class);*/


                        String message = jsonObject.getString("message");

                        //storing the user password
                        //SharedPrefManager.getInstance(getApplicationContext()).userPassword(get_user_confm_pswrd);

                        Toast.makeText(UserChangePasswordActivity.this,message,Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(UserChangePasswordActivity.this, UserDashBoardActivity.class);
                        startActivity(intent);

                    }

                } catch (JSONException e) {
                    MethodClass.error_alert(UserChangePasswordActivity.this);
                    e.printStackTrace();
                    Log.e("user_change_parce", e.toString());
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                MethodClass.hideProgressDialog(UserChangePasswordActivity.this);
                if (error.toString().contains("ConnectException")) {
                    MethodClass.network_error_alert(UserChangePasswordActivity.this);
                } else {
                    MethodClass.error_alert(UserChangePasswordActivity.this);
                }
            }

        }){
            //* Passing some request headers*
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", "Bearer " + utoken);

                Log.e("getHeaders: ", headers.toString());

                return headers;
            }
        };

        VolleySingleton.getInstance(UserChangePasswordActivity.this).addToRequestQueue(jsonObjectRequest);

    }
    @Override
    protected void onResume() {
        super.onResume();
        MethodClass.setUserMenu(this);
    }

    @SuppressLint("WrongConstant")
    public void user_home_menu(View view) {
        DrawerLayout drawer_layout = findViewById(R.id.user_drawer_layout);
        drawer_layout.openDrawer(Gravity.START);
    }

}
