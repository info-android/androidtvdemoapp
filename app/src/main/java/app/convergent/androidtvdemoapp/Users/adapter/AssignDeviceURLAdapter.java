package app.convergent.androidtvdemoapp.Users.adapter;

import static app.convergent.androidtvdemoapp.Helper.SharedPrefManager.KEY_TOKEN;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.material.card.MaterialCardView;
import com.google.android.material.snackbar.Snackbar;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import app.convergent.androidtvdemoapp.Helper.Constants;
import app.convergent.androidtvdemoapp.Helper.MethodClass;
import app.convergent.androidtvdemoapp.Helper.SharedPrefManager;
import app.convergent.androidtvdemoapp.Helper.VolleySingleton;
import app.convergent.androidtvdemoapp.R;
import app.convergent.androidtvdemoapp.Users.Model.SetListModel.SeeSetWithUrlsItem;
import app.convergent.androidtvdemoapp.Users.Model.UrlListModel.SetToUrlItem;
import app.convergent.androidtvdemoapp.Users.activity.URL_Management.Add_Edit_URL;
import app.convergent.androidtvdemoapp.Users.activity.URL_Management.URLManagement;
import app.convergent.androidtvdemoapp.Users.helper.DragItemTouchHelper;
import de.hdodenhof.circleimageview.CircleImageView;

public class AssignDeviceURLAdapter extends RecyclerView.Adapter<AssignDeviceURLAdapter.ViewHolder> {
    Activity activity;
    List<SetToUrlItem> map_list;


    public AssignDeviceURLAdapter(Activity activity, List<SetToUrlItem> map_list) {
        this.activity = activity;
        this.map_list = map_list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.assign_url_list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {

        SetToUrlItem model = map_list.get(position);


        // holder.setOrder_tv.setText(model.getDisplayOrder());
        holder.tv_url_name.setText(model.getUrlName());
        holder.tv_url.setText(model.getUrl());



    }

    @Override
    public int getItemCount() {
        return map_list == null ? 0 : map_list.size();  /*map_list.size()*/
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        private CircleImageView img_edit, img_delete;
        TextView tv_url_name, tv_url;
        MaterialCardView cardView;


        public ViewHolder(@NonNull View itemView) {

            super(itemView);

            cardView = itemView.findViewById(R.id.cardView);
            tv_url = itemView.findViewById(R.id.tv_url);
            tv_url_name = itemView.findViewById(R.id.tv_url_name);
            img_edit = itemView.findViewById(R.id.img_edit);
            img_delete = itemView.findViewById(R.id.img_delete);

        }

    }

    public void deleteUrl(String urlId) {


        if (!MethodClass.isNetworkConnected(activity)) {
            Snackbar snackbar = Snackbar.make(activity.findViewById(android.R.id.content), activity.getResources().getString(R.string.please_check_your_internet_connection), Snackbar.LENGTH_SHORT);
            snackbar.show();
            return;
        }

        MethodClass.showProgressDialog(activity);

        String server_url = Constants.BASE_URL + "url-delete";

        HashMap<String, String> params = new HashMap<String, String>();
        params.put("url_id", urlId);
        JSONObject jsonObject = MethodClass.Json_rpc_format(params);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                MethodClass.hideProgressDialog(activity);
                Log.e("resp", response.toString());
                try {

                    JSONObject jsonObject = MethodClass.get_result_from_webservice(activity, response);


                    if (jsonObject != null) {

                        ((URLManagement) activity).getUrlList();

                        SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(activity, SweetAlertDialog.SUCCESS_TYPE);
                        if (sweetAlertDialog.isShowing()) {
                            sweetAlertDialog.dismissWithAnimation();
                        }

                        sweetAlertDialog.setTitleText(jsonObject.getString("message"));
                        sweetAlertDialog.setContentText(jsonObject.getString("message"));
                        sweetAlertDialog.setConfirmText("Ok");
                        sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();

                            }
                        });
                        sweetAlertDialog.setCancelable(false);
                        sweetAlertDialog.setCanceledOnTouchOutside(false);
                        sweetAlertDialog.show();


                    }

                } catch (JSONException e) {
                    MethodClass.error_alert(activity);
                    e.printStackTrace();
                    Log.e("login_parce", e.toString());
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                MethodClass.hideProgressDialog(activity);
                if (error.toString().contains("ConnectException")) {
                    MethodClass.network_error_alert(activity);
                } else {
                    MethodClass.error_alert(activity);
                }
            }
        }) {
            // Passing some request headers
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", "Bearer " + String.valueOf(SharedPrefManager.getInstance(activity).getUserToken(activity, KEY_TOKEN)));

                Log.e("getHeaders: ", headers.toString());

                return headers;
            }
        };

        VolleySingleton.getInstance(activity).addToRequestQueue(jsonObjectRequest);

    }

}
