package app.convergent.androidtvdemoapp.Users.Model.AssignDeviceList;

import java.util.ArrayList;

import app.convergent.androidtvdemoapp.Users.Model.DeviceList.DeviceDetailsList;

public class AssignDevicesList {

    private int id;
    private String  device_id,device_name, status, set_id, set_name, interval;
    private ArrayList<AssignDevicesList> assignDevicesLists = new ArrayList<>();

    public AssignDevicesList() {

    }

    public int getId() {
        return id;
    }

    public String getDevice_id() {
        return device_id;
    }

    public String getDevice_name() {
        return device_name;
    }

    public String getStatus() {
        return status;
    }

    public String getSet_id() {
        return set_id;
    }

    public String getSet_name() {
        return set_name;
    }

    public String getInterval() {
        return interval;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setDevice_id(String device_id) {
        this.device_id = device_id;
    }

    public void setDevice_name(String device_name) {
        this.device_name = device_name;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setSet_id(String set_id) {
        this.set_id = set_id;
    }

    public void setSet_name(String set_name) {
        this.set_name = set_name;
    }

    public void setInterval(String interval) {
        this.interval = interval;
    }
}
