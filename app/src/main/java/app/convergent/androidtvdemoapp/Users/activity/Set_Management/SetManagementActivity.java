package app.convergent.androidtvdemoapp.Users.activity.Set_Management;

import static app.convergent.androidtvdemoapp.Helper.SharedPrefManager.KEY_TOKEN;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/*import app.convergent.androidtvapp.Adapter.SetAdapter;
import app.convergent.androidtvapp.Helper.MethodClass;
import app.convergent.androidtvapp.R;*/
import app.convergent.androidtvdemoapp.Users.adapter.SetAdapter;
import app.convergent.androidtvdemoapp.Helper.Constants;
import app.convergent.androidtvdemoapp.Helper.MethodClass;
import app.convergent.androidtvdemoapp.Helper.SharedPrefManager;
import app.convergent.androidtvdemoapp.Helper.VolleySingleton;
import app.convergent.androidtvdemoapp.R;
import app.convergent.androidtvdemoapp.Users.Model.SetListModel.SetListResponse;
import de.hdodenhof.circleimageview.CircleImageView;

public class SetManagementActivity extends AppCompatActivity {

    public RecyclerView user_recy_view;
    CircleImageView addSet_fav;
    Activity activity;
    CardView cardView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_management);
        activity = this;

        addSet_fav = findViewById(R.id.addSet_fav);
        addSet_fav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SetManagementActivity.this, AddSetActivity.class));
            }
        });
        user_recy_view = findViewById(R.id.user_recy_view);
        user_recy_view.setFocusable(false);
        cardView = findViewById(R.id.nodata_cardView);


    }


    public void getSetList() {


        if (!MethodClass.isNetworkConnected(this)) {
            Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content),
                    getResources().getString(R.string.please_check_your_internet_connection), Snackbar.LENGTH_SHORT);
            snackbar.show();
            return;
        }

        MethodClass.showProgressDialog(activity);

        String server_url = Constants.BASE_URL + "set-list";

        HashMap<String, String> params = new HashMap<String, String>();
        //  params.put("set_name", tv_setname.getText().toString());


        JSONObject jsonObject = MethodClass.Json_rpc_format(params);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                MethodClass.hideProgressDialog(activity);
                Log.e("resp", response.toString());
                try {

                    JSONObject jsonObject = MethodClass.get_result_from_webservice(activity, response);


                    if (jsonObject != null) {

                        cardView.setVisibility(View.GONE);
                        user_recy_view.setVisibility(View.VISIBLE);

                        SetListResponse model
                                = new Gson().fromJson(response.toString(), SetListResponse.class);


                        SetAdapter userItemAdapter = new SetAdapter(activity, model.getResult().getSeeSetWithUrls());

                        user_recy_view.setAdapter(userItemAdapter);

                        if(model.getResult().getSeeSetWithUrls().isEmpty()) {

                            user_recy_view.setVisibility(View.GONE);
                            cardView.setVisibility(View.VISIBLE);
                        }

                    }

                } catch (Exception e) {
                    MethodClass.error_alert(activity);
                    e.printStackTrace();
                    Log.e("login_parce", e.toString());
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                MethodClass.hideProgressDialog(activity);
                if (error.toString().contains("ConnectException")) {
                    MethodClass.network_error_alert(activity);
                } else {
                    MethodClass.error_alert(activity);
                }
            }
        }) {
            // Passing some request headers
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", "Bearer " + String.valueOf(SharedPrefManager.getInstance(activity).getUserToken(activity, KEY_TOKEN)));

                Log.e("getHeaders: ", headers.toString());

                return headers;
            }
        };

        VolleySingleton.getInstance(activity).addToRequestQueue(jsonObjectRequest);

    }


    @Override
    protected void onResume() {
        super.onResume();
        MethodClass.setUserMenu(this);
        getSetList();

    }

    @SuppressLint("WrongConstant")
    public void user_home_menu(View view) {
        DrawerLayout drawer_layout = findViewById(R.id.user_drawer_layout);
        drawer_layout.openDrawer(Gravity.START);
    }

}