package app.convergent.androidtvdemoapp.Users.Model.SetListModel;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.SerializedName;

import app.convergent.androidtvdemoapp.Helper.MethodClass;

public class Result {

    @SerializedName("set_list")
    private List<SeeSetWithUrlsItem> seeSetWithUrls;

    public void setSeeSetWithUrls(List<SeeSetWithUrlsItem> seeSetWithUrls) {
        this.seeSetWithUrls = seeSetWithUrls;
    }

    public List<SeeSetWithUrlsItem> getSeeSetWithUrls() {
        return MethodClass.defaultWhenNull(seeSetWithUrls, new ArrayList<>());

    }

    @Override
    public String toString() {
        return
                "Result{" +
                        "see_set_with_urls = '" + seeSetWithUrls + '\'' +
                        "}";
    }
}