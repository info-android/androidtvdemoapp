package app.convergent.androidtvdemoapp.Model_Class.UserList;

import java.util.ArrayList;

public class UserDetailsList {

    private int id;
    private String name, email, user_type, status, ph;
    private ArrayList<UserDetailsList> userDetailsList = new ArrayList<>();

    public UserDetailsList() {

    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getUser_type() {
        return user_type;
    }

    public String getStatus() {
        return status;
    }

    public String getPh() {
        return ph;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setUser_type(String user_type) {
        this.user_type = user_type;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setPh(String ph) {
        this.ph = ph;
    }
}
