package app.convergent.androidtvdemoapp.Model_Class.LoginModel;

public class Userdata {
    private int id;
    private String name, email, usertype;

    public Userdata(int id, String name, String email, String usertype) {
        this.id = id;
        this.email = email;
        this.usertype = usertype;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }


    public String getUserType() {
        return usertype;
    }

    public void setUserType(String usertype) {
        this.usertype = usertype;
    }
}
