package app.convergent.androidtvdemoapp.Model_Class.ForgotPasswordOtpEmail;

public class Result {

    private String message, email;
    int passcode;

    public Result(String message, String email, int passcode) {
        this.message = message;
        this.email = email;
        this.passcode = passcode;
    }

    public String getMessage() {
        return message;
    }

    public String getEmail() {
        return email;
    }

    public int getPasscode() {
        return passcode;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPasscode(int passcode) {
        this.passcode = passcode;
    }


}
