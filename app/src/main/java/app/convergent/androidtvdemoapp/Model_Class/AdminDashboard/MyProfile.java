package app.convergent.androidtvdemoapp.Model_Class.AdminDashboard;

public class MyProfile {

    private int id;
    private String name, email, usertype, ph, status;

    public MyProfile(int id, String name, String email, String usertype, String ph, String status) {
        this.id = id;
        this.email = email;
        this.usertype = usertype;
        this.name = name;
        this.ph = ph;
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getUsertype() {
        return usertype;
    }

    public String getPh() {
        return ph;
    }

    public String getStatus() {
        return status;
    }

    public int getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setUsertype(String usertype) {
        this.usertype = usertype;
    }

    public void setPh(String ph) {
        this.ph = ph;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setId(int id) {
        this.id = id;
    }



}
