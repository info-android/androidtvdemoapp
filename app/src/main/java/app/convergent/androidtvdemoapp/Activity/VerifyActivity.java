package app.convergent.androidtvdemoapp.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import app.convergent.androidtvdemoapp.Helper.Constants;
import app.convergent.androidtvdemoapp.Helper.MethodClass;
import app.convergent.androidtvdemoapp.Helper.VolleySingleton;
import app.convergent.androidtvdemoapp.Model_Class.ForgotPasswordOtpEmail.Result;
import app.convergent.androidtvdemoapp.R;

//import app.convergent.androidtvapp.R;

public class VerifyActivity extends AppCompatActivity {

    private Button btn_verify;
    private EditText editText1, editText2, editText3, editText4, editText5, editText6;
    public String email,fotp, ed1, ed2, ed3, ed4, ed5, ed6;
    public int otp;
    private TextView forgot_tv, resend_tv;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify);

        TextView title = findViewById(R.id.title);
        title.setText(getString(R.string.verify_user));
        //title.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);


        email = getIntent().getStringExtra("email");
        otp = getIntent().getIntExtra("otp",0);
        fotp = String.valueOf(otp);

        forgot_tv = findViewById(R.id.forgot_otp_tv);
        resend_tv = findViewById(R.id.resend_tv);
        forgot_tv.setText(fotp);

        editText1 = (EditText) findViewById(R.id.editText1);
        editText2 = (EditText) findViewById(R.id.editText2);
        editText3 = (EditText) findViewById(R.id.editText3);
        editText4 = (EditText) findViewById(R.id.editText4);
        editText5 = (EditText) findViewById(R.id.editText5);
        editText6 = (EditText) findViewById(R.id.editText6);

        /*btn_verify = findViewById(R.id.verify_btn);
        btn_verify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(VerifyActivity.this, LoginActivity.class);
                //intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);

            }
        });*/

        editText1.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() > 0) {
                    ed1 = editText1.getText().toString().trim();
                    editText2.requestFocus();
                } else {
                }
            }

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }
        });
        editText2.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() > 0) {
                    ed2 = editText2.getText().toString().trim();
                    editText3.requestFocus();
                } else {
                }

            }

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

        });
        editText3.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() > 0) {
                    ed3 = editText3.getText().toString().trim();
                    editText4.requestFocus();
                } else {
                }

            }

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

        });
        editText4.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() > 0) {
                    ed4 = editText4.getText().toString().trim();
                    editText5.requestFocus();
                } else {
                }

            }

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

        });
        editText5.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() > 0) {
                    ed5 = editText5.getText().toString().trim();
                    editText6.requestFocus();
                } else {
                }

            }

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

        });
        editText6.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable editable) {
                ed6 = editText6.getText().toString().trim();

            }

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

        });

    }

    public void verifyUser(View view) {

            if (!editText1.getText().toString().equals("") && !editText2.getText().toString().equals("") && !editText3.getText().toString().equals("") && !editText4.getText().toString().equals("") && !editText5.getText().toString().equals("") && !editText6.getText().toString().equals("")) {
                /*Log.e("editText1", editText1.getText().toString());
                Log.e("editText2", editText2.getText().toString());
                Log.e("editText3", editText3.getText().toString());
                Log.e("editText4", editText4.getText().toString());*/

                //Intent intent = new Intent(VerifyActivity.this, LoginActivity.class);
                //intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                //startActivity(intent);

                String forgot_otp = ed1 + ed2 + ed3 + ed4 + ed5 + ed6;
                Log.e("forgot_otp:",forgot_otp);

                if (!MethodClass.isNetworkConnected(this)) {
                    Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), getResources().getString(R.string.please_check_your_internet_connection), Snackbar.LENGTH_SHORT);
                    snackbar.show();
                    return;
                }

                MethodClass.showProgressDialog(VerifyActivity.this);
                String server_url = Constants.BASE_URL + "code-verify";

                HashMap<String, String> params = new HashMap<String, String>();
                params.put("email", email);
                params.put("forget_pass_code", forgot_otp);

                JSONObject jsonObject = MethodClass.Json_rpc_format(params);

                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        MethodClass.hideProgressDialog(VerifyActivity.this);
                        Log.e("resp", response.toString());
                        try {

                            JSONObject jsonObject = MethodClass.get_result_from_webservice(VerifyActivity.this, response);

                            //if no error in response
                            if (jsonObject!= null) {
                                //Toast.makeText(getApplicationContext(), jsonObject.getString("message"), Toast.LENGTH_SHORT).show();

                        /*GsonBuilder gsonBuilder = new GsonBuilder();
                        Gson gson = gsonBuilder.create();
                        LoginMain loginMain = gson.fromJson(response.toString(), LoginMain.class);*/

                                //getting forgot password result from the response
                                //JSONObject forgotOtpJson = jsonObject.getJSONObject("result");
                                //Log.e("forgotjson:",forgotOtpJson.toString());

                                //creating a forgot otp pwd object
                                Result forgotPwdOtp = new Result(
                                        jsonObject.getString("message"),
                                        jsonObject.getString("email"),
                                        jsonObject.getInt("forget_pass_code")
                                );

                                String otp = String.valueOf(forgotPwdOtp.getPasscode());
                                //if(!forgotPwdOtp.getPasscode().isEmpty()) {
                                if(!otp.isEmpty()) {

                                    Intent intent = new Intent(VerifyActivity.this, ResetPasswordActivity.class);
                                    intent.putExtra("from","verify_password");
                                    intent.putExtra("otp",forgotPwdOtp.getPasscode());
                                    intent.putExtra("email",forgotPwdOtp.getEmail());
                                    startActivity(intent);
                                }

                            }

                        } catch (JSONException e) {
                            MethodClass.error_alert(VerifyActivity.this);
                            e.printStackTrace();
                            Log.e("forgot_parce", e.toString());
                        }


                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        MethodClass.hideProgressDialog(VerifyActivity.this);
                        if (error.toString().contains("ConnectException")) {
                            MethodClass.network_error_alert(VerifyActivity.this);
                        } else {
                            MethodClass.error_alert(VerifyActivity.this);
                            Log.e("verror",error.toString());
                        }
                    }
                });

                VolleySingleton.getInstance(VerifyActivity.this).addToRequestQueue(jsonObjectRequest);

                //SubmitForm(loadJSONFromAsset(VerifyActivity.this));
            } else {
                if (editText1.getText().toString().trim().length() == 0 || editText2.getText().toString().trim().length() == 0 || editText3.getText().toString().trim().length() == 0 || editText4.getText().toString().trim().length() == 0 || editText5.getText().toString().trim().length() == 0 || editText6.getText().toString().trim().length() == 0) {
                    Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), "Entered verification code format is invalid", Snackbar.LENGTH_LONG);
                    snackbar.show();
                    return;
                }
            }


    }

    public void resend_otp(View view) {

        MethodClass.showProgressDialog(VerifyActivity.this);
        String server_url = Constants.BASE_URL + "enter-email";

        HashMap<String, String> params = new HashMap<String, String>();
        params.put("email", email);

        JSONObject jsonObject = MethodClass.Json_rpc_format(params);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                MethodClass.hideProgressDialog(VerifyActivity.this);
                Log.e("resp", response.toString());
                try {

                    JSONObject jsonObject = MethodClass.get_result_from_webservice(VerifyActivity.this, response);

                    //if no error in response
                    if (jsonObject!= null) {
                        //Toast.makeText(getApplicationContext(), jsonObject.getString("message"), Toast.LENGTH_SHORT).show();

                        /*GsonBuilder gsonBuilder = new GsonBuilder();
                        Gson gson = gsonBuilder.create();
                        LoginMain loginMain = gson.fromJson(response.toString(), LoginMain.class);*/

                        //getting forgot password result from the response
                        //JSONObject forgotOtpJson = jsonObject.getJSONObject("result");
                        //Log.e("forgotjson:",forgotOtpJson.toString());

                        //creating a forgot otp pwd object
                        Result forgotPwdOtp = new Result(
                                jsonObject.getString("message"),
                                jsonObject.getString("email"),
                                jsonObject.getInt("forget_pass_code")
                        );

                        String votp = String.valueOf(forgotPwdOtp.getPasscode());
                        //if(!forgotPwdOtp.getPasscode().isEmpty()) {
                        if(!votp.isEmpty()) {

                            fotp = votp;
                            forgot_tv.setText(fotp);
                        }

                    }

                } catch (JSONException e) {
                    MethodClass.error_alert(VerifyActivity.this);
                    e.printStackTrace();
                    Log.e("forgot_parce", e.toString());
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                MethodClass.hideProgressDialog(VerifyActivity.this);
                if (error.toString().contains("ConnectException")) {
                    MethodClass.network_error_alert(VerifyActivity.this);
                } else {
                    MethodClass.error_alert(VerifyActivity.this);
                    Log.e("verror",error.toString());
                }
            }
        });

        VolleySingleton.getInstance(VerifyActivity.this).addToRequestQueue(jsonObjectRequest);
    }


    public void back(View view) {
        onBackPressed();
    }

}

