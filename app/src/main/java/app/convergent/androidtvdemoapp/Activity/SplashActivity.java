package app.convergent.androidtvdemoapp.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import app.convergent.androidtvdemoapp.Admin.AdminDashboardActivity;
import app.convergent.androidtvdemoapp.Helper.SharedPrefManager;
import app.convergent.androidtvdemoapp.Model_Class.LoginModel.Userdata;
import app.convergent.androidtvdemoapp.R;
import app.convergent.androidtvdemoapp.Users.activity.UserDashBoardActivity;

//import app.convergent.androidtvapp.R;

public class SplashActivity extends AppCompatActivity {

    private Button btnStart;
    public String user_type = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        btnStart = findViewById(R.id.btnStart);
        btnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(SharedPrefManager.getInstance(SplashActivity.this).isLoggedIn()) {
                    Userdata user = SharedPrefManager.getInstance(SplashActivity.this).getUser();
                    user_type = user.getUserType();

                    if(user_type.equals("A")) {

                        Intent I = new Intent(SplashActivity.this, AdminDashboardActivity.class);
                        I.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(I);
                        finish();
                    } else if(user_type.equals("U")) {
                        Intent I = new Intent(SplashActivity.this, UserDashBoardActivity.class);
                        I.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(I);
                        finish();
                    }

                } else {
                    Intent I = new Intent(SplashActivity.this, LoginActivity.class);
                    I.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(I);
                    finish();
                }

            }
        });

        /*new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {


                if(SharedPrefManager.getInstance(SplashActivity.this).isLoggedIn()) {
                    Userdata user = SharedPrefManager.getInstance(SplashActivity.this).getUser();
                    user_type = user.getUserType();

                    if(user_type.equals("A")) {

                        Intent I = new Intent(SplashActivity.this, AdminDashboardActivity.class);
                        I.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(I);
                        finish();
                    } else if(user_type.equals("U")) {
                        Intent I = new Intent(SplashActivity.this, UserDashBoardActivity.class);
                        I.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(I);
                        finish();
                    }

                } else {
                    Intent I = new Intent(SplashActivity.this, LoginActivity.class);
                    I.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(I);
                    finish();
                }

                    *//*if (PreferenceManager.getDefaultSharedPreferences(SplashActivity.this).getBoolean("is_logged_in", false)) {

                        String log_in = String.valueOf((PreferenceManager.getDefaultSharedPreferences(SplashActivity.this).getBoolean("is_logged_in", false)));
                        String ufname = (PreferenceManager.getDefaultSharedPreferences(SplashActivity.this).getString("fname", ""));
                        String ulname = (PreferenceManager.getDefaultSharedPreferences(SplashActivity.this).getString("lname", ""));
                        String utoken = (PreferenceManager.getDefaultSharedPreferences(SplashActivity.this).getString("token", ""));
                        String user_id = (PreferenceManager.getDefaultSharedPreferences(SplashActivity.this).getString("user_id", ""));
                        String uemail = (PreferenceManager.getDefaultSharedPreferences(SplashActivity.this).getString("email", ""));

                        Toast.makeText(SplashActivity.this,"is_logged_in:"+ log_in + "\nfname:" + ufname + "\nlname:" + ulname + "\nuser_id:" + user_id + "\nemail:" + uemail + "\ntoken:" + utoken,Toast.LENGTH_LONG).show();

                        Intent I = new Intent(SplashActivity.this, LoginActivity.class);
                        I.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(I);
                        finish();

                    } else {

                        String log_in = String.valueOf((PreferenceManager.getDefaultSharedPreferences(SplashActivity.this).getBoolean("is_logged_in", false)));
                        String ufname = (PreferenceManager.getDefaultSharedPreferences(SplashActivity.this).getString("fname", ""));
                        String ulname = (PreferenceManager.getDefaultSharedPreferences(SplashActivity.this).getString("lname", ""));
                        String utoken = (PreferenceManager.getDefaultSharedPreferences(SplashActivity.this).getString("token", ""));
                        String user_id = (PreferenceManager.getDefaultSharedPreferences(SplashActivity.this).getString("user_id", ""));
                        String uemail = (PreferenceManager.getDefaultSharedPreferences(SplashActivity.this).getString("email", ""));

                        Toast.makeText(SplashActivity.this,"is_logged_in:"+ log_in + "\nfname:" + ufname + "\nlname:" + ulname + "\nuser_id:" + user_id + "\nemail:" + uemail + "\ntoken:" + utoken,Toast.LENGTH_LONG).show();

                        Intent I = new Intent(SplashActivity.this, LoginActivity.class);
                        I.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(I);
                        finish();
                    }*//*

                }

        }, 2000);*/


    }

    /*public void getStarted(View view){


        if(SharedPrefManager.getInstance(SplashActivity.this).getUser().getUserType().equals("A")) {
            Intent I = new Intent(SplashActivity.this, AdminDashboardActivity.class);
            I.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(I);
            finish();
        } else if(SharedPrefManager.getInstance(SplashActivity.this).getUser().getUserType().equals("U")) {
            Intent I = new Intent(SplashActivity.this, UserDashBoardActivity.class);
            I.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(I);
            finish();
        } else {
            Intent I = new Intent(SplashActivity.this, LoginActivity.class);
            I.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(I);
            finish();
        }

    }*/
}