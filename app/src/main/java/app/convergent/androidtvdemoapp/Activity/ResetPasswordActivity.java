package app.convergent.androidtvdemoapp.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import app.convergent.androidtvdemoapp.Helper.Constants;
import app.convergent.androidtvdemoapp.Helper.MethodClass;
import app.convergent.androidtvdemoapp.Helper.VolleySingleton;
import app.convergent.androidtvdemoapp.Model_Class.ForgotPasswordOtpEmail.Result;
import app.convergent.androidtvdemoapp.R;

//import app.convergent.androidtvapp.R;

public class ResetPasswordActivity extends AppCompatActivity {

    private EditText pwd_edText1, cpwd_edText2;
    private Button fpwd_button1, fcpwd_button2;
    public String pwd, cpwd, email, fotp;
    public int otp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);

        TextView title = findViewById(R.id.title);
        title.setText(getString(R.string.change_pwd));

        email = getIntent().getStringExtra("email");
        otp = getIntent().getIntExtra("otp",0);
        fotp = String.valueOf(otp);

        pwd_edText1 = findViewById(R.id.fpass);
        cpwd_edText2 = findViewById(R.id.fcpass);

        fpwd_button1 = findViewById(R.id.fpwd_button1);
        fcpwd_button2 = findViewById(R.id.fcpwd_button2);

        fpwd_button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pwd_edText1.getInputType() == InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD) {
                    pwd_edText1.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    pwd_edText1.setTransformationMethod(new PasswordTransformationMethod());
                } else {
                    pwd_edText1.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                    pwd_edText1.setTransformationMethod(new HideReturnsTransformationMethod());
                }
            }
        });

        fcpwd_button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cpwd_edText2.getInputType() == InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD) {
                    cpwd_edText2.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    cpwd_edText2.setTransformationMethod(new PasswordTransformationMethod());
                } else {
                    cpwd_edText2.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                    cpwd_edText2.setTransformationMethod(new HideReturnsTransformationMethod());
                }
            }
        });
    }

    public void fpwd_submit(View view) {

        pwd = pwd_edText1.getText().toString().trim();
        cpwd = cpwd_edText2.getText().toString().trim();

        if (pwd.length() == 0) {
            pwd_edText1.setError(getString(R.string.enterpas));
            pwd_edText1.requestFocus();
            return;
        }
        if (cpwd.length() == 0) {
            cpwd_edText2.setError(getString(R.string.enterpas));
            cpwd_edText2.requestFocus();
            return;
        }

        if(!pwd.equals(cpwd)) {
            Toast.makeText(ResetPasswordActivity.this,"Password doesn't match!",Toast.LENGTH_SHORT).show();
            return;
        }

        if (!MethodClass.isNetworkConnected(this)) {
            Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), getResources().getString(R.string.please_check_your_internet_connection), Snackbar.LENGTH_SHORT);
            snackbar.show();
            return;
        }

        MethodClass.showProgressDialog(ResetPasswordActivity.this);
        String server_url = Constants.BASE_URL + "new-password";

        HashMap<String, String> params = new HashMap<String, String>();
        params.put("email", email);
        params.put("forget_pass_code", fotp);
        params.put("new_password", pwd);
        params.put("new_password_confirmation", cpwd);

        JSONObject jsonObject = MethodClass.Json_rpc_format(params);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                MethodClass.hideProgressDialog(ResetPasswordActivity.this);
                Log.e("resp", response.toString());
                try {

                    JSONObject jsonObject = MethodClass.get_result_from_webservice(ResetPasswordActivity.this, response);

                    //if no error in response
                    if (jsonObject!= null) {
                        //Toast.makeText(getApplicationContext(), jsonObject.getString("message"), Toast.LENGTH_SHORT).show();

                        /*GsonBuilder gsonBuilder = new GsonBuilder();
                        Gson gson = gsonBuilder.create();
                        LoginMain loginMain = gson.fromJson(response.toString(), LoginMain.class);*/

                        //getting forgot password result from the response
                        //JSONObject forgotOtpJson = jsonObject.getJSONObject("result");
                        //Log.e("forgotjson:",forgotOtpJson.toString());



                        String message = jsonObject.getString("message");
                        if(!message.isEmpty()) {

                            Toast.makeText(ResetPasswordActivity.this,message,Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(ResetPasswordActivity.this, LoginActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                            finish();
                        }

                    }

                } catch (JSONException e) {
                    MethodClass.error_alert(ResetPasswordActivity.this);
                    e.printStackTrace();
                    Log.e("forgot_parce", e.toString());
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                MethodClass.hideProgressDialog(ResetPasswordActivity.this);
                if (error.toString().contains("ConnectException")) {
                    MethodClass.network_error_alert(ResetPasswordActivity.this);
                } else {
                    MethodClass.error_alert(ResetPasswordActivity.this);
                    Log.e("verror",error.toString());
                }
            }
        });

        VolleySingleton.getInstance(ResetPasswordActivity.this).addToRequestQueue(jsonObjectRequest);

    }

    public void back(View view) {
        onBackPressed();
    }
}

