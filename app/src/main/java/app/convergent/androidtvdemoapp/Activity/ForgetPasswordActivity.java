package app.convergent.androidtvdemoapp.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import app.convergent.androidtvdemoapp.Helper.Constants;
import app.convergent.androidtvdemoapp.Helper.MethodClass;
import app.convergent.androidtvdemoapp.Helper.VolleySingleton;
import app.convergent.androidtvdemoapp.Model_Class.ForgotPasswordOtpEmail.Result;
import app.convergent.androidtvdemoapp.R;

//import app.convergent.androidtvapp.R;

public class ForgetPasswordActivity extends AppCompatActivity {

    private EditText email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_password);

        TextView title = findViewById(R.id.title);
        title.setText(getString(R.string.forget_user));

        email = findViewById(R.id.forgot_email);

    }

    public void forgot_submit(View view) {

        String get_email = "";
        get_email = email.getText().toString().trim();

        if (get_email.length() == 0) {
            email.setError("please enter valid email address");
            email.requestFocus();
            return;
        }

        if (!MethodClass.emailValidator(get_email)) {
            email.setError("please enter valid email address");
            email.requestFocus();
            return;
        }

        if (!MethodClass.isNetworkConnected(this)) {
            Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), getResources().getString(R.string.please_check_your_internet_connection), Snackbar.LENGTH_SHORT);
            snackbar.show();
            return;
        }

        MethodClass.showProgressDialog(ForgetPasswordActivity.this);
        String server_url = Constants.BASE_URL + "enter-email";

        /*if (get_email.equalsIgnoreCase("admin@gmail.com")) {

            Intent intent = new Intent(ForgetPasswordActivity.this, VerifyActivity.class);
            //intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }else if ((get_email.equalsIgnoreCase("user@gmail.com"))){
            Intent intent = new Intent(ForgetPasswordActivity.this, VerifyActivity.class);
            //intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        } else
            Toast.makeText(ForgetPasswordActivity.this, "Registered email doesn't exist !!", Toast.LENGTH_SHORT).show();*/

        /*Intent intent = new Intent(ForgetPasswordActivity.this, VerifyActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);*/

        HashMap<String, String> params = new HashMap<String, String>();
        params.put("email", get_email);

        JSONObject jsonObject = MethodClass.Json_rpc_format(params);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                MethodClass.hideProgressDialog(ForgetPasswordActivity.this);
                Log.e("resp", response.toString());
                try {

                    JSONObject jsonObject = MethodClass.get_result_from_webservice(ForgetPasswordActivity.this, response);

                    //if no error in response
                    if (jsonObject!= null) {
                        //Toast.makeText(getApplicationContext(), jsonObject.getString("message"), Toast.LENGTH_SHORT).show();

                        /*GsonBuilder gsonBuilder = new GsonBuilder();
                        Gson gson = gsonBuilder.create();
                        LoginMain loginMain = gson.fromJson(response.toString(), LoginMain.class);*/

                        //getting forgot password result from the response
                        //JSONObject forgotOtpJson = jsonObject.getJSONObject("result");
                        //Log.e("forgotjson:",forgotOtpJson.toString());

                        //creating a forgot otp pwd object
                        Result forgotPwdOtp = new Result(
                                jsonObject.getString("message"),
                                jsonObject.getString("email"),
                                jsonObject.getInt("forget_pass_code")
                        );

                        String otp = String.valueOf(forgotPwdOtp.getPasscode());
                        //if(!forgotPwdOtp.getPasscode().isEmpty()) {
                        if(!otp.isEmpty()) {

                            Intent intent = new Intent(ForgetPasswordActivity.this, VerifyActivity.class);
                            intent.putExtra("from","forgot_password");
                            intent.putExtra("otp",forgotPwdOtp.getPasscode());
                            intent.putExtra("email",forgotPwdOtp.getEmail());
                            startActivity(intent);
                        }

                    }

                } catch (JSONException e) {
                    MethodClass.error_alert(ForgetPasswordActivity.this);
                    e.printStackTrace();
                    Log.e("forgot_parce", e.toString());
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                MethodClass.hideProgressDialog(ForgetPasswordActivity.this);
                if (error.toString().contains("ConnectException")) {
                    MethodClass.network_error_alert(ForgetPasswordActivity.this);
                } else {
                    MethodClass.error_alert(ForgetPasswordActivity.this);
                    Log.e("verror",error.toString());
                }
            }
        });

        VolleySingleton.getInstance(ForgetPasswordActivity.this).addToRequestQueue(jsonObjectRequest);
    }
    public void login(View view) {
        String get_email = "", get_pswrd = "";
        /*get_email = email_et.getText().toString().trim();
        get_pswrd = pass.getText().toString().trim();

        if (get_email.length() == 0) {
            email_et.setError("please enter valid email address");
            email_et.requestFocus();
            return;
        }
        if (!MethodClass.emailValidator(get_email)) {
            email_et.setError("please enter valid email address");
            email_et.requestFocus();
            return;
        }
        if (get_pswrd.length() == 0) {
            pass.setError(getString(R.string.enterpas));
            pass.requestFocus();
            return;*/


        Intent intent = new Intent(ForgetPasswordActivity.this, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        }

    public void submit(View view) {


    }

    public void back(View view) {
        onBackPressed();
    }
}
