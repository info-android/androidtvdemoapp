package app.convergent.androidtvdemoapp.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import app.convergent.androidtvdemoapp.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}