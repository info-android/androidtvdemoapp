package app.convergent.androidtvdemoapp.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import app.convergent.androidtvdemoapp.Admin.AdminDashboardActivity;
import app.convergent.androidtvdemoapp.Helper.Constants;
import app.convergent.androidtvdemoapp.Helper.MethodClass;
/*import app.convergent.androidtvdemoapp.Helper.SessionManager;
import app.convergent.androidtvdemoapp.Model_Class.LoginModel.LoginMain;*/
import app.convergent.androidtvdemoapp.Helper.SharedPrefManager;
import app.convergent.androidtvdemoapp.Helper.VolleySingleton;
import app.convergent.androidtvdemoapp.Model_Class.LoginModel.Userdata;
import app.convergent.androidtvdemoapp.R;
import app.convergent.androidtvdemoapp.Users.activity.UserDashBoardActivity;

/*import app.convergent.androidtvapp.Admin.AdminDashboardActivity;
import app.convergent.androidtvapp.Helper.MethodClass;
import app.convergent.androidtvapp.R;
import app.convergent.androidtvapp.Users.UserDashBoardActivity;*/

public class LoginActivity extends AppCompatActivity {

    private EditText email_et, pass;
    private Button button1;
    public static String email = "";
    public String get_pswrd = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        email_et = findViewById(R.id.email_et);
        pass = findViewById(R.id.pass);
        button1 = findViewById(R.id.button1);

        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pass.getInputType() == InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD) {
                    pass.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    pass.setTransformationMethod(new PasswordTransformationMethod());
                } else {
                    pass.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                    pass.setTransformationMethod(new HideReturnsTransformationMethod());
                }
            }
        });

    }

    public void login(View view) {
        String get_email = "";
        get_email = email_et.getText().toString().trim();
        get_pswrd = pass.getText().toString().trim();

       if (get_email.length() == 0) {
           email_et.setError("please enter valid email address");
           email_et.requestFocus();
           return;
       }
       if (!MethodClass.emailValidator(get_email)) {
           email_et.setError("please enter valid email address");
           email_et.requestFocus();
           return;
       }
        if (get_pswrd.length() == 0) {
           pass.setError(getString(R.string.enterpas));
           pass.requestFocus();
           return;
       }

        /*if (!get_pswrd.equalsIgnoreCase("123456")) {

            Toast.makeText(LoginActivity.this, "Incorrect password !!", Toast.LENGTH_SHORT).show();
        }

        if (get_email.equalsIgnoreCase("admin@gmail.com")) {

            Intent intent = new Intent(LoginActivity.this, AdminDashboardActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }else if ((get_email.equalsIgnoreCase("user@gmail.com"))){
            Intent intent = new Intent(LoginActivity.this, UserDashBoardActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        } else
            Toast.makeText(LoginActivity.this, "Incorrect email id !!", Toast.LENGTH_SHORT).show();*/



        if (!MethodClass.isNetworkConnected(this)) {
            Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), getResources().getString(R.string.please_check_your_internet_connection), Snackbar.LENGTH_SHORT);
            snackbar.show();
            return;
        }

        MethodClass.showProgressDialog(LoginActivity.this);

        String server_url = Constants.BASE_URL + "login";

        HashMap<String, String> params = new HashMap<String, String>();
        params.put("email", get_email);
        params.put("password", get_pswrd);

        JSONObject jsonObject = MethodClass.Json_rpc_format(params);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                MethodClass.hideProgressDialog(LoginActivity.this);
                Log.e("resp", response.toString());
                try {

                    JSONObject jsonObject = MethodClass.get_result_from_webservice(LoginActivity.this, response);

                    //if no error in response
                    if (jsonObject!= null) {
                        //Toast.makeText(getApplicationContext(), jsonObject.getString("message"), Toast.LENGTH_SHORT).show();

                        /*GsonBuilder gsonBuilder = new GsonBuilder();
                        Gson gson = gsonBuilder.create();
                        LoginMain loginMain = gson.fromJson(response.toString(), LoginMain.class);*/

                        //getting user from the response
                        JSONObject userJson = jsonObject.getJSONObject("userdata");

                        String token = jsonObject.getString("token");

                        //creating a new user object
                        Userdata user = new Userdata(
                                userJson.getInt("id"),
                                userJson.getString("name"),
                                userJson.getString("email"),
                                userJson.getString("user_type")
                        );

                        //storing the user in shared preferences
                        SharedPrefManager.getInstance(getApplicationContext()).userLogin(user);

                        //storing the user token
                        SharedPrefManager.getInstance(getApplicationContext()).userToken(token);

                        //storing the user password
                        //SharedPrefManager.getInstance(getApplicationContext()).userPassword(get_pswrd);

                        if(user.getUserType().equals("A")) {
                            startActivity(new Intent(getApplicationContext(), AdminDashboardActivity.class));
                            finish();
                        }
                        else if(user.getUserType().equals("U")) {
                            startActivity(new Intent(getApplicationContext(), UserDashBoardActivity.class));
                            finish();
                        }
                        else {
                            Toast.makeText(getApplicationContext(), jsonObject.getString("Incorrect mail id !!"), Toast.LENGTH_SHORT).show();
                        }

                    }

                } catch (JSONException e) {
                    MethodClass.error_alert(LoginActivity.this);
                    e.printStackTrace();
                    Log.e("login_parce", e.toString());
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                MethodClass.hideProgressDialog(LoginActivity.this);
                if (error.toString().contains("ConnectException")) {
                    MethodClass.network_error_alert(LoginActivity.this);
                } else {
                    MethodClass.error_alert(LoginActivity.this);
                }
            }
        });

        VolleySingleton.getInstance(LoginActivity.this).addToRequestQueue(jsonObjectRequest);

    }


    public void forgotPassword(View view) {
        Intent intent = new Intent(this, ForgetPasswordActivity.class);
        if (getIntent().getExtras() != null) {
            if (getIntent().getStringExtra("type").equals("C")) {
                intent.putExtra("type", "C");
            } else {
                intent.putExtra("type", "S");
            }
        } else {
            intent.putExtra("type", "S");
        }
        startActivity(intent);
    }

}