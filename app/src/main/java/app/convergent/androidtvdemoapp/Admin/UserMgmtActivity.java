package app.convergent.androidtvdemoapp.Admin;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import app.convergent.androidtvdemoapp.Activity.SplashActivity;
import app.convergent.androidtvdemoapp.Adapter.ItemAdapter;
import app.convergent.androidtvdemoapp.Helper.Constants;
import app.convergent.androidtvdemoapp.Helper.MethodClass;
import app.convergent.androidtvdemoapp.Helper.SharedPrefManager;
import app.convergent.androidtvdemoapp.Helper.VolleySingleton;
import app.convergent.androidtvdemoapp.Model_Class.AdminDashboard.MyProfile;
import app.convergent.androidtvdemoapp.Model_Class.LoginModel.Userdata;
import app.convergent.androidtvdemoapp.Model_Class.UserList.UserDetailsList;
import app.convergent.androidtvdemoapp.R;

import static android.accounts.AccountManager.KEY_PASSWORD;
import static app.convergent.androidtvdemoapp.Helper.SharedPrefManager.KEY_TOKEN;

/*import app.convergent.androidtvapp.Adapter.ItemAdapter;
import app.convergent.androidtvapp.Helper.MethodClass;
import app.convergent.androidtvapp.R;*/

public class UserMgmtActivity extends AppCompatActivity {

    public RecyclerView recy_view;
    private ArrayList<UserDetailsList> user_list = new ArrayList<>();
    ItemAdapter itemAdapter;
    CardView cardView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_mgmt);
        recy_view=findViewById(R.id.recy_view);

        cardView = findViewById(R.id.nodata_cardView);

        //list();

    }

    public void list() {

        String utoken = String.valueOf(SharedPrefManager.getInstance(UserMgmtActivity.this).getUserToken(UserMgmtActivity.this,KEY_TOKEN));

        if(MethodClass.isNetworkConnected(this)) {
            MethodClass.showProgressDialog(UserMgmtActivity.this);

            String server_url = Constants.BASE_URL + "user-list";

            HashMap<String, String> params = new HashMap<String, String>();

            JSONObject jsonObject = MethodClass.Json_rpc_format(params);

            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, server_url, jsonObject, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    MethodClass.hideProgressDialog(UserMgmtActivity.this);
                    Log.e("resp", response.toString());
                    try {

                        JSONObject jsonObj = MethodClass.get_result_from_webservice(UserMgmtActivity.this, response);

                        //if no error in response
                        if (jsonObj!= null) {
                            JSONArray user_jsonArray = jsonObj.getJSONArray("all_user");
                            Log.e("user_array:", String.valueOf(user_jsonArray));

                            if (user_jsonArray.length() != 0) {
                                Log.e("user_jsonArray", String.valueOf(user_jsonArray.length()));

                                user_list.clear();
                                cardView.setVisibility(View.GONE);

                                for (int i = 0; i < user_jsonArray.length(); i++) {

                                    UserDetailsList userDetailsList = new UserDetailsList();

                                    userDetailsList.setId(Integer.parseInt(user_jsonArray.getJSONObject(i).getString("id")));
                                    userDetailsList.setName(user_jsonArray.getJSONObject(i).getString("name"));
                                    userDetailsList.setEmail(user_jsonArray.getJSONObject(i).getString("email"));
                                    userDetailsList.setUser_type(user_jsonArray.getJSONObject(i).getString("user_type"));
                                    userDetailsList.setStatus(user_jsonArray.getJSONObject(i).getString("status"));
                                    userDetailsList.setPh(user_jsonArray.getJSONObject(i).getString("ph"));

                                    user_list.add(userDetailsList);
                                    Log.e("all_user:", String.valueOf(user_list));

                                }


                                itemAdapter = new ItemAdapter(UserMgmtActivity.this,user_list);
                                recy_view.setAdapter(itemAdapter);
                                recy_view.setFocusable(false);
                                itemAdapter.notifyDataSetChanged();
                            }
                            else if(user_jsonArray.length() == 0) {
                                cardView.setVisibility(View.VISIBLE);
                            }
                            else {

                                if (recy_view.getAdapter() != null) {
                                    itemAdapter.update(user_list);
                                    recy_view.getAdapter().notifyDataSetChanged();

                                    recy_view.setVisibility(View.GONE);
                                    cardView.setVisibility(View.VISIBLE);
                                }
                            }


                        }

                    } catch (JSONException e) {
                        MethodClass.error_alert(UserMgmtActivity.this);
                        e.printStackTrace();
                        Log.e("user_parce", e.toString());
                    }


                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    MethodClass.hideProgressDialog(UserMgmtActivity.this);
                    if (error.toString().contains("ConnectException")) {
                        MethodClass.network_error_alert(UserMgmtActivity.this);
                    } else {
                        MethodClass.error_alert(UserMgmtActivity.this);
                        Log.e("user_parce2", error.toString());
                    }
                }

            }){
                //* Passing some request headers*
                @Override
                public Map getHeaders() throws AuthFailureError {
                    HashMap headers = new HashMap();
                    headers.put("Content-Type", "application/json");
                    headers.put("Authorization", "Bearer " + utoken);

                    Log.e("getHeaders: ", headers.toString());

                    return headers;
                }
            };

            VolleySingleton.getInstance(UserMgmtActivity.this).addToRequestQueue(jsonObjectRequest);


        } else {

            Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), getResources().getString(R.string.no_internet_connection), Snackbar.LENGTH_SHORT);
            snackbar.show();
            return;
        }

        /*ArrayList<HashMap<String,String>> arrayList=new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            HashMap<String,String> hashMap=new HashMap<>();
            hashMap.put("key","value");
            arrayList.add(hashMap);
        }

        ItemAdapter adapter = new ItemAdapter(this,arrayList);
        recy_view.setAdapter(adapter);
        recy_view.setFocusable(false);*/

    }

    @Override
    protected void onResume() {
        super.onResume();
        MethodClass.setMenu(this);

        list();


    }


    public void member_add_member(View view) {

        Intent intent = new Intent(UserMgmtActivity.this,UserAddActivity.class);
        startActivity(intent);

    }

    @SuppressLint("WrongConstant")
    public void home_menu(View view) {
        DrawerLayout drawer_layout = findViewById(R.id.drawer_layout);
        drawer_layout.openDrawer(Gravity.START);
    }

}
