package app.convergent.androidtvdemoapp.Admin;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import javax.microedition.khronos.egl.EGLDisplay;

import app.convergent.androidtvdemoapp.Helper.Constants;
import app.convergent.androidtvdemoapp.Helper.MethodClass;
import app.convergent.androidtvdemoapp.Helper.SharedPrefManager;
import app.convergent.androidtvdemoapp.R;

import static app.convergent.androidtvdemoapp.Helper.SharedPrefManager.KEY_TOKEN;

/*import app.convergent.androidtvapp.Helper.MethodClass;
import app.convergent.androidtvapp.R;*/

public class UserEditActivity extends AppCompatActivity {

    public String user_id, user_name, user_phone, user_email;
    private EditText uname, uphone, umail;
    int uid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_edit);

        TextView title = findViewById(R.id.admin_dash_title);
        title.setText(getString(R.string.edit_member));

        uid = getIntent().getIntExtra("user_id",0);
        user_id = String.valueOf(uid);
        user_name = getIntent().getStringExtra("user_name");
        user_phone = getIntent().getStringExtra("user_phone");
        user_email = getIntent().getStringExtra("user_email");

        uname = findViewById(R.id.uname);
        umail = findViewById(R.id.uemail);
        uphone = findViewById(R.id.uphone);

        uname.setText(user_name);
        umail.setText(user_email);
        uphone.setText(user_phone);

    }

    public void editMember(View view) {

        String get_user_name = uname.getText().toString().trim();
        String get_user_phone = uphone.getText().toString().trim();
        String utoken = String.valueOf(SharedPrefManager.getInstance(UserEditActivity.this).getUserToken(UserEditActivity.this,KEY_TOKEN));

        if (get_user_name.length() == 0) {
            uname.setError(getString(R.string.entername));
            uname.requestFocus();
            return;
        }
        if (get_user_phone.length() == 0) {
            uphone.setError(getString(R.string.enterph));
            uphone.requestFocus();
            return;
        }

        if(MethodClass.isNetworkConnected(this)) {
            MethodClass.showProgressDialog(UserEditActivity.this);

            String server_url = Constants.BASE_URL + "single-user-view";

            StringRequest stringRequest = new StringRequest(Request.Method.POST, server_url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {
                        // converting the string to json array object
                        Log.d("Response:", response);
                        //Toast.makeText(EditProfileActivity.this, response, Toast.LENGTH_SHORT).show();
                        //if(response.equals("success")) {
                        JSONObject object = new JSONObject(response);

                        String userMsg = object.getString("message");
                        MethodClass.hideProgressDialog(UserEditActivity.this);
                        Toast.makeText(UserEditActivity.this, userMsg, Toast.LENGTH_SHORT).show();

                        /*if (object.has("result")) {

                            JSONObject successResponse = object.getJSONObject("result");

                            if (successResponse.has("message")) {


                                MethodClass.hideProgressDialog(UserAddActivity.this);
                                //JSONObject userMsg = successResponse.getJSONObject("message");
                                String msg = successResponse.getString("message");


                                Toast.makeText(UserAddActivity.this, msg, Toast.LENGTH_SHORT).show();
                            }

                        }*/

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            },
                    new com.android.volley.Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                            Toast.makeText(UserEditActivity.this, "Connection Error!", Toast.LENGTH_SHORT).show();
                            Log.d("Connection Error!", String.valueOf(error));
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<>();
                    //Adding parameters to request
                    params.put("name", get_user_name);
                    params.put("ph", get_user_phone);
                    params.put("user_id", user_id);
                    //params.put("user_type", get_user_type);


                    Log.e("param:", String.valueOf(params));
                    //returning parameter
                    return params;


                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("Authorization", "bearer" + utoken);
                    return headers;
                }

            };

            // Adding the string request to the queue
            RequestQueue requestQueue = Volley.newRequestQueue(UserEditActivity.this);
            requestQueue.add(stringRequest);

        } else {

            Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), getResources().getString(R.string.no_internet_connection), Snackbar.LENGTH_SHORT);
            snackbar.show();
            return;

        }

    }
    @Override
    protected void onResume() {
        super.onResume();
        MethodClass.setMenu(this);

    }

    @SuppressLint("WrongConstant")
    public void admin_home_menu(View view) {
        DrawerLayout drawer_layout = findViewById(R.id.drawer_layout);
        drawer_layout.openDrawer(Gravity.START);
    }

}
