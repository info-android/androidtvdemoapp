package app.convergent.androidtvdemoapp.Admin;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import app.convergent.androidtvdemoapp.Activity.MainActivity;
import app.convergent.androidtvdemoapp.Helper.Constants;
import app.convergent.androidtvdemoapp.Helper.MethodClass;
import app.convergent.androidtvdemoapp.Helper.SharedPrefManager;
import app.convergent.androidtvdemoapp.Helper.VolleySingleton;
import app.convergent.androidtvdemoapp.Model_Class.LoginModel.Userdata;
import app.convergent.androidtvdemoapp.R;

import static android.accounts.AccountManager.KEY_PASSWORD;
import static app.convergent.androidtvdemoapp.Helper.SharedPrefManager.KEY_TOKEN;

/*import app.convergent.androidtvapp.Helper.MethodClass;
import app.convergent.androidtvapp.R;*/

public class UserAddActivity extends AppCompatActivity {

    private EditText user_name, user_email, user_phone;
    Button saveUser_btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_add);

        TextView title = findViewById(R.id.admin_dash_title);
        title.setText(getString(R.string.add_member));

        user_name = findViewById(R.id.user_name);
        user_email = findViewById(R.id.user_email);
        user_phone = findViewById(R.id.user_phone);
        saveUser_btn = findViewById(R.id.saveUser_btn);

        saveUser_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveUser();
            }
        });

    }

    public void saveUser() {

        String get_user_name = user_name.getText().toString().trim();
        String get_user_email = user_email.getText().toString().trim();
        String get_user_ph = user_phone.getText().toString().trim();

        Userdata user = SharedPrefManager.getInstance(UserAddActivity.this).getUser();
        String get_user_type = user.getUserType();
        String adm_pwd = String.valueOf(SharedPrefManager.getInstance(UserAddActivity.this).getUserPassword(UserAddActivity.this,KEY_PASSWORD));
        String utoken = String.valueOf(SharedPrefManager.getInstance(UserAddActivity.this).getUserToken(UserAddActivity.this,KEY_TOKEN));

        if (get_user_name.length() == 0) {
            user_name.setError(getString(R.string.entername));
            user_name.requestFocus();
            return;
        }
        if (get_user_email.length() == 0) {
            user_email.setError(getString(R.string.entermail));
            user_email.requestFocus();
            return;
        }
        if (!MethodClass.emailValidator(get_user_email)) {
            user_email.setError("please enter valid email address");
            user_email.requestFocus();
            return;
        }
        if (get_user_ph.length() == 0 ) {
            user_phone.setError(getString(R.string.enterph));
            user_phone.requestFocus();
            return;
        }

        if (get_user_ph.length() < 10){
            user_phone.setError(getString(R.string.please_enter_valid_phone_number));
            user_phone.requestFocus();
            return;
        }


        if (VolleySingleton.getInstance(this).isConnected()){
            MethodClass.showProgressDialog(UserAddActivity.this);

            String server_url = Constants.BASE_URL + "register";
            StringRequest stringRequest=new StringRequest(Request.Method.POST, server_url, response -> {
                Log.d("registerResponse:",response);


                try {
                    JSONObject object = new JSONObject(response);
                    if (object.has("result")) {

                        JSONObject successResponse = object.getJSONObject("result");

                        if (successResponse.has("message")) {

                            MethodClass.hideProgressDialog(UserAddActivity.this);
                            String msg = successResponse.getString("message");

                            user_name.setText("");
                            user_email.setText("");
                            user_phone.setText("");

                            Toast.makeText(UserAddActivity.this, msg, Toast.LENGTH_SHORT).show();
                            MethodClass.go_to_next_activity(UserAddActivity.this, AdminDashboardActivity.class);
                        }

                    }
                    else if(object.has("error")){

                        MethodClass.hideProgressDialog(UserAddActivity.this);
                        JSONObject errorResponse = object.getJSONObject("error");

                        if(errorResponse.has("message")) {

                            String errMsg = errorResponse.getString("message");

                            Toast.makeText(UserAddActivity.this, errMsg, Toast.LENGTH_SHORT).show();

                        }


                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }




            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(UserAddActivity.this, "Connection Error!", Toast.LENGTH_SHORT).show();
                    Log.d("Connection Error!", String.valueOf(error));
                }
            }){
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<>();
                    params.put("name", get_user_name);
                    params.put("email", get_user_email);
                    params.put("ph", get_user_ph);

                    //params.put("password", adm_pwd);
                    //params.put("user_type", get_user_type);

                    params.put("password", adm_pwd);

                    params.put("user_type", "U");
                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("Authorization", "bearer" + utoken);
                    return headers;
                }

            };

            stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                    0,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleySingleton.getInstance(this).addToRequestQueue(stringRequest);
        } else {
            Toast.makeText(this, "No internet connection", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        MethodClass.setMenu(this);

    }

    @SuppressLint("WrongConstant")
    public void admin_home_menu(View view) {
        DrawerLayout drawer_layout = findViewById(R.id.drawer_layout);
        drawer_layout.openDrawer(Gravity.START);
    }

}

