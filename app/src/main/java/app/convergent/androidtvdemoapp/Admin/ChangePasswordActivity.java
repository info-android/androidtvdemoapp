package app.convergent.androidtvdemoapp.Admin;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import app.convergent.androidtvdemoapp.Helper.Constants;
import app.convergent.androidtvdemoapp.Helper.MethodClass;
import app.convergent.androidtvdemoapp.Helper.SharedPrefManager;
import app.convergent.androidtvdemoapp.Helper.VolleySingleton;
import app.convergent.androidtvdemoapp.R;
import app.convergent.androidtvdemoapp.Users.activity.UserChangePasswordActivity;

import static app.convergent.androidtvdemoapp.Helper.SharedPrefManager.KEY_TOKEN;

/*import app.convergent.androidtvapp.Helper.MethodClass;
import app.convergent.androidtvapp.R;*/

public class ChangePasswordActivity extends AppCompatActivity {
    private Button admin_button1, admin_button2, admin_button3;
    private EditText old_pass, confm_pass, pass;
    public String get_confm_pswrd = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);

        TextView title = findViewById(R.id.admin_dash_title);
        title.setText(getString(R.string.change_pwd));

        old_pass = findViewById(R.id.admin_opass1);
        confm_pass = findViewById(R.id.admin_confpass1);
        pass = findViewById(R.id.admin_pass1);
        admin_button1 = findViewById(R.id.admin_old_button1);
        admin_button2 = findViewById(R.id.admin_button1);
        admin_button3 = findViewById(R.id.admin_button2);


        admin_button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (old_pass.getInputType() == InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD) {
                    old_pass.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    old_pass.setTransformationMethod(new PasswordTransformationMethod());
                } else {
                    old_pass.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                    old_pass.setTransformationMethod(new HideReturnsTransformationMethod());
                }
            }
        });

        admin_button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pass.getInputType() == InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD) {
                    pass.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    pass.setTransformationMethod(new PasswordTransformationMethod());
                } else {
                    pass.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                    pass.setTransformationMethod(new HideReturnsTransformationMethod());
                }
            }
        });

        admin_button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (confm_pass.getInputType() == InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD) {
                    confm_pass.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    confm_pass.setTransformationMethod(new PasswordTransformationMethod());
                } else {
                    confm_pass.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                    confm_pass.setTransformationMethod(new HideReturnsTransformationMethod());
                }
            }
        });
    }

    public void changePwd(View view) {

        String get_old_pswrd = "", get_new_pswrd = "";
        get_old_pswrd = old_pass.getText().toString().trim();
        get_new_pswrd = pass.getText().toString().trim();
        get_confm_pswrd = confm_pass.getText().toString().trim();

        if (get_old_pswrd.length() == 0) {
            old_pass.setError(getString(R.string.enterpas));
            old_pass.requestFocus();
            return;
        }

        if (get_new_pswrd.length() == 0) {
            pass.setError(getString(R.string.enterpas));
            pass.requestFocus();
            return;
        }

        if (get_confm_pswrd.length() == 0) {
            confm_pass.setError(getString(R.string.enterpas));
            confm_pass.requestFocus();
            return;
        }

        if(!get_new_pswrd.equals(get_confm_pswrd)) {
            Toast.makeText(ChangePasswordActivity.this,"Password doesn't match !",Toast.LENGTH_LONG).show();
            return;
        }

        if (!MethodClass.isNetworkConnected(this)) {
            Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), getResources().getString(R.string.please_check_your_internet_connection), Snackbar.LENGTH_SHORT);
            snackbar.show();
            return;
        }

        String utoken = String.valueOf(SharedPrefManager.getInstance(ChangePasswordActivity.this).getUserToken(ChangePasswordActivity.this,KEY_TOKEN));
        MethodClass.showProgressDialog(ChangePasswordActivity.this);

        String server_url = Constants.BASE_URL + "change-password";

        HashMap<String, String> params = new HashMap<String, String>();
        params.put("old_password", get_old_pswrd);
        params.put("new_password", get_new_pswrd);
        params.put("password_confirmation", get_confm_pswrd);

        JSONObject jsonObject = MethodClass.Json_rpc_format(params);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                MethodClass.hideProgressDialog(ChangePasswordActivity.this);
                Log.e("resp", response.toString());
                try {

                    JSONObject jsonObject = MethodClass.get_result_from_webservice(ChangePasswordActivity.this, response);

                    //if no error in response
                    if (jsonObject!= null) {
                        //Toast.makeText(getApplicationContext(), jsonObject.getString("message"), Toast.LENGTH_SHORT).show();

                        /*GsonBuilder gsonBuilder = new GsonBuilder();
                        Gson gson = gsonBuilder.create();
                        LoginMain loginMain = gson.fromJson(response.toString(), LoginMain.class);*/


                        String message = jsonObject.getString("message");

                        //storing the user password
                        SharedPrefManager.getInstance(getApplicationContext()).userPassword(get_confm_pswrd);

                        Toast.makeText(ChangePasswordActivity.this,message,Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(ChangePasswordActivity.this,AdminDashboardActivity.class);
                        startActivity(intent);

                    }

                } catch (JSONException e) {
                    MethodClass.error_alert(ChangePasswordActivity.this);
                    e.printStackTrace();
                    Log.e("change_parce", e.toString());
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                MethodClass.hideProgressDialog(ChangePasswordActivity.this);
                if (error.toString().contains("ConnectException")) {
                    MethodClass.network_error_alert(ChangePasswordActivity.this);
                } else {
                    MethodClass.error_alert(ChangePasswordActivity.this);
                }
            }

        }){
            //* Passing some request headers*
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", "Bearer " + utoken);

                Log.e("getHeaders: ", headers.toString());

                return headers;
            }
        };

        VolleySingleton.getInstance(ChangePasswordActivity.this).addToRequestQueue(jsonObjectRequest);


    }

    @Override
    protected void onResume() {
        super.onResume();
        MethodClass.setMenu(this);
    }

    @SuppressLint("WrongConstant")
    public void admin_home_menu(View view) {
        DrawerLayout drawer_layout = findViewById(R.id.drawer_layout);
        drawer_layout.openDrawer(Gravity.START);
    }


//    public void back(View view) {
//        onBackPressed();
//    }
}

