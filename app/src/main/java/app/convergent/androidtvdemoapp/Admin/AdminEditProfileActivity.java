package app.convergent.androidtvdemoapp.Admin;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import app.convergent.androidtvdemoapp.Helper.Constants;
import app.convergent.androidtvdemoapp.Helper.MethodClass;
import app.convergent.androidtvdemoapp.Helper.SharedPrefManager;
import app.convergent.androidtvdemoapp.Helper.VolleySingleton;
import app.convergent.androidtvdemoapp.Model_Class.AdminDashboard.MyProfile;
import app.convergent.androidtvdemoapp.Model_Class.LoginModel.Userdata;
import app.convergent.androidtvdemoapp.R;
import app.convergent.androidtvdemoapp.Users.activity.UserDashBoardActivity;
import app.convergent.androidtvdemoapp.Users.activity.UserEditProfileActivity;

import static app.convergent.androidtvdemoapp.Helper.SharedPrefManager.KEY_TOKEN;

/*import app.convergent.androidtvapp.Helper.MethodClass;
import app.convergent.androidtvapp.R;*/

public class AdminEditProfileActivity extends AppCompatActivity {

    public static Activity activity;
    private EditText adm_name, adm_email, adm_phone;
    private Button submit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);

        adm_name = findViewById(R.id.adm_name);
        adm_email = findViewById(R.id.adm_email);
        adm_phone = findViewById(R.id.adm_phone);

        TextView title = findViewById(R.id.admin_dash_title);
        title.setText(getString(R.string.edit_user));
    }

    public void edtProfile(View view) {

        String get_admin_name = adm_name.getText().toString().trim();
        String get_admin_ph = adm_phone.getText().toString().trim();

        if (get_admin_name.length() == 0) {
            adm_name.setError(getString(R.string.entername));
            adm_name.requestFocus();
            return;
        }

        if (get_admin_ph.length() == 0) {
            adm_phone.setError(getString(R.string.enterph));
            adm_phone.requestFocus();
            return;
        }

        String utoken = String.valueOf(SharedPrefManager.getInstance(AdminEditProfileActivity.this).getUserToken(AdminEditProfileActivity.this,KEY_TOKEN));

        if(MethodClass.isNetworkConnected(this)) {
            MethodClass.showProgressDialog(AdminEditProfileActivity.this);

            String server_url = Constants.BASE_URL + "edit-profile";

            HashMap<String, String> params = new HashMap<String, String>();
            params.put("name", get_admin_name);
            params.put("ph", get_admin_ph);

            JSONObject jsonObject = MethodClass.Json_rpc_format(params);

            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    MethodClass.hideProgressDialog(AdminEditProfileActivity.this);
                    Log.e("resp", response.toString());
                    try {

                        JSONObject jsonObject = MethodClass.get_result_from_webservice(AdminEditProfileActivity.this, response);

                        //if no error in response
                        if (jsonObject!= null) {
                            //Toast.makeText(getApplicationContext(), jsonObject.getString("message"), Toast.LENGTH_SHORT).show();

                        /*GsonBuilder gsonBuilder = new GsonBuilder();
                        Gson gson = gsonBuilder.create();
                        LoginMain loginMain = gson.fromJson(response.toString(), LoginMain.class);*/

                            //getting user-profile from the response
                            JSONObject profileJson = jsonObject.getJSONObject("myprofile");

                            //creating a new user object
                            MyProfile myProfile = new MyProfile(
                                    profileJson.getInt("id"),
                                    profileJson.getString("name"),
                                    profileJson.getString("email"),
                                    profileJson.getString("user_type"),
                                    profileJson.getString("ph"),
                                    profileJson.getString("status")
                            );

                            //creating a new user object
                            Userdata user = new Userdata(
                                    profileJson.getInt("id"),
                                    profileJson.getString("name"),
                                    profileJson.getString("email"),
                                    profileJson.getString("user_type")
                            );

                            //updating the user in shared preferences
                            SharedPrefManager.getInstance(getApplicationContext()).userLogin(user);

                            String admin_name = myProfile.getName();
                            adm_name.setText(admin_name);
                            String admin_mail = myProfile.getEmail();
                            adm_email.setText(admin_mail);
                            String admin_ph = myProfile.getPh();
                            adm_phone.setText(admin_ph);

                            Intent I = new Intent(AdminEditProfileActivity.this, AdminDashboardActivity.class);
                            startActivity(I);


                        }

                    } catch (JSONException e) {
                        MethodClass.error_alert(AdminEditProfileActivity.this);
                        e.printStackTrace();
                        Log.e("change_parce", e.toString());
                    }


                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    MethodClass.hideProgressDialog(AdminEditProfileActivity.this);
                    if (error.toString().contains("ConnectException")) {
                        MethodClass.network_error_alert(AdminEditProfileActivity.this);
                    } else {
                        MethodClass.error_alert(AdminEditProfileActivity.this);
                    }
                }

            }){
                //* Passing some request headers*
                @Override
                public Map getHeaders() throws AuthFailureError {
                    HashMap headers = new HashMap();
                    headers.put("Content-Type", "application/json");
                    headers.put("Authorization", "Bearer " + utoken);

                    Log.e("getHeaders: ", headers.toString());

                    return headers;
                }
            };

            VolleySingleton.getInstance(AdminEditProfileActivity.this).addToRequestQueue(jsonObjectRequest);

        } else {

            Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), getResources().getString(R.string.no_internet_connection), Snackbar.LENGTH_SHORT);
            snackbar.show();
            return;

        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        MethodClass.setMenu(this);

        show_admin_profile();
    }

    @SuppressLint("WrongConstant")
    public void admin_home_menu(View view) {
        DrawerLayout drawer_layout = findViewById(R.id.drawer_layout);
        drawer_layout.openDrawer(Gravity.START);
    }

    public void show_admin_profile () {

        String utoken = String.valueOf(SharedPrefManager.getInstance(AdminEditProfileActivity.this).getUserToken(AdminEditProfileActivity.this,KEY_TOKEN));

        if (!MethodClass.isNetworkConnected(this)) {
            Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), getResources().getString(R.string.please_check_your_internet_connection), Snackbar.LENGTH_SHORT);
            snackbar.show();
            return;
        }

        MethodClass.showProgressDialog(AdminEditProfileActivity.this);

        String server_url = Constants.BASE_URL + "my-profile";

        HashMap<String, String> params = new HashMap<String, String>();

        JSONObject jsonObject = MethodClass.Json_rpc_format(params);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                MethodClass.hideProgressDialog(AdminEditProfileActivity.this);
                Log.e("resp", response.toString());
                try {

                    JSONObject jsonObject = MethodClass.get_result_from_webservice(AdminEditProfileActivity.this, response);

                    //if no error in response
                    if (jsonObject!= null) {
                        //Toast.makeText(getApplicationContext(), jsonObject.getString("message"), Toast.LENGTH_SHORT).show();

                        /*GsonBuilder gsonBuilder = new GsonBuilder();
                        Gson gson = gsonBuilder.create();
                        LoginMain loginMain = gson.fromJson(response.toString(), LoginMain.class);*/

                        //getting user-profile from the response
                        JSONObject profileJson = jsonObject.getJSONObject("myprofile");

                        //creating a new user object
                        MyProfile myProfile = new MyProfile(
                                profileJson.getInt("id"),
                                profileJson.getString("name"),
                                profileJson.getString("email"),
                                profileJson.getString("user_type"),
                                profileJson.getString("ph"),
                                profileJson.getString("status")
                        );

                        String admin_name = myProfile.getName();
                        adm_name.setText(admin_name);
                        String admin_mail = myProfile.getEmail();
                        adm_email.setText(admin_mail);
                        String admin_ph = myProfile.getPh();
                        adm_phone.setText(admin_ph);


                    }

                } catch (JSONException e) {
                    MethodClass.error_alert(AdminEditProfileActivity.this);
                    e.printStackTrace();
                    Log.e("change_parce", e.toString());
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                MethodClass.hideProgressDialog(AdminEditProfileActivity.this);
                if (error.toString().contains("ConnectException")) {
                    MethodClass.network_error_alert(AdminEditProfileActivity.this);
                } else {
                    MethodClass.error_alert(AdminEditProfileActivity.this);
                }
            }

        }){
            //* Passing some request headers*
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", "Bearer " + utoken);

                Log.e("getHeaders: ", headers.toString());

                return headers;
            }
        };

        VolleySingleton.getInstance(AdminEditProfileActivity.this).addToRequestQueue(jsonObjectRequest);

    }


}

