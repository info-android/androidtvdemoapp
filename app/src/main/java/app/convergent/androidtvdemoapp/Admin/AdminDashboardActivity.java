package app.convergent.androidtvdemoapp.Admin;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import app.convergent.androidtvdemoapp.Helper.Constants;
import app.convergent.androidtvdemoapp.Helper.MethodClass;
import app.convergent.androidtvdemoapp.Helper.SharedPrefManager;
import app.convergent.androidtvdemoapp.Helper.VolleySingleton;
import app.convergent.androidtvdemoapp.Model_Class.AdminDashboard.MyProfile;
import app.convergent.androidtvdemoapp.Model_Class.LoginModel.Userdata;
import app.convergent.androidtvdemoapp.R;

import static app.convergent.androidtvdemoapp.Helper.SharedPrefManager.KEY_TOKEN;

/*
import app.convergent.androidtvapp.Helper.MethodClass;
import app.convergent.androidtvapp.R;
*/

public class AdminDashboardActivity extends AppCompatActivity {

    private ImageView add_user, view_user;
    private TextView admin_name_tv, admin_mail_tv, admin_ph_tv, admin_total_dev_tv, admin_total_user_tv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        TextView title = findViewById(R.id.admin_dash_title);
        title.setText(getString(R.string.dashboard));

        add_user = findViewById(R.id.addUser);
        view_user = findViewById(R.id.viewUser);
        admin_name_tv = findViewById(R.id.admin_tv);
        admin_mail_tv = findViewById(R.id.admin_mail_tv);
        admin_ph_tv = findViewById(R.id.admin_ph_tv);
        admin_total_dev_tv = findViewById(R.id.admin_total_dev_tv);
        admin_total_user_tv = findViewById(R.id.admin_total_user_tv);

        add_user.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(AdminDashboardActivity.this, UserAddActivity.class);
                //intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);

            }
        });

        view_user.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(AdminDashboardActivity.this, UserMgmtActivity.class);
                //intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);

            }
        });


    }

    @Override
    protected void onResume() {
        super.onResume();
        MethodClass.setMenu(this);

        admin_dashboard();
    }

    public void back(View view) {
        onBackPressed();
    }

    @SuppressLint("WrongConstant")
    public void admin_home_menu(View view) {
        DrawerLayout drawer_layout = findViewById(R.id.drawer_layout);
        drawer_layout.openDrawer(Gravity.START);
    }

    public void admin_dashboard() {

        String utoken = String.valueOf(SharedPrefManager.getInstance(AdminDashboardActivity.this).getUserToken(AdminDashboardActivity.this,KEY_TOKEN));

        if (!MethodClass.isNetworkConnected(this)) {
            Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), getResources().getString(R.string.please_check_your_internet_connection), Snackbar.LENGTH_SHORT);
            snackbar.show();
            return;
        }

        MethodClass.showProgressDialog(AdminDashboardActivity.this);
        String server_url = Constants.BASE_URL + "my-profile";

        HashMap<String, String> params = new HashMap<String, String>();

        JSONObject jsonObject = MethodClass.Json_rpc_format(params);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                MethodClass.hideProgressDialog(AdminDashboardActivity.this);
                Log.e("resp", response.toString());
                try {

                    JSONObject jsonObject = MethodClass.get_result_from_webservice(AdminDashboardActivity.this, response);

                    //if no error in response
                    if (jsonObject!= null) {
                        //Toast.makeText(getApplicationContext(), jsonObject.getString("message"), Toast.LENGTH_SHORT).show();

                        /*GsonBuilder gsonBuilder = new GsonBuilder();
                        Gson gson = gsonBuilder.create();
                        LoginMain loginMain = gson.fromJson(response.toString(), LoginMain.class);*/

                        //getting user-profile from the response
                        JSONObject profileJson = jsonObject.getJSONObject("myprofile");

                        //creating a new user object
                        MyProfile myProfile = new MyProfile(
                                profileJson.getInt("id"),
                                profileJson.getString("name"),
                                profileJson.getString("email"),
                                profileJson.getString("user_type"),
                                profileJson.getString("ph"),
                                profileJson.getString("status")
                        );

                        String admin_name = myProfile.getName();
                        admin_name_tv.setText(admin_name);
                        String admin_mail = myProfile.getEmail();
                        admin_mail_tv.setText(admin_mail);
                        String admin_ph = myProfile.getPh();
                        admin_ph_tv.setText(admin_ph);

                        String total_user = jsonObject.getString("totalUser");
                        admin_total_user_tv.setText(total_user);
                        String total_device = jsonObject.getString("total_device");
                        admin_total_dev_tv.setText(total_device);

                    }

                } catch (JSONException e) {
                    MethodClass.error_alert(AdminDashboardActivity.this);
                    e.printStackTrace();
                    Log.e("change_parce", e.toString());
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                MethodClass.hideProgressDialog(AdminDashboardActivity.this);
                if (error.toString().contains("ConnectException")) {
                    MethodClass.network_error_alert(AdminDashboardActivity.this);
                } else {
                    MethodClass.error_alert(AdminDashboardActivity.this);
                }
            }

        }){
            //* Passing some request headers*
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", "Bearer " + utoken);

                Log.e("getHeaders: ", headers.toString());

                return headers;
            }
        };

        VolleySingleton.getInstance(AdminDashboardActivity.this).addToRequestQueue(jsonObjectRequest);

    }
}
