package app.convergent.androidtvdemoapp.retrofit.interfaces;

import com.google.gson.JsonObject;

import java.util.HashMap;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Url;

public interface ApiInterface {

    @GET()
    Call<String> get(@Url String url, @Header("tag") String tag, @Header("Authorization") String header);

    @Headers({"Content-Type: application/json"})
    @POST()
    Call<String> getPostByJsonBody(@Url String url, @Body HashMap<String, Object> params, @Header("tag") String tag, @Header("Authorization") String header);


    @Headers({"Content-Type: application/json"})
    @POST()
    Call<String> getPostByJsonBody(@Url String url, @Body JsonObject params, @Header("tag") String tag, @Header("Authorization") String header);

    // whithout header jsonbody

    @Headers({"Content-Type: application/json"})
    @POST()
    Call<String> getPostByJsonBody(@Url String url,  @Body HashMap<String, Object> params, @Header("tag") String tag);


    @FormUrlEncoded
    @POST()
    Call<String> getPostByFromData(@Url String url, @FieldMap HashMap<String, String> params, @Header("tag") String tag, @Header("Authorization") String header);



    @Multipart
    @POST()
    Call<String> UploadWithJsonBody(@Url String url, @PartMap HashMap<String, RequestBody> param, @Part MultipartBody.Part file, @Header("tag") String tag,@Header("Authorization") String header);


}
