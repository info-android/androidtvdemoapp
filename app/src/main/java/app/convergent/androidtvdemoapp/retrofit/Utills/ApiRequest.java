package app.convergent.androidtvdemoapp.retrofit.Utills;


import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.util.Log;

import androidx.annotation.NonNull;


import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import app.convergent.androidtvdemoapp.Helper.MethodClass;
import app.convergent.androidtvdemoapp.Helper.ToastUtils;
import app.convergent.androidtvdemoapp.R;
import app.convergent.androidtvdemoapp.retrofit.NetWorkChecker;
import app.convergent.androidtvdemoapp.retrofit.interfaces.ApiInterface;
import app.convergent.androidtvdemoapp.retrofit.interfaces.OnCallBackListner;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ApiRequest {
    private Activity context;
    private OnCallBackListner listner;
   // private ProgressDialog progressDialog;
    private Dialog progressDialog;

    public ApiRequest(Activity context, OnCallBackListner listner) {
        this.context = context;
        this.listner = listner;

    }

    private void loader() {
        try {
            if (!context.isFinishing()) {
                if (progressDialog != null) {
                    progressDialog.dismiss();
                    progressDialog = null;
                    Log.e("loader", "showProgressDialog: ");
                }
                progressDialog = new Dialog(context);
                progressDialog.setCancelable(false);
                progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                progressDialog.setContentView(R.layout.custom_progress_dialog);
            }

        } catch (Exception e) {
            e.printStackTrace();
            Log.e("error_loader", e.getMessage());
        }


    }




    public void callGetRequest(String url, String tag,String header) {
        if (NetWorkChecker.check(context)) {
            loader();
            if (progressDialog != null) {
                progressDialog.show();
            }

            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<String> callMethod = apiInterface.get(url, tag, header);
            callMethod.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {

                    onCallBackSuccess(call, response);
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    onCallBackFaild(call, t);
                }
            });
        }

    }


    // JSON BODY
    public void callPOSTwithJsonBody(String url, HashMap<String, Object> params, String tag,String header) {
        if (NetWorkChecker.check(context)) {
            loader();
            if (progressDialog != null) {
                progressDialog.show();
            }
            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<String> callMethod = apiInterface.getPostByJsonBody(url, params, tag, header);
            callMethod.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {

                    onCallBackSuccess(call, response);

                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    onCallBackFaild(call, t);
                }
            });
        }

    }

    public void callPOSTwithJsonBody(String url, JsonObject params, String tag, String header) {
        if (NetWorkChecker.check(context)) {

            loader();
            if (progressDialog != null) {
                progressDialog.show();
            }
            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<String> callMethod = apiInterface.getPostByJsonBody(url, params, tag, header);
            callMethod.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {

                    onCallBackSuccess(call, response);

                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    onCallBackFaild(call, t);
                }
            });
        }

    }


    public void callPOSTwithJsonBody(String url,  HashMap<String, Object> params, String tag) {
        if (NetWorkChecker.check(context)) {

            loader();
            if (progressDialog != null) {
                progressDialog.show();
            }

            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<String> callMethod = apiInterface.getPostByJsonBody(url, params, tag);
            callMethod.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {

                    onCallBackSuccess(call, response);

                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    onCallBackFaild(call, t);
                }
            });
        }

    }



    public void callPOSTwithFromData(String url, HashMap<String, String> params, String tag,String header) {
        if (NetWorkChecker.check(context)) {
            loader();
            if (progressDialog != null) {
                progressDialog.show();
            }
            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<String> callMethod = apiInterface.getPostByFromData(url, params, tag, header);
            callMethod.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {

                    onCallBackSuccess(call, response);

                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    onCallBackFaild(call, t);
                }
            });
        }

    }


    public void callFileUploadwithJsonBody(String url, @NonNull HashMap<String, String> param, @NonNull PART part, final String tag,final String header) {

        if (NetWorkChecker.check(context)) {
            loader();
            if (progressDialog != null) {
                progressDialog.show();
            }

            ApiInterface service = ApiClient.getClient().create(ApiInterface.class);

            Call<String> stringCall = service.UploadWithJsonBody(url, getParam(param), Params.createMultiPart(part), tag,header);
            stringCall.enqueue(new Callback<String>() {
                @Override
                public void onResponse(@NonNull Call<String> call, @NonNull Response<String> response) {

                    onCallBackSuccess(call, response);
                }

                @Override
                public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {

                    onCallBackFaild(call, t);

                }
            });
        }

    }


    public void onCallBackSuccess(Call<String> call, Response<String> response) {

        if (response.isSuccessful()) {

            if (progressDialog != null) {
                progressDialog.dismiss();
            }

            Log.e("response_body" + call.request().header("tag"), response.body());
            try {
                JSONObject jsonObject = MethodClass.get_result_from_webservice((Activity) context, new JSONObject(response.body()));
                if (jsonObject != null) {
                    listner.OnCallBackSuccess(call.request().header("tag"), new JSONObject(response.body()));
                }
            } catch (Exception e) {
                listner.OnCallBackError(call.request().header("tag"), e.getMessage(), -1);
                e.printStackTrace();
            }
        }

        else {
            if (progressDialog != null) {
                progressDialog.dismiss();
            }
            try {
                JSONObject object = new JSONObject(response.errorBody().toString());

                if (object.has("error"))
                    ToastUtils.showLong((Activity) context, object.getString("error"), true);


            } catch (JSONException e) {
                e.printStackTrace();
                ToastUtils.showLong((Activity) context, context.getString(R.string.Somethingwrong), true);

            }


        }


    }


    public void onCallBackFaild(Call<String> call, Throwable t) {


        try {
            if (progressDialog != null) {
                progressDialog.dismiss();
            }
            if (listner != null) {
                listner.OnCallBackError(call.request().header("tag"), t.getMessage(), -1);
            }

            ToastUtils.showLong((Activity) context, context.getString(R.string.Somethingwrong), true);
            Log.e("response_body" + call.request().header("tag"), call.toString());

        } catch (Exception e) {

            listner.OnCallBackError(call.request().header("tag"), e.getMessage(), -1);

            ToastUtils.showLong((Activity) context, context.getString(R.string.Somethingwrong), true);

        }


    }


    private HashMap<String, RequestBody> getParam(HashMap<String, String> param) {
        HashMap<String, RequestBody> tempParam = new HashMap<>();
        for (String key : param.keySet()) {
            tempParam.put(key, toRequestBody(param.get(key)));
        }

        return tempParam;
    }


    private static RequestBody toRequestBody(String value) {
        return RequestBody.create(
                okhttp3.MultipartBody.FORM, value);
    }
}

