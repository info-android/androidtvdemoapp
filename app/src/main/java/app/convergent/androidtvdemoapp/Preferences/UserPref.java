package app.convergent.androidtvdemoapp.Preferences;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;

import app.convergent.androidtvdemoapp.Helper.Constants;


public class UserPref {

    @SuppressLint("StaticFieldLeak")
    private static Context context;
    static SharedPreferences sharedPreferences;
    static SharedPreferences.Editor editor;

    public UserPref(Context context) {
        UserPref.context = context;
        sharedPreferences = context.getSharedPreferences(Constants.MY_PREFS_NAME, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        editor.apply();

    }


    public static void setLogged(boolean is) {
        editor.putBoolean("USER_LOGGED", is);
        editor.apply();
    }


    public static boolean isLogged() {
        return sharedPreferences.getBoolean("USER_LOGGED", false);
    }


}
