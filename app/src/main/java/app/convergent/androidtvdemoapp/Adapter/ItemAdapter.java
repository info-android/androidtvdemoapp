package app.convergent.androidtvdemoapp.Adapter;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.snackbar.Snackbar;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/*import app.convergent.androidtvapp.Admin.UserEditActivity;
import app.convergent.androidtvapp.R;*/
import app.convergent.androidtvdemoapp.Admin.UserEditActivity;
import app.convergent.androidtvdemoapp.Admin.UserMgmtActivity;
import app.convergent.androidtvdemoapp.Helper.Constants;
import app.convergent.androidtvdemoapp.Helper.MethodClass;
import app.convergent.androidtvdemoapp.Helper.SharedPrefManager;
import app.convergent.androidtvdemoapp.Helper.VolleySingleton;
import app.convergent.androidtvdemoapp.Model_Class.UserList.UserDetailsList;
import app.convergent.androidtvdemoapp.R;
import app.convergent.androidtvdemoapp.Users.Model.DeviceList.DeviceDetailsList;
import de.hdodenhof.circleimageview.CircleImageView;

import static app.convergent.androidtvdemoapp.Helper.SharedPrefManager.KEY_MSG;
import static app.convergent.androidtvdemoapp.Helper.SharedPrefManager.KEY_TOKEN;

public class ItemAdapter extends RecyclerView.Adapter<ItemAdapter.ViewHolder>{
    Activity activity;
    public ArrayList<HashMap<String, String>> map_list;
    //Creating an arraylist of UserDetailsList
    public ArrayList<UserDetailsList> userDetailsLists;
    public String msg, umsg, server_blck_url, server_actv_url, utoken;
    public String user_status = "";

    public ItemAdapter(Activity activity, ArrayList<UserDetailsList> userDetailsLists) {
        this.activity = activity;
        //this.map_list = map_list;
        this.userDetailsLists = userDetailsLists;
    }

    public void update(ArrayList<UserDetailsList> userDetailsLists) {
        this.userDetailsLists = userDetailsLists;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.user_home_list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        //final HashMap<String, String> map = map_list.get(position);
        final UserDetailsList list_items = userDetailsLists.get(position);

        holder.uname.setText(list_items.getName());
        holder.umail.setText(list_items.getEmail());
        holder.uph.setText(list_items.getPh());

        int user_id = list_items.getId();
        String user_name = list_items.getName();
        String user_phone = list_items.getPh();
        String user_email = list_items.getEmail();
        user_status = list_items.getStatus();
        Log.e("user_status:",user_status);

        utoken = String.valueOf(SharedPrefManager.getInstance(activity).getUserToken(activity,KEY_TOKEN));
        //msg = String.valueOf(SharedPrefManager.getInstance(activity).getUserToken(activity,KEY_MSG));

        server_blck_url = Constants.BASE_URL + "user-block";
        server_actv_url = Constants.BASE_URL + "user-active";

        holder.edtUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent intent = new Intent(activity, UserEditActivity.class);
                intent.putExtra("from","user_mngnt");
                intent.putExtra("user_id",user_id);
                intent.putExtra("user_name",user_name);
                intent.putExtra("user_phone",user_phone);
                intent.putExtra("user_email",user_email);
                activity.startActivity(intent);

            }
        });

        if(user_status.equals("B")) {
            holder.blckUser.setColorFilter(ContextCompat.getColor(activity, R.color.red_clr));

        }


        holder.blckUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String status_user = list_items.getStatus();

                SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(activity, SweetAlertDialog.WARNING_TYPE);
                if (sweetAlertDialog.isShowing()) {
                    sweetAlertDialog.dismissWithAnimation();
                }

                sweetAlertDialog.setTitleText(status_user.equals("A") ? "Block User" : "Unblock User");
                sweetAlertDialog.setContentText("Are you sure you want to " + (status_user.equals("A") ? "Block" : "Unblock") + " this user?");
                sweetAlertDialog.setConfirmText("Ok");
                sweetAlertDialog.setCancelText("No");
                sweetAlertDialog.setCancelClickListener(null);

                sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();

                        if(MethodClass.isNetworkConnected(activity)) {
                            MethodClass.showProgressDialog(activity);
                            //String status_user = list_items.getStatus();
                            if(status_user.equals("A")) {

                                StringRequest stringRequest = new StringRequest(Request.Method.POST, server_blck_url, new Response.Listener<String>() {
                                    @Override
                                    public void onResponse(String response) {
                                        try {
                                            Log.d("Response:", response);
                                            JSONObject object = new JSONObject(response);

                                            if (object.has("result")) {

                                                JSONObject successResponse = object.getJSONObject("result");

                                                if (successResponse.has("message")) {


                                                    MethodClass.hideProgressDialog(activity);
                                                    umsg = successResponse.getString("message");
                                                    ( (UserMgmtActivity)activity).list();


                                                    Toast.makeText(activity, umsg, Toast.LENGTH_SHORT).show();
                                                }

                                            }

                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                },
                                        new com.android.volley.Response.ErrorListener() {
                                            @Override
                                            public void onErrorResponse(VolleyError error) {

                                                Toast.makeText(activity, "Connection Error!", Toast.LENGTH_SHORT).show();
                                                Log.d("Connection Error!", String.valueOf(error));
                                            }
                                        }) {
                                    @Override
                                    protected Map<String, String> getParams() {
                                        Map<String, String> params = new HashMap<>();

                                        params.put("id", String.valueOf(user_id));

                                        Log.e("param:", String.valueOf(params));
                                        return params;


                                    }

                                    @Override
                                    public Map<String, String> getHeaders() throws AuthFailureError {
                                        Map<String, String> headers = new HashMap<>();
                                        headers.put("Authorization", "bearer" + utoken);
                                        return headers;
                                    }

                                };

                                // Adding the string request to the queue
                                RequestQueue requestQueue = Volley.newRequestQueue(activity);
                                requestQueue.add(stringRequest);

                            } else if(status_user.equals("B")) {

                                StringRequest stringRequest = new StringRequest(Request.Method.POST, server_actv_url, new Response.Listener<String>() {
                                    @Override
                                    public void onResponse(String response) {
                                        try {

                                            Log.d("Response:", response);

                                            JSONObject object = new JSONObject(response);

                                            if (object.has("result")) {

                                                JSONObject successResponse = object.getJSONObject("result");

                                                if (successResponse.has("message")) {
                                                    umsg = successResponse.getString("message");

                                                    ((UserMgmtActivity)activity).list();

                                                    holder.blckUser.setColorFilter(ContextCompat.getColor(activity, R.color.transparent));
                                                    Toast.makeText(activity, umsg, Toast.LENGTH_SHORT).show();
                                                }

                                            }

                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                },
                                        new com.android.volley.Response.ErrorListener() {
                                            @Override
                                            public void onErrorResponse(VolleyError error) {

                                                Toast.makeText(activity, "Connection Error!", Toast.LENGTH_SHORT).show();
                                                Log.d("Connection Error!", String.valueOf(error));
                                            }
                                        }) {
                                    @Override
                                    protected Map<String, String> getParams() {
                                        Map<String, String> params = new HashMap<>();

                                        params.put("id", String.valueOf(user_id));
                                        Log.e("param:", String.valueOf(params));
                                        return params;


                                    }

                                    @Override
                                    public Map<String, String> getHeaders() throws AuthFailureError {
                                        Map<String, String> headers = new HashMap<>();
                                        headers.put("Authorization", "bearer" + utoken);
                                        return headers;
                                    }

                                };

                                RequestQueue requestQueue = Volley.newRequestQueue(activity);
                                requestQueue.add(stringRequest);

                            }
                        } else {

                            Snackbar snackbar = Snackbar.make(v.findViewById(android.R.id.content), v.getResources().getString(R.string.no_internet_connection), Snackbar.LENGTH_SHORT);
                            snackbar.show();
                            return;

                        }

                    }
                });
                sweetAlertDialog.setCancelable(true);
                sweetAlertDialog.setCanceledOnTouchOutside(true);
                sweetAlertDialog.show();

            }
        });

        holder.delUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(activity, SweetAlertDialog.WARNING_TYPE);
                if (sweetAlertDialog.isShowing()) {
                    sweetAlertDialog.dismissWithAnimation();
                }

                sweetAlertDialog.setTitleText("Delete User");
                sweetAlertDialog.setContentText("Are you sure you want to delete this user?");
                sweetAlertDialog.setConfirmText("Ok");
                sweetAlertDialog.setCancelText("No");
                sweetAlertDialog.setCancelClickListener(null);

                sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();
                        deleteUser(String.valueOf(user_id));
                    }
                });
                sweetAlertDialog.setCancelable(true);
                sweetAlertDialog.setCanceledOnTouchOutside(true);
                sweetAlertDialog.show();


            }
        });


    }

    @Override
    public int getItemCount() {
        //return map_list == null ? 0 : map_list.size();
        return  userDetailsLists == null ? 0 : userDetailsLists.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private CircleImageView edtUser, delUser, blckUser, actvUser;
        private TextView uname, umail, uph;

        public ViewHolder(@NonNull View itemView) {

            super(itemView);

            edtUser = itemView.findViewById(R.id.edtUser);
            delUser = itemView.findViewById(R.id.delUser);
            blckUser = itemView.findViewById(R.id.blckUser);
            actvUser = itemView.findViewById(R.id.actvUser);
            uname = itemView.findViewById(R.id.uname);
            umail = itemView.findViewById(R.id.umail);
            uph = itemView.findViewById(R.id.uph);
        }
    }

    public void deleteUser(String user_id) {

        if (!MethodClass.isNetworkConnected(activity)) {
            Snackbar snackbar = Snackbar.make(activity.findViewById(android.R.id.content), activity.getResources().getString(R.string.please_check_your_internet_connection), Snackbar.LENGTH_SHORT);
            snackbar.show();
            return;
        }

            String server_del_url = Constants.BASE_URL + "user-delete";

            StringRequest stringRequest = new StringRequest(Request.Method.POST, server_del_url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {
                        Log.d("Response:", response);
                        JSONObject object = new JSONObject(response);

                        if (object.has("result")) {

                            JSONObject successResponse = object.getJSONObject("result");

                            if (successResponse.has("message")) {


                                MethodClass.hideProgressDialog(activity);
                                umsg = successResponse.getString("message");
                                ( (UserMgmtActivity)activity).list();

                                Toast.makeText(activity, umsg, Toast.LENGTH_SHORT).show();
                            }

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            },
                    new com.android.volley.Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                            Toast.makeText(activity, "Connection Error!", Toast.LENGTH_SHORT).show();
                            Log.d("Connection Error!", String.valueOf(error));
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<>();

                    params.put("id", String.valueOf(user_id));

                    Log.e("param:", String.valueOf(params));
                    return params;


                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("Authorization", "bearer" + utoken);
                    return headers;
                }

            };

            // Adding the string request to the queue
            RequestQueue requestQueue = Volley.newRequestQueue(activity);
            requestQueue.add(stringRequest);



    }

}
