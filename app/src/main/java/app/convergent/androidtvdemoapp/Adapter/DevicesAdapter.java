package app.convergent.androidtvdemoapp.Adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import app.convergent.androidtvdemoapp.R;
import app.convergent.androidtvdemoapp.Users.Model.DeviceList.DeviceDetailsList;

import static app.convergent.androidtvdemoapp.Users.activity.AssignDevices.AssignDevicesActivity.selectedDevicesArr;

/*import app.convergent.androidtvapp.R;

import static app.convergent.androidtvapp.Users.AssignDevices.AssignDevicesActivity.selectedDevicesArr;*/

public class DevicesAdapter extends RecyclerView.Adapter<DevicesAdapter.ViewHolder> {
    Activity activity;
    public ArrayList<DeviceDetailsList> map_list;

    public DevicesAdapter(Activity activity, ArrayList<DeviceDetailsList> map_list) {
        this.activity = activity;
        this.map_list = map_list;
        selectedDevicesArr = new ArrayList<>();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.devices_list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {

        int pos = holder.getAdapterPosition();
        holder.deviceName_chkbx.setText(map_list.get(pos).getDevice_name() + " ( " + map_list.get(pos).getDevice_id() + " )");

        if (selectedDevicesArr.size()> 0)
            holder.deviceName_chkbx.setChecked(map_list.get(pos).isChecked());


        holder.deviceName_chkbx.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    selectedDevicesArr.add(map_list.get(pos));
                    map_list.get(pos).setChecked(isChecked);
                }
                else {
                    selectedDevicesArr.remove(map_list.get(pos));
                    map_list.get(pos).setChecked(isChecked);
                }

            }
        });

       /* holder.deviceName_chkbx.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                    if (holder.deviceName_chkbx.isChecked()) {

                        selectedDevicesArr.add(map_list.get(pos));

                    }

                    if (!holder.deviceName_chkbx.isChecked()) {

                        int removeId = Integer.parseInt(map_list.get(pos));
                        for (int ii = 0; ii < selectedDevicesArr.size(); ii++) {
                            if (removeId == Integer.parseInt(selectedDevicesArr.get(ii))) {

                                selectedDevicesArr.remove(ii);
                            }
                        }

                    } else {
                    holder.deviceName_chkbx.setChecked(false);
                }

            }
        });*/


    }

    @Override
    public int getItemCount() {
        return map_list == null ? 0 : map_list.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        CheckBox deviceName_chkbx;

        public ViewHolder(@NonNull View itemView) {

            super(itemView);

            deviceName_chkbx = itemView.findViewById(R.id.deviceName_chkbx);

        }
    }
}
