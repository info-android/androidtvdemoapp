package app.convergent.androidtvdemoapp.Adapter;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

/*import app.convergent.androidtvapp.R;
import app.convergent.androidtvapp.Users.URL_Management.Add_Edit_URL;*/
import app.convergent.androidtvdemoapp.R;
import app.convergent.androidtvdemoapp.Users.activity.URL_Management.Add_Edit_URL;
import de.hdodenhof.circleimageview.CircleImageView;

public class AssignAdapter extends RecyclerView.Adapter<AssignAdapter.ViewHolder> {
    Activity activity;
    public ArrayList<String> map_list;

    public AssignAdapter(Activity activity, ArrayList<String> map_list) {
        this.activity = activity;
        this.map_list = map_list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.url_list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        /*final HashMap<String, String> map = map_list.get(position);*/

        //holder.setOrder_tv.setText(position + "");

        holder.urlDel.setVisibility(View.GONE);
        holder.edtURL_ib.setVisibility(View.GONE);
        holder.edtURL_ib.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(activity, Add_Edit_URL.class);
                intent.putExtra("url_header", "EDIT URL");
                intent.putExtra("url_save", "SAVE URL");
                activity.startActivity(intent);

            }
        });
    }

    @Override
    public int getItemCount() {
        return map_list == null ? 0 : 6;
    }  /*map_list.size()*/

    public class ViewHolder extends RecyclerView.ViewHolder {

        private CircleImageView edtURL_ib, urlDel;
        TextView setOrder_tv;

        public ViewHolder(@NonNull View itemView) {

            super(itemView);

            edtURL_ib = itemView.findViewById(R.id.img_edit);
            urlDel = itemView.findViewById(R.id.img_delete);
            //setOrder_tv = itemView.findViewById(R.id.setOrder_tv);
        }
    }
}
