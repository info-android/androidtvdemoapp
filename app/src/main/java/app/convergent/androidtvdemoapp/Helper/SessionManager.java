/*
package app.convergent.androidtvdemoapp.Helper;

import android.content.Context;
import android.content.SharedPreferences;

*/
/*import com.goluggcargo.goluggcargo.Model.LoginMainModel.Userdata;
import com.goluggcargo.goluggcargo.R;*//*

import com.google.gson.GsonBuilder;

import app.convergent.androidtvdemoapp.R;


public class SessionManager {

    private static SharedPreferences preferences;
    private static SharedPreferences.Editor editor;

    public SessionManager(Context context) {
        preferences = context.getSharedPreferences(context.getString(R.string.app_name), 0);
        editor = preferences.edit();
    }

    public static void clearSessionManager() {
        editor.clear().apply();
    }


    public static String getdata(String id){
        return preferences.getString(id,"");
    }


    public static void setdata(String id,String value){
        editor.putString(id,value);
        editor.apply();

    }


    public static void createuser(String id,
                                  String name,
                                  String email) {

        editor.putString(Variable.KEY_ID, id);
        editor.putString(Variable.KEY_NAME, name);
        editor.putString(Variable.KEY_EMAIL, email);
        editor.apply();

    }


    public static void setLogged(boolean is) {
        editor.putBoolean("USER_LOGGED", is);
        editor.apply();
    }


    public static boolean isLogged() {
        return preferences.getBoolean("USER_LOGGED", false);
    }


    public static void setLanguage(boolean is) {
        editor.putBoolean("LANGUAGE", is);
        editor.commit();
    }


    public static boolean isLanguageSelectd() {
        return preferences.getBoolean("LANGUAGE", false);
    }


    public static void setWholeUserData(User data) {

        editor.putString("Userdata", new GsonBuilder().create().toJson(data).toString());

    }

    public static User getUserData() {

        return new GsonBuilder().create().fromJson(preferences.getString("Userdata", ""), User.class);
    }


}
*/
