package app.convergent.androidtvdemoapp.Helper;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import app.convergent.androidtvdemoapp.Activity.LoginActivity;
import app.convergent.androidtvdemoapp.Model_Class.LoginModel.Userdata;

public class SharedPrefManager {

    private static final String SHARED_PREF_NAME = "userlogin";
    public static final String KEY_USERNAME = "keyusername";
    private static final String KEY_EMAIL = "keyemail";
    private static final String KEY_USERTYPE = "keyusertype";
    private static final String KEY_ID = "keyid";
    private static final String KEY_PASSWORD = "keypassword";
    public static final String KEY_TOKEN = "keytoken";
    public static final String KEY_MSG = "keymsg";
    private static SharedPrefManager mInstance;
    private static Context ctx;

    private SharedPrefManager(Context context) {
        ctx = context;
    }
    public static synchronized SharedPrefManager getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new SharedPrefManager(context);
        }
        return mInstance;
    }

    //this method will store the user data in shared-preferences
    public void userLogin(Userdata userdata) {
        SharedPreferences sharedPreferences = ctx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(KEY_ID, userdata.getId());
        editor.putString(KEY_USERNAME, userdata.getName());
        editor.putString(KEY_EMAIL, userdata.getEmail());
        editor.putString(KEY_USERTYPE, userdata.getUserType());
        editor.apply();
    }

    //this method will store the user token
    public void userToken(String token) {

        SharedPreferences sharedPreferences = ctx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(KEY_TOKEN, token);
        editor.apply();
    }

    //this method will return user token
    public String getUserToken(Context ctx, String key) {
        SharedPreferences sharedPreferences = ctx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(KEY_TOKEN, null);
    }

    //this method will store the user password
    public void userPassword(String pwd) {

        SharedPreferences sharedPreferences = ctx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(KEY_PASSWORD, pwd);
        editor.apply();
    }

    //this method will return user password
    public String getUserPassword(Context ctx, String key) {
        SharedPreferences sharedPreferences = ctx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(KEY_PASSWORD, null);
    }

    //this method will store the user name
    public void userName(String uname) {

        SharedPreferences sharedPreferences = ctx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(KEY_USERNAME, uname);
        editor.apply();
    }

    //this method will return user name
    public String getUserName(Context ctx, String key) {
        SharedPreferences sharedPreferences = ctx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(KEY_USERNAME, null);
    }

    //this method will store the user message
    public void userMsg(String msg) {

        SharedPreferences sharedPreferences = ctx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(KEY_MSG, msg);
        editor.apply();
    }

    //this method will return user message
    public String getUserMsg(Context ctx, String key) {
        SharedPreferences sharedPreferences = ctx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(KEY_MSG, null);
    }


    //this method will checker whether user is already logged in or not
    public boolean isLoggedIn() {
        SharedPreferences sharedPreferences = ctx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(KEY_USERNAME, null) != null;
    }

    //this method will give the logged in user
    public Userdata getUser() {
        SharedPreferences sharedPreferences = ctx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return new Userdata(
                sharedPreferences.getInt(KEY_ID, -1),
                sharedPreferences.getString(KEY_USERNAME, null),
                sharedPreferences.getString(KEY_EMAIL, null),
                sharedPreferences.getString(KEY_USERTYPE, null)
        );
    }

    //this method will logout the user
    public void logout() {
        SharedPreferences sharedPreferences = ctx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.apply();
        //ctx.startActivity(new Intent(ctx, LoginActivity.class));
    }

}
