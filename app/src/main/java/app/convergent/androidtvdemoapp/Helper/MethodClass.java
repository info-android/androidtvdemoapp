package app.convergent.androidtvdemoapp.Helper;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.telephony.PhoneNumberUtils;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;

import com.google.android.material.navigation.NavigationView;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import app.convergent.androidtvdemoapp.Activity.LoginActivity;
import app.convergent.androidtvdemoapp.Admin.AdminDashboardActivity;
import app.convergent.androidtvdemoapp.Admin.AdminEditProfileActivity;
import app.convergent.androidtvdemoapp.Admin.UserMgmtActivity;
import app.convergent.androidtvdemoapp.Model_Class.LoginModel.Userdata;
import app.convergent.androidtvdemoapp.R;
import app.convergent.androidtvdemoapp.Users.activity.AssignDevices.AssignDevicesActivity;
import app.convergent.androidtvdemoapp.Users.activity.DeviceManagement.DeviceMgmtActivity;
import app.convergent.androidtvdemoapp.Users.activity.EditDeviceActivity;
import app.convergent.androidtvdemoapp.Users.activity.Set_Management.SetManagementActivity;
import app.convergent.androidtvdemoapp.Users.activity.UserChangePasswordActivity;
import app.convergent.androidtvdemoapp.Users.activity.UserDashBoardActivity;
import app.convergent.androidtvdemoapp.Users.activity.UserEditProfileActivity;
import de.hdodenhof.circleimageview.CircleImageView;

import static app.convergent.androidtvdemoapp.Helper.Constants.UNAME;
import static app.convergent.androidtvdemoapp.Helper.SharedPrefManager.KEY_TOKEN;
import static app.convergent.androidtvdemoapp.Helper.SharedPrefManager.KEY_USERNAME;

public class MethodClass {

    public static String BUNDLE_KEY = "BUNDLE_APP";
    static Dialog progressDialog;

    public static Dialog loader(Activity activity) {
        try {
            if (!activity.isFinishing()) {

                if (progressDialog != null) {
                    progressDialog.dismiss();
                    progressDialog = null;
                    Log.e("loader", "showProgressDialog: ");
                }
                progressDialog = new Dialog(activity);
                progressDialog.setCancelable(false);
                progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                progressDialog.setContentView(R.layout.custom_progress_dialog);


            }

        } catch (Exception e) {
            e.printStackTrace();
            Log.e("error_loader", e.getMessage());
        }

        return progressDialog;

    }

    public static <T> T defaultWhenNull(@Nullable T object, @NonNull T def) {
        if (object instanceof String) {
            if (((String) object).isEmpty()) {
                return def;
            }
        }
        return (object == null) ? def : object;
    }


    public static boolean isInteger(String s) {
        try {
            Integer.parseInt(s);
        } catch (NumberFormatException e) {
            return false;
        } catch (NullPointerException e) {
            return false;
        }
        // only got here if we didn't return false
        return true;
    }

    public static void setMenu(final Activity activity) {
        NavigationView navigationView = (NavigationView) activity.findViewById(R.id.nav_view);
        View headerView = navigationView.getHeaderView(0);
        final DrawerLayout drawer_layout = activity.findViewById(R.id.drawer_layout);
        /* if (PreferenceManager.getDefaultSharedPreferences(activity).getBoolean("is_logged_in", false)) {*/

        Userdata user = SharedPrefManager.getInstance(activity).getUser();
        String user_name = user.getName();
        String user_email = user.getEmail();

        LinearLayout userName_ll = headerView.findViewById(R.id.userName_ll);

        LinearLayout close_menu = headerView.findViewById(R.id.close_menu);

        CircleImageView image_nav = headerView.findViewById(R.id.image_nav);
        TextView name_nav = headerView.findViewById(R.id.name_nav);
        TextView email_nav = headerView.findViewById(R.id.email_nav);

        name_nav.setText(user_name);
        email_nav.setText(user_email);
            /*String user_image = USER_IMAGE_URL + PreferenceManager.getDefaultSharedPreferences(activity).getString("profile_pic", "");
            String user_name = PreferenceManager.getDefaultSharedPreferences(activity).getString("name", "");
            String user_email = PreferenceManager.getDefaultSharedPreferences(activity).getString("email", "");
            Picasso.get().load(user_image).placeholder(R.drawable.logo_2).error(R.drawable.logo_2).into(image_nav);
            name_nav.setText(user_name);
            if (!user_email.equals("") && !user_email.equals(null) && !user_email.equals("null")) {
                email_nav.setText(user_email);
            }
        } */

        close_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawer_layout.closeDrawers();
            }
        });

        userName_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!activity.getLocalClassName().equals("Admin.AdminDashboardActivity")) {
                    Intent intent = new Intent(activity, AdminDashboardActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    activity.startActivity(intent);
                }
            }
        });

        LinearLayout dashboard_nav_lin = headerView.findViewById(R.id.home_nav_lin);
        LinearLayout change_pwd_nav_lin = headerView.findViewById(R.id.change_pwd_nav_lin);
        LinearLayout edit_nav_lin = headerView.findViewById(R.id.edit_nav_lin);
        LinearLayout logout_login_nav_lin = headerView.findViewById(R.id.logout_login_nav_lin);


        dashboard_nav_lin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawer_layout.closeDrawers();
                if (!activity.getLocalClassName().equals("Admin.HomeActivity")) {
                    Intent intent = new Intent(activity, UserMgmtActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    activity.startActivity(intent);
                }

            }
        });

        change_pwd_nav_lin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawer_layout.closeDrawers();
                if (!activity.getLocalClassName().equals("Admin.ChangePasswordActivity")) {
                    Intent intent = new Intent(activity, app.convergent.androidtvdemoapp.Admin.ChangePasswordActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    activity.startActivity(intent);
                }

            }
        });

        edit_nav_lin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawer_layout.closeDrawers();
                if (!activity.getLocalClassName().equals("Admin.EditProfileActivity")) {
                    Intent intent = new Intent(activity, AdminEditProfileActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    activity.startActivity(intent);
                }

            }
        });

        logout_login_nav_lin.setOnClickListener(view -> {
            AlertDialog.Builder builder1 = new AlertDialog.Builder(activity);
            builder1.setMessage("Are you sure to logout?");
            builder1.setCancelable(true);

            builder1.setPositiveButton(
                    "Yes",
                    (dialog, id) -> {

                        /*SharedPreferences preferences = getSharedPreferences(Constants.MY_PREFS_NAME, MODE_PRIVATE);
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.clear();
                        editor.apply();*/

                        /*Intent intent = new Intent(activity, LoginActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        activity.startActivity(intent);*/

                        SharedPrefManager.getInstance(activity).logout();
                        Intent intent = new Intent(activity, LoginActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        activity.startActivity(intent);
                        activity.finishAffinity();

                        dialog.cancel();
                    });

            builder1.setNegativeButton(
                    "No",
                    (dialog, id) -> dialog.cancel());

            AlertDialog alert11 = builder1.create();
            alert11.show();


        });

    }


    public static void setUserMenu(final Activity activity) {
        NavigationView navigationView = (NavigationView) activity.findViewById(R.id.nav_view);
        View headerView = navigationView.getHeaderView(0);
        final DrawerLayout drawer_layout = activity.findViewById(R.id.user_drawer_layout);
        /* if (PreferenceManager.getDefaultSharedPreferences(activity).getBoolean("is_logged_in", false)) {*/

        Userdata user = SharedPrefManager.getInstance(activity).getUser();
        String user_name = user.getName();
        String user_email = user.getEmail();

        //UNAME = String.valueOf(SharedPrefManager.getInstance(activity).getUserName(activity,KEY_USERNAME));

        LinearLayout userName_ll = headerView.findViewById(R.id.userName_ll);

        LinearLayout user_close_menu = headerView.findViewById(R.id.user_close_menu);

        CircleImageView image_nav = headerView.findViewById(R.id.image_nav);
        TextView name_nav = headerView.findViewById(R.id.name_nav);
        TextView email_nav = headerView.findViewById(R.id.email_nav);

        //name_nav.setText(user_name);
        name_nav.setText(user_name);
        email_nav.setText(user_email);

        userName_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!activity.getLocalClassName().equals("Users.UserDashBoardActivity")) {
                    Intent intent = new Intent(activity, UserDashBoardActivity.class);
                    activity.startActivity(intent);
                }
            }
        });

            /*String user_image = USER_IMAGE_URL + PreferenceManager.getDefaultSharedPreferences(activity).getString("profile_pic", "");
            String user_name = PreferenceManager.getDefaultSharedPreferences(activity).getString("name", "");
            String user_email = PreferenceManager.getDefaultSharedPreferences(activity).getString("email", "");
            Picasso.get().load(user_image).placeholder(R.drawable.logo_2).error(R.drawable.logo_2).into(image_nav);
            name_nav.setText(user_name);
            if (!user_email.equals("") && !user_email.equals(null) && !user_email.equals("null")) {
                email_nav.setText(user_email);
            }
        } */

        LinearLayout user_change_pwd_nav_lin = headerView.findViewById(R.id.user_change_pwd_nav_lin);
        LinearLayout user_edit_nav_lin = headerView.findViewById(R.id.user_edit_nav_lin);
        LinearLayout user_device_mgmt_lin = headerView.findViewById(R.id.user_device_mgmt_lin);
        LinearLayout user_set_mgmt_lin = headerView.findViewById(R.id.user_set_mgmt_lin);
        LinearLayout user_assign_dev_mgmt_lin = headerView.findViewById(R.id.user_assign_dev_mgmt_lin);

        LinearLayout user_logout_login_nav_lin = headerView.findViewById(R.id.user_logout_login_nav_lin);


        user_close_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawer_layout.closeDrawers();
            }
        });

        user_assign_dev_mgmt_lin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawer_layout.closeDrawers();
                if (!activity.getLocalClassName().equals("Users.AssignDevices.AssignDevices")) {
                    Intent intent = new Intent(activity, AssignDevicesActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    activity.startActivity(intent);
                }

            }
        });

        user_set_mgmt_lin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawer_layout.closeDrawers();
                if (!activity.getLocalClassName().equals("Users.Set_Management.SetManagement")) {
                    Intent intent = new Intent(activity, SetManagementActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    activity.startActivity(intent);
                }

            }
        });

        user_device_mgmt_lin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawer_layout.closeDrawers();
                if (!activity.getLocalClassName().equals("Users.DeviceMgmtActivity")) {
                    Intent intent = new Intent(activity, DeviceMgmtActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    activity.startActivity(intent);
                }

            }
        });

        user_change_pwd_nav_lin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawer_layout.closeDrawers();
                if (!activity.getLocalClassName().equals("Users.UserChangePasswordActivity")) {
                    Intent intent = new Intent(activity, UserChangePasswordActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    activity.startActivity(intent);
                }

            }
        });

        user_edit_nav_lin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawer_layout.closeDrawers();
                if (!activity.getLocalClassName().equals("Users.UserEditProfileActivity")) {
                    Intent intent = new Intent(activity, UserEditProfileActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    activity.startActivity(intent);
                }

            }
        });

        user_logout_login_nav_lin.setOnClickListener(view -> {
            AlertDialog.Builder builder1 = new AlertDialog.Builder(activity);
            builder1.setMessage("Are you sure to logout?");
            builder1.setCancelable(true);

            builder1.setPositiveButton(
                    "Yes",
                    (dialog, id) -> {

                        /*SharedPreferences preferences = getSharedPreferences(Constants.MY_PREFS_NAME, MODE_PRIVATE);
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.clear();
                        editor.apply();*/

                        /*Intent intent = new Intent(activity, LoginActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        activity.startActivity(intent);*/

                        SharedPrefManager.getInstance(activity).logout();
                        Intent intent = new Intent(activity, LoginActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        activity.startActivity(intent);
                        activity.finishAffinity();

                        dialog.cancel();
                    });

            builder1.setNegativeButton(
                    "No",
                    (dialog, id) -> dialog.cancel());

            AlertDialog alert11 = builder1.create();
            alert11.show();


        });

    }


    public static void hide_keyboard(Context context) {
        ((AppCompatActivity) context).getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
    }

    public static void go_to_next_activity(Activity activity, Class next_activity) {
        activity.startActivity(new Intent(activity, next_activity));
    }


    static Dialog mDialog;


    public static void showProgressDialog(Activity activity) {
        if (mDialog != null) {
            mDialog.dismiss();
        }
        mDialog = new Dialog(activity);
        mDialog.setCancelable(false);
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        mDialog.setContentView(R.layout.custom_progress_dialog);
        mDialog.show();
    }

    public static void hideProgressDialog(Activity activity) {
        if (mDialog != null) {
            mDialog.dismiss();
        }
    }

    public static boolean emailValidator(String email) {
        Pattern pattern;
        Matcher matcher;
        final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(email);
        return matcher.matches();
    }

/*
    public static HashMap Json_rpc_format(HashMap<String, String> params) {
        HashMap<String, Object> main_param = new HashMap<String, Object>();
        main_param.put("jsonrpc", "2.0");
        main_param.put("params", params);
        Log.e("request", new JSONObject(main_param).toString());
        return main_param;
    }*/

    public static JSONObject Json_rpc_format(HashMap<String, String> params) {

        HashMap<String, Object> main_param = new HashMap<String, Object>();
        main_param.put("params", new JSONObject(params));
        main_param.put("jsonrpc", "2.0");
        Log.e("req", new JSONObject(main_param).toString());
        return new JSONObject(main_param);

    }

    public static JSONObject Json_rpc_object_format(HashMap<String, Object> params) {
        HashMap<String, Object> main_param = new HashMap<String, Object>();
        main_param.put("params", new JSONObject(params));
        main_param.put("jsonrpc", "2.0");
        Log.e("req", new JSONObject(main_param).toString());
        return new JSONObject(main_param);
    }

    public static JSONObject get_result_from_webservice(Activity activity, JSONObject response) {
        JSONObject result = null;
        if (response.has("error")) {

            try {
                String error = response.getString("error");
                JSONObject jsonObject = new JSONObject(error);

                SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(activity, SweetAlertDialog.ERROR_TYPE);
                if (sweetAlertDialog.isShowing()) {
                    sweetAlertDialog.dismissWithAnimation();
                }

                sweetAlertDialog.setTitleText(jsonObject.getString("message"));
                sweetAlertDialog.setContentText(jsonObject.getString("meaning"));
                sweetAlertDialog.setConfirmText("Ok");
                sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();
                    }
                });
                sweetAlertDialog.show();


            } catch (JSONException e) {
                e.printStackTrace();
                Log.e("JsonException:", String.valueOf(e));
            }

        } else if (response.has("result")) {
            try {
                result = response.getJSONObject("result");
            } catch (JSONException e) {
                e.printStackTrace();
            }

        } else {
            error_alert(activity);

        }
        return result;

    }

    public static void error_alert(Activity activity) {
        try {
            if (!activity.isFinishing()) {
                SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(activity, SweetAlertDialog.ERROR_TYPE);
                if (sweetAlertDialog.isShowing()) {
                    sweetAlertDialog.dismissWithAnimation();
                }


                sweetAlertDialog.setTitleText("Oops!");
                sweetAlertDialog.setContentText("Something went wrong. Please try after some time.");
                sweetAlertDialog.setConfirmText("Ok");
                sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();
                    }
                });
                sweetAlertDialog.show();

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void network_error_alert(Activity activity) {
        SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(activity, SweetAlertDialog.ERROR_TYPE);
        if (sweetAlertDialog.isShowing()) {
            sweetAlertDialog.dismissWithAnimation();
        }

        sweetAlertDialog.setTitleText("Network Error");
        sweetAlertDialog.setContentText("Please check your your internet connection.");
        sweetAlertDialog.setConfirmText("Settings");
        sweetAlertDialog.setCancelText("Okay");
        sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sDialog) {
                sDialog.dismissWithAnimation();
                activity.startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 1);
            }
        });
        sweetAlertDialog.setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sDialog) {
                sDialog.dismissWithAnimation();
            }
        });
        sweetAlertDialog.show();
    }

    public static JSONObject Json_rpc_format_obj(HashMap<String, Object> params) {
        HashMap<String, Object> main_param = new HashMap<String, Object>();
        main_param.put("params", new JSONObject(params));
        main_param.put("jsonrpc", "2.0");
        return new JSONObject(main_param);
    }


    public static Bundle get_Bundle(Activity activity) {

        return activity.getIntent().getBundleExtra(BUNDLE_KEY);
    }

    public static void openWhatsAppConversation(Context context, String number, String message) {

        number = number.replace(" ", "").replace("+", "");

        Intent sendIntent = new Intent("android.intent.action.MAIN");

        sendIntent.setType("text/plain");
        sendIntent.putExtra(Intent.EXTRA_TEXT, message);
        sendIntent.setComponent(new ComponentName("com.whatsapp", "com.whatsapp.Conversation"));
        sendIntent.putExtra("jid", PhoneNumberUtils.stripSeparators(number) + "@s.whatsapp.net");

        context.startActivity(sendIntent);
    }

    public static class StringWithTag {
        public String string;
        public Object tag;

        public StringWithTag(String stringPart, Object tagPart) {
            string = stringPart;
            tag = tagPart;
        }

        @Override
        public String toString() {
            return string;
        }
    }

    public static boolean isNetworkConnected(Activity activity) {
        ConnectivityManager cm = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected();
    }


    public static class CountryModelName {
        public String countryName;
        public String countrySortName;
        public String countryCode;
        public Object tag;

        public CountryModelName(String countryName, String countrySortName, String countryCode, Object tagPart) {
            this.countryName = countryName;
            this.countrySortName = countrySortName;
            this.countryCode = countryCode;
            this.tag = tagPart;
        }

        @Override
        public String toString() {
            return countryName;
        }
    }

    public static class CountryModelName3 {
        public String shippingAllowed;
        public String countryName;
        public String countrySortName;
        public String countryCode;
        public Object tag;

        public CountryModelName3(String shippingAllowed, String countryName, String countrySortName, String countryCode, Object tagPart) {
            this.shippingAllowed = shippingAllowed;
            this.countryName = countryName;
            this.countrySortName = countrySortName;
            this.countryCode = countryCode;
            this.tag = tagPart;
        }

        @Override
        public String toString() {
            return countryName;
        }
    }

    public static class CountryModelName2 {
        public String countryName;
        public String countrySortName;
        public String countryCode;
        public String currencyCode;
        public String currencyConvert;
        public Object tag;

        public CountryModelName2(String countryName, String countrySortName, String countryCode, String currencyCode, String currencyConvert, Object tagPart) {
            this.countryName = countryName;
            this.countrySortName = countrySortName;
            this.countryCode = countryCode;
            this.currencyCode = currencyCode;
            this.currencyConvert = currencyConvert;
            this.tag = tagPart;
        }

        @Override
        public String toString() {
            return countryName;
        }
    }

    public static class CountryModelCode implements Serializable {
        public String countryName;
        public String countrySortName;
        public String countryCode;
        public String tag;

        public CountryModelCode(String countryName, String countrySortName, String countryCode, String tagPart) {
            this.countryName = countryName;
            this.countrySortName = countrySortName;
            this.countryCode = countryCode;
            this.tag = tagPart;
        }

    }


    public static String checkNull(Object data) {
        if (data.equals(null) || data.equals("null") || data.equals("") || data.equals("Select")) {
            return "";
        } else {
            return String.valueOf(data);
        }
    }

    public static String noFormat(double d) {
        d = noRound(d, 2);
        if (d == (long) d)
            return String.format(Locale.ENGLISH, "%d", (long) d);
        else
            return String.format(Locale.ENGLISH, "%s", d);
    }

    public static double noRound(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }

}
